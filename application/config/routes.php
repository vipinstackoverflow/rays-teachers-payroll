<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Faculty PayRoll Management
* Company 
* Branches
* Faculties
* Payment Slabs
* Class Report
* PaySlip Generation
*/
$route['default_controller'] = 'user/LoginController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Admin Login
$route['user/login'] = 'user/LoginController';
$route['user/authenticate'] = 'user/AuthController/authenticate';
$route['user/logout'] = 'user/LoginController/logout';
//Admin Change Password
$route['password/change-password'] = 'admin/PasswordController/change_password';
$route['password/update-password'] = 'admin/PasswordController/update_password';

//Admin Home
$route['admin/userpanel'] = 'admin/AdminController';

$route['user/users'] = 'admin/UserController/users';
$route['user/is-username-valid'] = 'admin/UserController/is_username_valid';
$route['user/register'] = 'admin/UserController/register';
$route['user/update-status/(:num)/(:any)'] = 'admin/UserController/update_user_status/$1/$2';
$route['user/edit/(:num)'] = 'admin/UserController/edit_user/$1';
$route['user/update/(:num)'] = 'admin/UserController/update_user/$1';
$route['user/delete/(:num)'] = 'admin/UserController/delete_user/$1';

//Centers
$route['institute/centers'] = 'admin/InstituteController/centers';
$route['institute/save-center'] = 'admin/InstituteController/save_center';
$route['institute/update-center'] = 'admin/InstituteController/update_center';
$route['institute/delete-center/(:num)'] = 'admin/InstituteController/delete_center/$1';

//Courses,Batches,Subjects,Chapters
$route['institute/courses'] = 'admin/CourseController/courses';
$route['institute/course/save'] = 'admin/CourseController/save';

$route['institute/batches/(:num)'] = 'admin/BatchController/batches/$1';
$route['institute/batch/save/(:num)'] = 'admin/BatchController/save/$1';
$route['institute/batch/update/(:num)'] = 'admin/BatchController/update/$1';
$route['institute/batch/delete/(:num)/(:num)'] = 'admin/BatchController/delete/$1/$2';

$route['institute/subjects'] = 'admin/SubjectController/subjects';
$route['institute/subject/save'] = 'admin/SubjectController/save';
$route['institute/subject/update'] = 'admin/SubjectController/update';
$route['institute/subject/delete/(:num)'] = 'admin/SubjectController/delete/$1';

$route['institute/chapters/(:num)/(:num)'] = 'admin/ChapterController/chapters/$1/$2';
$route['institute/chapter/save/(:num)/(:num)'] = 'admin/ChapterController/save/$1/$2';
$route['institute/chapter/update/(:num)/(:num)'] = 'admin/ChapterController/update/$1/$2';
$route['institute/chapter/delete/(:num)/(:num)/(:num)'] = 'admin/ChapterController/delete/$1/$2/$3';

$route['faculty/registration'] = 'admin/FacultyRegisterController/registration';
$route['faculty/register'] = 'admin/FacultyRegisterController/register';
$route['faculty/faculties'] = 'admin/FacultyController/faculties';
$route['faculty/update'] = 'admin/FacultyEditController/update';
$route['faculty/delete/(:num)'] = 'admin/FacultyDeleteController/delete/$1';

$route['faculty/bc'] = 'admin/BCController/batch_in_charges';
$route['bc/register'] = 'admin/BCRegisterController/register';
$route['bc/edit/(:num)'] = 'admin/BCEditController/edit/$1';
$route['bc/update'] = 'admin/BCEditController/update';
$route['bc/delete/(:num)'] = 'admin/BCDeleteController/delete/$1';

$route['slab/slab-types'] = 'admin/SlabTypeController/slab_types';
$route['slab/save'] = 'admin/SlabTypeController/save';
$route['slab/update'] = 'admin/SlabTypeController/update';
$route['slab/delete/(:num)'] = 'admin/SlabTypeController/delete/$1';

$route['slab/slabs'] = 'admin/SlabController/faculties';
$route['slab/slabs/(:num)'] = 'admin/SlabController/slabs/$1';

$route['slab/save-slab/(:num)'] = 'admin/SlabController/save/$1';
$route['slab/update-slab/(:num)'] = 'admin/SlabController/update/$1';
$route['slab/delete-slab/(:num)/(:num)'] = 'admin/SlabController/delete/$1/$2';

$route['timetable/upload'] = 'admin/TimeTableController/upload';
$route['timetable/save'] = 'admin/TimeTableController/upload_excel';
$route['viewbc'] = 'admin/ReportController/view_bc';
$route['timing-requests/date/(:any)'] = 'admin/TimingRequestController/date/$1';


$route['timming-request/daily-report/(:any)'] = 'admin/ReportController/dailyreport/$1';
$route['timing-requests/daily-report/(:any)/(:any)'] = 'admin/ReportController/viewBatchDetails/$1/$2';


$route['timing-requests/unsubmitted/(:any)'] = 'admin/TimingRequestController/unsubmitted/$1';
$route['timing-requests/(:any)/(:any)'] = 'admin/TimingRequestController/view_requests/$1/$2';
$route['timing-requests/add-fac-req-to-bc/(:any)/(:any)'] = 'admin/TimingRequestController/add_faculty_request_to_bc/$1/$2';
$route['timing-requests/delete/(:any)/(:any)/(:any)'] = 'admin/TimingRequestController/delete_timing/$1/$2/$3';
$route['timing-requests/final-approval/(:num)/(:any)'] = 'admin/TimingRequestController/final_approval/$1/$2';
$route['timing-requests/update-final-approval/(:num)/(:any)'] = 'admin/TimingRequestController/update_final_approval/$1/$2';

$route['timing-request/submit/approve/(:num)'] = 'admin/TimingRequestController/submit/$1';
$route['request/approve/(:any)/(:any)/(:any)/(:any)'] = 'admin/TimingRequestController/approve/$1/$2/$3/$4';

$route['report'] = 'admin/TimingRequestController/report';
$route['faculty-report'] = 'admin/TimingSearchController/searchreport';


$route['timing-requests/approved-report'] = 'admin/TimingRequestController/approved';
$route['timing-requests/approved-search'] = 'admin/TimingSearchController/search';

$route['timing-requests/consolidated-report'] = 'admin/ReportController/report';
$route['timing-requests/search-consolidated-report'] = 'admin/ReportController/search_report';

$route['timing-requests/daily-report'] = 'admin/ReportController/daily_report';
//Faculty
$route['faculty/userpanel'] = 'faculty/FacultyController';

$route['profile'] = 'faculty/ProfileController/profile';
$route['profile/update'] = 'faculty/ProfileController/update';

$route['timetable/view/(:any)'] = 'faculty/TimeTableController/view/$1';

$route['timing'] = 'faculty/TimingController/timings';
$route['timing/submit'] = 'faculty/TimingController/submit';
$route['timing/update/(:num)'] = 'faculty/TimingController/update/$1';
$route['timing/delete/(:num)'] = 'faculty/TimingController/delete/$1';
$route['timing/final-submission'] = 'faculty/TimingController/final_submission';

$route['timing/approved'] = 'faculty/TimingController/approved';
$route['lastmonthreport'] = 'faculty/TimingController/lastmonthreport';

//Batch In Charge
$route['bc/userpanel'] = 'bc/BCController';
$route['time-table/batches'] = 'bc/TimeTableController/batches';
$route['time-table/view/(:any)/(:any)'] = 'bc/TimeTableController/view/$1/$2';

$route['timings/batches'] = 'bc/TimingController/batches';
$route['timings/new/(:any)'] = 'bc/TimingController/timings/$1';
$route['timings/submit'] = 'bc/TimingController/submit';
$route['timings/update/(:num)'] = 'bc/TimingController/update/$1';
$route['timings/delete/(:num)'] = 'bc/TimingController/delete/$1';
$route['timings/final-submission'] = 'bc/TimingController/final_submission';
$route['timings/approved'] = 'bc/TimingController/approved';

//Status

$route['status/userpanel'] = 'status/StatusController';
$route['status/chapters-status/(:num)/(:num)/(:num)/(:num)'] = 'status/StatusController/status/$1/$2/$3/$4';
//API
$route['api/v1/classes'] = 'api/APIController/classes';
$route['api/v1/batches'] = 'api/APIController/batches';
$route['api/v1/subjects'] = 'api/APIController/subjects';
$route['api/v1/slabtypes'] = 'api/APIController/slabtypes';
$route['api/v1/chapters'] = 'api/APIController/chapters';
$route['api/v1/teachers'] = 'api/APIController/teachers';
$route['api/v1/chapter-hour'] = 'api/APIController/chapter_hour';