   <?php $this->load->view('import/header'); ?>

    <main class="app-content">

      <div class="app-title">

        <div>

          <h1><i class="fa fa-users"></i> Faculty List</h1>

          <p>Faculty Payroll</p>

        </div>

        <ul class="app-breadcrumb breadcrumb">

          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>

          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>

          <li class="breadcrumb-item"><a href="#">lastmonthreport</a></li>

        </ul>

      </div>

      <div class="row" >

        <div class="col-md-12" >

          <div class="tile table-responsive" >

              <?php if($this->session->flashdata('success')) { ?>

              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">

               <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                  <span aria-hidden="true" style="font-size:20px">×</span>

                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>

              </div>

              <?php } ?>

              <?php if($this->session->flashdata('error')) { ?>

              <div class="alert alert-danger fade in alert-dismissible show">

               <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                  <span aria-hidden="true" style="font-size:20px">×</span>

                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>

              </div>

              <?php } ?> 

              <h4>Approved Timings</h4>

              <table id="approved" name="approved" class="table table-bordered" >

                <thead>

                  <tr>

                    <td>Date</td>

                    <td>Batch</td>

                    <td>Slab</td>

                    <td>Start Time</td>

                    <td>End Time</td>

                    <td>Duration</td>

                    <!-- <td>Payment</td> -->

                  </tr>

                </thead>

                <tbody>

                <?php $i=0; $payment = 0; foreach($approved as $data) { $i++; $payment += $data->payment; ?>

                <?php $date = date('d-m-Y', strtotime($data->_date)); ?>

                  <tr> 

                    <td ><?php echo date('d M', strtotime($data->_date)); ?></td>

                    <td><?php echo $data->batch_code; ?></td>

                    <td><?php echo $data->slab_type; ?></td>

                    <td><?php echo date('h:ia', strtotime($data->start_time)); ?></td>

                    <td><?php echo date('h:ia', strtotime($data->end_time)); ?></td>

                    <td><?php echo $data->duration; ?></td>

                    <!-- <td><?php echo $data->payment; ?></td> -->

                  </tr>

                <?php } ?>

                <tr>

                  <td></td>

                  <td></td>

                  <td></td>

                  <td></td>

                  <td></td>

                  <td><h5>Summary</h5></td>

                  <td></td>

                </tr>

                <?php foreach($summary as $data) { ?>

                  <tr>

                    <td></td>

                    <td></td>

                    <td></td>

                    <td></td>

                    <td><?php echo $data->slab_type; ?></td>

                    <td><?php echo $data->meta->duration; ?></td>

                    <!-- <td><?php echo $data->meta->amount; ?></td> -->

                  </tr>

                <?php } ?>

                  <tr>

                    <td></td>

                    <td></td>

                    <td></td>

                    <td></td>

                    <td></td>

                    <!-- <td>Total</td>

                    <td><?php echo $payment; ?></td> -->

                  </tr>

                </tbody>

              </table>

              <div class="tile-footer" >

              <!-- <div class="row">

                <div class="col-md-8 col-md-offset-3">

                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;

                </div>

              </div> -->              

            </div>

          </div>

        </div>

      </div>

    </main>

    <!-- The Modal -->

    <div class="modal" id="editFaculty">

      <div class="modal-dialog">

        <div class="modal-content">

        

          <!-- Modal Header -->

          <div class="modal-header">

            <h4 class="modal-title">Edit Faculty</h4>

            <button type="button" class="close" data-dismiss="modal">&times;</button>

          </div>

          

          <!-- Modal body -->

          <?php echo form_open('faculty/update', 'id="updatefaculty"'); ?>

          <div class="modal-body">

            <input type="hidden" name="facultyid" id="facultyid" >

            <div class="form-group row">

              <label class="control-label col-md-3">Faculty Code</label>

              <div class="col-md-8">

                <input class="form-control" type="text" name="facultycode" id="facultycode" placeholder="Faculty Code eg:AS">

              </div>

            </div>

            <div class="form-group row">

              <label class="control-label col-md-3">Name</label>

              <div class="col-md-8">

                <input class="form-control" type="text" name="name" id="name" placeholder="Enter full name">

              </div>

            </div>

            <div class="form-group row">

              <label class="control-label col-md-3">Email</label>

              <div class="col-md-8">

                <input class="form-control" type="email" name="email" id="email" placeholder="Enter email address">

              </div>

            </div>

            <div class="form-group row">

              <label class="control-label col-md-3">Phone</label>

              <div class="col-md-8">

                <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone">

              </div>

            </div>

            <div class="form-group row">

              <label class="control-label col-md-3">Address</label>

              <div class="col-md-8">

                <textarea class="form-control" rows="4" name="address" id="address" placeholder="Enter your current address"></textarea>

              </div>

            </div>

            <div class="form-group row">

              <label class="control-label col-md-3">Center</label>

              <div class="col-md-8">

                <select class="form-control" id="branch" name="branch" >

                  <?php foreach($branches as $data) { ?>

                  <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>

                  <?php } ?>

                </select>

              </div>

            </div>

          </div>

          

          <!-- Modal footer -->

          <div class="modal-footer">

            <button type="submit" class="btn btn-success" >Update</button>

          </div>

          <?php echo form_close(); ?>

        </div>

      </div>

    </div>   

    <?php $this->load->view('import/scripts'); ?>

    <?php $this->load->view('import/data-table'); ?>

    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script src="<?php echo base_url('assets/admin/js/jquery.table.marge.js'); ?>"></script>



    <script type="text/javascript">

      

      $('#approved').margetable({

          type: 2,

          colindex: [0]

      });



      $("#approved").DataTable({

        dom: 'Bfrtip',

        buttons: [

          {

            extend: 'print',

            exportOptions: {

              columns: [0, 1, 2, 3, 4, 5, 6]

            },

            title:"<?php echo @$faculty->name." - Report"; ?>"

          }, 

          {

            extend: 'excel',

            exportOptions: {

              columns: [0, 1, 2, 3, 4, 5, 6]

            },

            title:"<?php echo @$faculty->name." - Report"; ?>"

          },

          {

            extend: 'pdf',

            exportOptions: {

              columns: [0, 1, 2, 3, 4, 5, 6]

            },

            title:"<?php echo @$faculty->name." - Report"; ?>"

          }

        ],

        "stateSave":true,

        "order" : [],

        "bPaginate" : false

      });

    </script>

    <?php $this->load->view('import/footer'); ?>