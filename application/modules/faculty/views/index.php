   <?php $this->load->view('import/header'); ?>
   <style type "text/css">
<!--
/* @group Blink */
.blink {
    -webkit-animation: blink .75s linear infinite;
    -moz-animation: blink .75s linear infinite;
    -ms-animation: blink .75s linear infinite;
    -o-animation: blink .75s linear infinite;
     animation: blink .75s linear infinite;
}
@-webkit-keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
}
@-moz-keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
}
@-ms-keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
}
@-o-keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
}
@keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
}
/* @end */

-->

</style>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('profile'); ?>">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-user fa-3x"></i>
            <div class="info">
              <h4>Profile</h4>
              <p><b>Update Profile</b></p>
            </div>
          </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('timetable/view/'.date('d-m-Y')); ?>">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-calendar fa-3x"></i>
            <div class="info">
              <h4>Time Table</h4>
              <p><b>View Timings</b></p>
            </div>
          </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('timing'); ?>">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-clock-o fa-3x"></i>
            <div class="info">
              <h4>Timings</h4>
              <p><b>Mark Ur Timings</b></p>
            </div>
          </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('timing/approved') ?>">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-check fa-3x"></i>
            <div class="info">
              <h4>Approved Timings</h4>
              <p><b>View</b></p>
            </div>
          </div>
          </a>
        </div>
        
        <div class="col-md-6 col-lg-3" id="togglee">
          <a href="<?php echo base_url('lastmonthreport') ?>">
          <div class="widget-small primary coloured-icon" ><i class="icon fa fa-inr fa-3x"  ></i>
            <div class="info">
              <h4>LAST MONTH REPORT</h4>
              <p><b><center> <h4 style="color:#943126;font-family: times;"class="tab blink">October 2019</h4></center></b></p>
            </div>
          </div>
          </a>
        </div>
      
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <!--  <script type="text/javascript">
      var h = new Date().getHours();
      var date = new Date();
      // alert(date);
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 28);
      // alert(firstDay);
      var lastDay = new Date(date.getFullYear(), date.getMonth(), 5);
       // alert(lastDay);
   if (date>=firstDay && date<=lastDay )  {  
    // if(h>=17 && h<18)
    // {
            document.getElementById('togglee').style.visibility = 'visible';
        } else {
          // alert('ff');
            document.getElementById('togglee').style.visibility = 'hidden';
        }
      </script> -->
    <?php $this->load->view('import/footer'); ?>