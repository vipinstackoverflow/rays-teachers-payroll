<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingModel extends CI_Model {

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}
	public function faculty1($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}

	public function centers()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}
	
	public function classes()
	{
		$query = $this->db->get('classes');

		return $query->result();
	}

	public function subject()
	{
		$facultyID = $this->session->facultyid;

		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row()->subject;
	}

	public function batches_list($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function batches()
	{
		$this->db->where_in('batch_id', json_decode($this->session->permissions));
		$this->db->join('classes', 'classes.class_id = batches.class_id');
		$this->db->join('branches', 'branches.branch_id = batches.branch_id');
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function periods()
	{
		$query = $this->db->get('periods');

		return $query->result();
	}

	public function slabtypes($centerID)
	{
		$this->db->select('slab_type_id, slab_type');
		$this->db->where('status', 'active');
		$this->db->where('branch_id', $centerID);
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function chapters($subjectID)
	{
		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('chapters');

		return $query->result();
	}

	public function submitted_timings()
	{
		$this->db->where('_date', date('Y-m-d'));
		$this->db->where('faculty_id', $this->session->facultyid);
		$this->db->where('status', 'user_pending');
		$this->db->join('batches', 'batches.batch_id = timings.batch_id');
		$query = $this->db->get('timings');

		return $query->result();
	}

	/**
	* Get time table for the requested date for the faculty
	* Timing is stored for each faculty in timetable with faculty code
	* Get faculty code from faculty table using faculty id in the session
	* @param string $date, string $facultyID
	* @return array
	*/
	public function timings($date, $facultyID)
	{
		//get faculty code using faculty ID
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		$facultyCode = $query->row()->faculty_code;

		// Get timings
		$this->db->where('faculty_code', $facultyCode);
		$this->db->where('_date', $date);
		$this->db->where('time_table.status', 'pending');
		$this->db->join('slab_types', 'slab_types.slab_type_id = time_table.slab_type_id', 'left');
		$this->db->order_by('time_table.start_time', 'ASC');
		$query = $this->db->get('time_table');

		return $query->result();
	}

	public function submit($timing)
	{
		//Generate UID
		$this->db->select_max('timing_uid');
		$query = $this->db->get('timings');

		$timingUID = 1;
		$timingUID = ($query->num_rows() > 0)? $query->row()->timing_uid+1 : 1;

		$timing['timing_uid'] = $timingUID;

		$this->db->insert('timings', $timing);
	}

	public function update($timing, $timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->update('timings', $timing);
	}

	public function delete($timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->delete('timings');
	}

	public function update_timetable($timetableID)
	{
		$timetable['status'] = 'submitted';
		
		$this->db->where('time_table_id', $timetableID);
		$this->db->update('time_table', $timetable);
	}

	public function slab($slabtypeID, $facultyID)
	{
		$this->db->where('slab_type_id', $slabtypeID);
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('slabs');

		return $query->row();
	}

	public function final_submission($timingID)
	{
		for ($i=0; $i < count($timingID); $i++) 
		{ 
			$timing['status'] = 'pending';

			$this->db->where('timing_id', $timingID[$i]);
			$this->db->update('timings', $timing);
		}
	}

	public function approved($facultyID)
	{
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->result();
	}


	public function search($facultyID)
	{
	    $from = (date('d') > 29)? date('Y-m').'-29' : date('Y-m', strtotime('-1 months')).'-29';
	    $to = date('Y-m-d');
		$this->db->where('timings._date >=', $from);
		$this->db->where('timings._date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$query = $this->db->get('timings');

		return $query->result();
	}
    public function search1($facultyID)
	{
	    $from = "2019-09-29";
	    $to="2019-10-28";
	    // $from = (date('d') > 29)? date('Y-m',strtotime('-2 months')).'-29':date('Y-m',strtotime('-2 months')).'-29';
	    // $to =(date('d') > 29)? date('Y-m').'-28' : date('Y-m', strtotime('-1 months')).'-28';
       
		$this->db->where('timings._date >=', $from);
		$this->db->where('timings._date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$query = $this->db->get('timings');
 // print_r($query->result());exit;
		return $query->result();
	}
	public function search_summary($facultyID)
	{
	    $from = (date('d') > 29)? date('Y-m').'-29' : date('Y-m', strtotime('-1 months')).'-29';
	    $to = date('Y-m-d');

		$this->db->where('timings._date >=', $from);
		$this->db->where('timings._date <=', $to);
		//$this->db->where('_date >=', $from);
		//$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->group_by('timings.slab_type_id');
		$query = $this->db->get('timings');

		if($query->num_rows() > 0)
		{
			$slabtypes = array();
			foreach ($query->result() as $data) 
			{
				$slabtypes[] = $data->slab_type_id;
			}

			$this->db->where_in('slab_type_id', $slabtypes);
			$query = $this->db->get('slab_types');

			$summaryData = array();
			if ($query->num_rows() > 0) 
			{
				$slabtypesData = $query->result();

				for ($i=0; $i < count($slabtypesData); $i++) 
				{
					$summaryData[$i] = $slabtypesData[$i];
					$summaryData[$i]->meta 	= $this->summary_meta($facultyID, $slabtypesData[$i]->slab_type_id, $from, $to);
				}
			}

			return $summaryData;
		}
		else
		{
			return array();
		}
	}
	public function search_summary1($facultyID)
	{
	    // $from = (date('d') > 29)? date('Y-m',strtotime('-2 months')).'-29':date('Y-m',strtotime('-2 months')).'-29';
	    // $to = (date('d') > 29)? date('Y-m').'-28' : date('Y-m', strtotime('-1 months')).'-28';
        $from = "2019-09-29";
	    $to="2019-10-28";
		$this->db->where('timings._date >=', $from);
		$this->db->where('timings._date <=', $to);
		//$this->db->where('_date >=', $from);
		//$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->group_by('timings.slab_type_id');
		$query = $this->db->get('timings');

		if($query->num_rows() > 0)
		{
			$slabtypes = array();
			foreach ($query->result() as $data) 
			{
				$slabtypes[] = $data->slab_type_id;
			}

			$this->db->where_in('slab_type_id', $slabtypes);
			$query = $this->db->get('slab_types');

			$summaryData = array();
			if ($query->num_rows() > 0) 
			{
				$slabtypesData = $query->result();

				for ($i=0; $i < count($slabtypesData); $i++) 
				{
					$summaryData[$i] = $slabtypesData[$i];
					$summaryData[$i]->meta 	= $this->summary_meta($facultyID, $slabtypesData[$i]->slab_type_id, $from, $to);
				}
			}

			return $summaryData;
		}
		else
		{
			return array();
		}
	}

	private function summary_meta($facultyID, $slabtypeID, $from, $to)
	{
		$this->db->where('timings._date >=', $from);
		$this->db->where('timings._date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.slab_type_id', $slabtypeID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');


		$duration = date('Y-m-d H:i:s', strtotime('2018-01-01 00:00:00'));
		$hour = 0;
		$minutes = 0;
		$seconds = 0;
		$amount 	= '0';
		foreach ($query->result() as $data) 
		{
		    $hour += date('H', strtotime($data->duration));
		    $minutes += date('i', strtotime($data->duration));
		    $seconds += date('s', strtotime($data->duration));
		    
			$amount 	+= $data->payment;
		}
		$duration 	= date('Y-m-d H:i:s',strtotime("+".$hour." hour +".$minutes." minutes +".$seconds." seconds",strtotime($duration)));
        $datetime1 = new DateTime('2018-01-01 00:00:00');
        $datetime2 = new DateTime(date('Y-m-d H:i:s', strtotime($duration)));
        $interval = $datetime1->diff($datetime2);
        //echo $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes"; exit;
        
        $h = $interval->format('%d')*24;
        $h += $interval->format('%h');
        $m = $interval->format('%i');
        $s = $interval->format('%s');
        	
		$data = (object) array('duration' => $h.' Hours '.$m.' minutes '.$s.'  seconds', 'amount' => $amount);
		
		return $data;
	}

}