<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimeTableModel extends CI_Model {

	/**
	* Get time table for the requested date for the faculty
	* Timing is stored for each faculty in timetable with faculty code
	* Get faculty code from faculty table using faculty id in the session
	* @param string $date, string $facultyID
	* @return array
	*/
	public function timings($date, $facultyID)
	{
		//get faculty code using faculty ID
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		$facultyCode = $query->row()->faculty_code;

		// Get timings
		$this->db->where('faculty_code', $facultyCode);
		$this->db->where('_date', $date);
		$this->db->join('slab_types', 'slab_types.slab_type_id = time_table.slab_type_id', 'left');
		$this->db->order_by('time_table.start_time', 'ASC');
		$query = $this->db->get('time_table');

		return $query->result();
	}
}