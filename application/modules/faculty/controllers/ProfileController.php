<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('ProfileModel');
	}

	private function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
		elseif ($this->session->usertype != '4') 
		{
			redirect('user/login');
		}
	}

	public function profile()
	{
		$facultyID = $this->session->facultyid;

		$data['profile'] = $this->ProfileModel->profile($facultyID);

		$this->load->view('profile/profile', $data);
	}

	/**
	* Update faculty with their information
	* Validate and Update Faculty details
	*/
	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim');
		$this->form_validation->set_rules('phone', 'Branch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('email', 'email', 'required|xss_clean|trim|valid_email');
		$this->form_validation->set_rules('address', 'Address', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$facultyID 					= $this->session->facultyid;
			$faculty['name'] 			= $this->input->post('name');
			$faculty['email'] 			= $this->input->post('email');
			$faculty['phone'] 			= $this->input->post('phone');
			$faculty['address'] 		= $this->input->post('address');
			$faculty['ac_holder'] 		= $this->input->post('ac_holder');
			$faculty['ac_number'] 		= $this->input->post('ac_number');
			$faculty['ifsc'] 			= $this->input->post('ifsc');
			$faculty['bank_name'] 		= $this->input->post('bank');
			$faculty['pan_no'] 			= $this->input->post('pan');
			$faculty['branch_id'] 		= $this->input->post('branch');

			$config['upload_path'] 		= './resources/images/';
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['max_size'] 		= '10000';

			$this->load->library('upload', $config);

			//create folder if does not exist
			if(!is_dir('./resources/images/'))
			{
				mkdir('./resources/images/', 0777 ,TRUE);
			}

			if($this->upload->do_upload('image'))
			{
				$upload_data 	 = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$faculty['image'] = $upload_data['file_name'];
			}
			
			$this->db->trans_start();

				$this->ProfileModel->update($faculty, $facultyID);
				$this->ProfileModel->update_user($faculty, $facultyID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('profile');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('profile');
		}
	}
}