<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
	}

	private function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
		elseif ($this->session->usertype != '4') 
		{
			redirect('user/login');
		}
	}

	public function index()
	{
		$this->load->view('index');
	}
}