<?php defined('BASEPATH') OR exit('No direct script access allowed');



class TimingController extends MX_Controller {



	public function __construct()

	{

		parent::__construct();



		$this->isuservalid();

		$this->load->model('TimingModel');

	}



	private function isuservalid()

	{

		if(!$this->session->user)

		{

			redirect('user/login');

		}

		elseif ($this->session->usertype != '4') 

		{

			redirect('user/login');

		}

	}



	public function timings()

	{

		$data['centres'] = $this->TimingModel->centers();

		$data['classes'] = $this->TimingModel->classes();

		$data['subject'] = $this->TimingModel->subject();

		//$data['batches'] = $this->TimingModel->batches();

		$data['periods'] = $this->TimingModel->periods();

		$timings 		 = $this->TimingModel->submitted_timings();



		$timingData = array();

		for ($i=0; $i < count($timings); $i++) 

		{ 

			$timingData[$i] = $timings[$i];

			$timingData[$i]->batches 	= $this->TimingModel->batches_list($timings[$i]->class_id);

			$timingData[$i]->chapters 	= $this->TimingModel->chapters($timings[$i]->subject_id);

			$timingData[$i]->slabtypes 	= $this->TimingModel->slabtypes($timings[$i]->center_id);

		}



		$data['timings'] = $timingData;



		$this->load->view('timing/timings', $data);

	}



	public function submit()

	{

		$this->load->library('form_validation');



		$this->form_validation->set_rules('ttdate', 'Date', 'required|xss_clean|trim');

		//$this->form_validation->set_rules('center', 'Center', 'required|xss_clean|trim');

		//$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');

		$this->form_validation->set_rules('batch', 'Batch', 'required|xss_clean|trim');

		$this->form_validation->set_rules('slab', 'Slab', 'required|xss_clean|trim');

		$this->form_validation->set_rules('start', 'Started Time', 'required|xss_clean|trim');

		$this->form_validation->set_rules('end', 'Ended Time', 'required|xss_clean|trim');

		//$this->form_validation->set_rules('remarks', 'Remarks', 'required|xss_clean|trim');



		if($this->form_validation->run() == TRUE)

		{

			$slabtypeID 				= $this->input->post('slab');

			$facultyID 					= $this->session->facultyid;

			$slab 						= $this->TimingModel->slab($slabtypeID, $facultyID);

			$rate 						= (isset($slab))? $slab->rate : 0;

			$extra 						= (isset($slab))? $slab->extra : 0;

			$amount						= ($rate+$extra)/60;

			$batch 						= explode("|", $this->input->post('batch'));



			$timing['time_table_id'] 	= '0';

			$timing['center_id'] 		= $this->input->post('centre');

			$timing['class_id'] 		= $this->input->post('class');

			$batch 						= explode('|', $this->input->post('batch'));

			$timing['batch_id'] 		= $batch[0];

			$subject 					= explode('|', $this->input->post('chapter'));

			$timing['subject_id'] 		= $subject[0];

			$timing['chapter_id'] 		= $subject[1];

			$timing['slab_type_id']	 	= $slabtypeID;

			$timing['period_id']	 	= $this->input->post('period');

			$timing['faculty_id'] 		= $this->session->facultyid;

			$timing['marker_id'] 		= $this->session->facultyid;

			$timing['start_time'] 		= date('H:i:s', strtotime($this->input->post('start')));

			$timing['end_time'] 		= date('H:i:s', strtotime($this->input->post('end')));

			$minutes 					= round(abs(strtotime($timing['end_time']) - strtotime($timing['start_time'])) / 60,2);

			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));

			$timing['payment'] 			= $minutes*$amount;

			$timing['batch_code'] 		= $batch[1];

			$timing['chapter_code'] 	= $subject[2];

			$timing['remarks'] 			= $this->input->post('remarks');

			$timing['_date'] 			= date('Y-m-d', strtotime($this->input->post('ttdate')));

			$timing['user'] 			= $this->session->username;



			$this->db->trans_start();



				//Submit timing for review

				$this->TimingModel->submit($timing);



			$this->db->trans_complete();

			

			($this->db->trans_status() === FALSE)?

			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):

			$this->session->set_flashdata('success', 'Timing added');



			redirect('timing');

		}

		else

		{

			$this->session->set_flashdata('error', validation_errors());

			redirect('timing');

		}

	}



	public function update($timingID)

	{

		$this->load->library('form_validation');



		$this->form_validation->set_rules('ttdate', 'Date', 'required|xss_clean|trim');

		//$this->form_validation->set_rules('center', 'Center', 'required|xss_clean|trim');

		//$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');

		$this->form_validation->set_rules('batch', 'Batch', 'required|xss_clean|trim');

		$this->form_validation->set_rules('slab', 'Slab', 'required|xss_clean|trim');

		$this->form_validation->set_rules('start', 'Started Time', 'required|xss_clean|trim');

		$this->form_validation->set_rules('end', 'Ended Time', 'required|xss_clean|trim');

		//$this->form_validation->set_rules('remarks', 'Remarks', 'required|xss_clean|trim');



		if($this->form_validation->run() == TRUE)

		{

			$slabtypeID 				= $this->input->post('slab');

			$facultyID 					= $this->session->facultyid;

			$slab 						= $this->TimingModel->slab($slabtypeID, $facultyID);

			$rate 						= (isset($slab))? $slab->rate : 0;

			$extra 						= (isset($slab))? $slab->extra : 0;

			$amount						= ($rate+$extra)/60;

			$batch 						= explode("|", $this->input->post('batch'));



			$timing['time_table_id'] 	= '0';

			$timing['center_id'] 		= $this->input->post('centre');

			$timing['class_id'] 		= $this->input->post('class');

			$batch 						= explode('|', $this->input->post('batch'));

			$timing['batch_id'] 		= $batch[0];

			$subject 					= explode('|', $this->input->post('chapter'));

			$timing['subject_id'] 		= $subject[0];

			$timing['chapter_id'] 		= $subject[1];

			$timing['slab_type_id']	 	= $slabtypeID;

			$timing['period_id']	 	= $this->input->post('period');

			$timing['faculty_id'] 		= $this->session->facultyid;

			$timing['marker_id'] 		= $this->session->facultyid;

			$timing['start_time'] 		= date('H:i:s', strtotime($this->input->post('start')));

			$timing['end_time'] 		= date('H:i:s', strtotime($this->input->post('end')));

			$minutes 					= round(abs(strtotime($timing['end_time']) - strtotime($timing['start_time'])) / 60,2);

			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));

			$timing['payment'] 			= $minutes*$amount;

			$timing['batch_code'] 		= $batch[1];

			$timing['chapter_code'] 	= $subject[2];

			$timing['remarks'] 			= $this->input->post('remarks');

			$timing['_date'] 			= date('Y-m-d', strtotime($this->input->post('ttdate')));

			$timing['user'] 			= $this->session->username;



			$this->db->trans_start();



				//Update

				$this->TimingModel->update($timing, $timingID);



			$this->db->trans_complete();

			

			($this->db->trans_status() === FALSE)?

			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):

			$this->session->set_flashdata('success', 'Updated');



			redirect('timing');

		}

		else

		{

			$this->session->set_flashdata('error', validation_errors());

			redirect('timing');

		}

	}



	public function delete($timingID)

	{

		$this->db->trans_start();



			$this->TimingModel->delete($timingID);



		$this->db->trans_complete();

		

		($this->db->trans_status() === FALSE)?

		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):

		$this->session->set_flashdata('success', 'Deleted');



		redirect('timing');

	}



	public function final_submission()

	{

		$timingID = $this->input->post('timings');



		$this->db->trans_start();



			$this->TimingModel->final_submission($timingID);



		$this->db->trans_complete();

		

		echo ($this->db->trans_status() === FALSE)? FALSE : TRUE;

	}



	public function approved()

	{

		$facultyID 			= $this->session->facultyid;

		$data['approved'] 	= $this->TimingModel->search($facultyID);

		$data['summary'] 	= $this->TimingModel->search_summary($facultyID);

		$data['faculty'] 	= $this->TimingModel->faculty($facultyID);



		$this->load->view('timing/approved', $data);

	}
	public function lastmonthreport()

	{

		$facultyID 			= $this->session->facultyid;

		$data['approved'] 	= $this->TimingModel->search1($facultyID);

		$data['summary'] 	= $this->TimingModel->search_summary1($facultyID);

		$data['faculty'] 	= $this->TimingModel->faculty1($facultyID);



		$this->load->view('timing/lastmonthreport', $data);

	}

}