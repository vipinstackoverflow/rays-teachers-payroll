<?php defined('BASEPATH') OR exit("No direct script access allowed");

class StatusModel extends CI_Model {

	public function branches()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	public function classes()
	{
		$query = $this->db->get('classes');

		return $query->result();
	}

	public function batches($centerID, $classID)
	{
		$this->db->select('batch_id, batch, batch_code');
		$this->db->where('batch_status', 'active');
		$this->db->where('branch_id', $centerID);
		$this->db->where('class_id', $classID);
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function subjects($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('subjects');

		return $query->result();
	}

	public function chapters($subjectID)
	{
		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('chapters');

		return $query->result();
	}

	public function status($centreID, $classID, $batchID, $subjectID, $chapterID)
	{
		$this->db->where('branch_id', $centreID);
		$this->db->where('class_id', $classID);
		$this->db->where('batch_id', $batchID);
		$this->db->where('subject_id', $subjectID);
		$this->db->where('chapter_id', $chapterID);
		$query = $this->db->get('chapter_timing');

		return $query->row();
	}

	public function completion_status($centreID, $classID, $batchID, $subjectID, $chapterID)
	{
		$this->db->where('center_id', $centreID);
		$this->db->where('class_id', $classID);
		$this->db->where('batch_id', $batchID);
		$this->db->where('subject_id', $subjectID);
		$this->db->where('chapter_id', $chapterID);
		$this->db->order_by('timing_id', 'DESC');
		$query = $this->db->get('timings');

		return ($query->num_rows() > 0)? $query->row()->chapter_status : 'Nil';
	}
}