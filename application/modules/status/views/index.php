   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4>Chapter Status</h4>
              <?php
                $centreID   = $this->uri->segment(3);
                $classID    = $this->uri->segment(4);
                $batchID    = $this->uri->segment(5);
                $subjectID  = $this->uri->segment(6);
              ?>
              <div class="row">
                <div class="col-md-3" >
                  <div class="form-group row">
                    <label class="control-label col-md-3">Centre</label>
                    <div class="col-md-8">
                      <select class="form-control" name="branch" id="branch" >
                          <option value="" >Please Select</option>
                        <?php foreach($branches as $data) { ?>
                          <option value="<?php echo $data->branch_id; ?>" <?php if($centreID == $data->branch_id) { ?> selected <?php } ?> ><?php echo $data->branch; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-3" >
                  <div class="form-group row">
                    <label class="control-label col-md-3">Course</label>
                    <div class="col-md-8">
                      <select class="form-control" name="class" id="class" >
                          <option value="" >Please Select</option>
                          <?php foreach($classes as $data) { ?>
                          <option value="<?php echo $data->class_id; ?>" <?php if($classID == $data->class_id) { ?> selected <?php } ?> ><?php echo $data->class; ?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-2" >
                  <div class="form-group row">
                    <label class="control-label col-md-3">Batch</label>
                    <div class="col-md-8">
                      <select class="form-control" name="batch" id="batch" >
                          <option value="" >Please Select</option>
                          <?php foreach($batches as $data) { ?>
                          <option value="<?php echo $data->batch_id; ?>" <?php if($batchID == $data->batch_id) { ?> selected <?php } ?> ><?php echo $data->batch; ?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-3" >
                  <div class="form-group row">
                    <label class="control-label col-md-3">Subject</label>
                    <div class="col-md-8">
                      <select class="form-control" name="subject" id="subject" >
                          <option value="" >Please Select</option>
                          <?php foreach($subjects as $data) { ?>
                          <option value="<?php echo $data->subject_id; ?>" <?php if($subjectID == $data->subject_id) { ?> selected <?php } ?> ><?php echo $data->subject; ?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-1" >
                  <div class="form-group row">
                    <div class="col-md-8">
                      <input type="button" class="btn btn-success" name="search" id="search" value="View" >
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-12" >
                <?php if(isset($chapters)) { ?>
                  <table class="table table-bordered" >
                    <thead>
                      <tr>
                        <th>Chapter</th>
                        <th>Hours Allowed</th>
                        <th>Hours Taken</th>
                        <th>Hours Balance</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($chapters as $data) { ?>
                      <tr>
                        <td>
                          <?php echo $data->chapter; ?>    
                        </td>
                        <td>
                          <?php echo (isset($data->chapstatus))? $data->chapstatus->hours_allowed : $data->hours; ?>
                        </td>
                        <td>
                          <?php echo (isset($data->chapstatus))? $data->chapstatus->hours_taken : 'Nil' ?> 
                        </td>
                        <td>
                          <?php
                            if(isset($data->chapstatus))
                            {
                              $minutes  = round(abs(strtotime($data->chapstatus->hours_allowed) - strtotime($data->chapstatus->hours_taken)) / 60,2);
                              $duration   = gmdate("H:i:s", ($minutes * 60));
                            }
                            else
                            {
                              $duration = $data->hours;
                            }

                            echo $duration;
                          ?>
                        </td>
                        <td><?php echo $data->chapcompletionstatus; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                <?php } ?>
              </div>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script type="text/javascript">
      
      $("#class").on('change', function(){
        centerid  = $("#branch").val();
        classid   = $(this).val();

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/batches'); ?>",
          dataType:"json",
          data:{ centerid, classid },
          success:function(res){
            batches = '<option value="" >Please Select</option>';
            $.each(res, function(i, data){
              batches += "<option value='"+data.batch_id+"' >"+data.batch+"</option>";
            });
            $("#batch").html(batches);
          },
          error:function(res){
            //console.log('Connection error');
            alert('Error establishing coonection');
          }
        });
      });
      
      $("#batch").on('change', function(){
        centreid  = $("#branch").val();
        classid   = $("#class").val();
        batchid   = $(this).val();

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/subjects'); ?>",
          dataType:"json",
          data:{ centreid, classid, batchid },
          success:function(res){
            subjects = '<option value="" >Please Select</option>';
            $.each(res, function(i, data){
              subjects += "<option value='"+data.subject_id+"' >"+data.subject+"</option>";
            });
            $("#subject").html(subjects);
          },
          error:function(res){
            //console.log('Connection error');
            alert('Error establishing coonection');
          }
        });
      });

      $("#search").on('click', function(){
        centreid    = $("#branch").val();
        classid     = $("#class").val();
        batchid     = $("#batch").val();
        subjectid   = $("#subject").val();

        window.location = "<?php echo base_url('status/chapters-status/') ?>"+centreid+"/"+classid+"/"+batchid+"/"+subjectid;
      });
    </script>
    <?php $this->load->view('import/footer'); ?>