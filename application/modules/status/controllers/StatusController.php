<?php defined('BASEPATH') OR exit('No direct script access allowed');

class StatusController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('StatusModel');
	}

	private function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '5')
		{
			redirect('user/login');
		}
	}

	public function index()
	{
		$data['branches'] 	= $this->StatusModel->branches();
		$data['classes'] 	= $this->StatusModel->classes();

		$this->load->view('index', $data);
	}

	public function status($centreID, $classID, $batchID, $subjectID)
	{
		$chapters 	= $this->StatusModel->chapters($subjectID);

		$chapterStatus = array();
		for ($i=0; $i < count($chapters); $i++) 
		{ 
			$chapterStatus[$i] 			= $chapters[$i];
			$chapterStatus[$i]->chapstatus 	= $this->StatusModel->status($centreID, $classID, $batchID, $subjectID, $chapters[$i]->chapter_id);
			$chapterStatus[$i]->chapstatus 	= $this->StatusModel->status($centreID, $classID, $batchID, $subjectID, $chapters[$i]->chapter_id);
			$chapterStatus[$i]->chapcompletionstatus 	= $this->StatusModel->completion_status($centreID, $classID, $batchID, $subjectID, $chapters[$i]->chapter_id);
		}
		$data['branches'] 	= $this->StatusModel->branches();
		$data['classes'] 	= $this->StatusModel->classes();
		$data['batches'] 	= $this->StatusModel->batches($centreID, $classID);
		$data['subjects'] 	= $this->StatusModel->subjects($classID);
		$data['chapters'] 	= $chapterStatus;

		$this->load->view('index', $data);
	}
}