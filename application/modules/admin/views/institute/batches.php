   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-university"></i> Batches</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Course</a></li>
          <li class="breadcrumb-item"><a href="#">Batches</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4><?php echo $class->class; ?> - Batches</h4>

              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newBatch" >Add Batch</button><br><br>

              <table id="batches" name="batches" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Center</td>
                    <td>Batch</td>
                    <td>Batch Code</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($batches as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->branch; ?></td>
                    <td><?php echo $data->batch; ?></td>
                    <td><?php echo $data->batch_code; ?></td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editBatch" data-batchid="<?php echo $data->batch_id; ?>" data-batch="<?php echo $data->batch; ?>" data-branchid="<?php echo $data->branch_id; ?>" data-subjects="<?php echo str_replace('"', "'", $data->subjects); ?>"  >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('institute/batch/delete/'.$class->class_id.'/'.$data->batch_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure To Delete ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newBatch">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Batch</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/batch/save/'.$class->class_id, 'id="savebatch"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Batch</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="batch" id="batch" placeholder="Batch">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Center</label>
              <div class="col-md-8">
                <select class="form-control" name="branch" id="branch" >
                  <?php foreach($branches as $data) { ?>  
                    <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>            
            <div class="form-group row">
              <label class="control-label col-md-3">Subjects</label>
              <div class="col-md-8">
                <select class="form-control subjects" name="subjects[]" id="subjects" multiple="" >
                  <?php foreach($subjects as $data) { ?>  
                    <option value="<?php echo $data->subject_id; ?>" ><?php echo $data->subject; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>     
    <!-- The Modal -->
    <div class="modal" id="editBatch">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Batch</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/batch/update/'.$class->class_id, 'id="updatebatch"'); ?>
          <div class="modal-body">
            <input type="hidden" name="batchid" id="batchid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Batch</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="batch" id="_batch" placeholder="Batch">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Branch</label>
              <div class="col-md-8">
                <select class="form-control" name="branch" id="_branch" >
                  <?php foreach($branches as $data) { ?>  
                    <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Subjects</label>
              <div class="col-md-8">
                <select class="form-control subjects" name="subjects[]" id="_subjects" multiple="" >
                  <?php foreach($subjects as $data) { ?>  
                    <option value="<?php echo $data->subject_id; ?>" ><?php echo $data->subject; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(".edit").on('click', function(){
        $("#batchid").val($(this).data('batchid'));
        $("#_batch").val($(this).data('batch'));
        $("#_branch option:selected").removeAttr('selected');
        $("#_branch option[value=" + $(this).data('branchid') + "]").attr('selected', true);

        subjects = $(this).data('subjects');
        subjects = subjects.replace(/\'/g, "\"");
        $("#_subjects option:selected").removeAttr('selected');
        data     = $.parseJSON(subjects);
        $.each(data, function(i, item) {
            //console.log(data[i]);
            $("#_subjects option[value=" + data[i] + "]").attr('selected', true);
        });

      });
      $("#batches").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Batches of <?php echo $class->class; ?>"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Batches of <?php echo $class->class; ?>"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Batches of <?php echo $class->class; ?>"
          }
        ],
        "stateSave":true
      });

      $("#savebatch").validate({
        rules:{
          batch:{
            required:true
          },
          branch:{
            required:true
          },
          subjects:{
            required:true
          }
        },
        messages:{
          batch:{
            required:"Please enter Batch"
          },
          branch:{
            required:"Please choose Center"
          },
          subjects:{
            required:"Please choose subjects"
          }
        }
      });      

      $("#updatebatch").validate({
        ignore:"",
        rules:{
          batchid:{
            required:true
          },
          batch:{
            required:true
          },
          branch:{
            required:true
          },
          subjects:{
            required:true
          }
        },
        messages:{
          batch:{
            required:"Please enter Batch"
          },
          branch:{
            required:"Please choose Branch"
          },
          subjects:{
            required:"Please choose subjects"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>