   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-file"></i> Subjects</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Subjects</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4>Subjects</h4>

              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newSubject" >Add Subject</button><br><br>

              <table id="subjects" name="subjects" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Course</td>
                    <td>Subject</td>
                    <td>Chapters</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($subjects as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->class; ?></td>
                    <td><?php echo $data->subject; ?></td>
                    <td>
                      <a class="btn btn-sm btn-success" href="<?php echo base_url('institute/chapters/'.$data->class_id.'/'.$data->subject_id); ?>">Chapters</a>
                    </td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editSubject" data-subjectid="<?php echo $data->subject_id; ?>" data-classid="<?php echo $data->class_id; ?>" data-subject="<?php echo $data->subject; ?>"   >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('institute/subject/delete/'.$data->subject_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure To Delete ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newSubject">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Subject</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/subject/save/', 'id="savesubject"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Course</label>
              <div class="col-md-8">
                <select class="form-control" name="class" id="class" >
                  <?php foreach($classes as $data) { ?>  
                    <option value="<?php echo $data->class_id; ?>" ><?php echo $data->class; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Subject</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="subject" id="subject" placeholder="Subject">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>     
    <!-- The Modal -->
    <div class="modal" id="editSubject">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Subject</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/subject/update/', 'id="updatesubject"'); ?>
          <div class="modal-body">
            <input type="hidden" name="subjectid" id="subjectid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Course</label>
              <div class="col-md-8">
                <select class="form-control" name="class" id="_class" >
                  <?php foreach($classes as $data) { ?>  
                    <option value="<?php echo $data->class_id; ?>" ><?php echo $data->class; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Subject</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="subject" id="_subject" placeholder="Subject">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(".edit").on('click', function(){
        $("#subjectid").val($(this).data('subjectid'));
        $("#_class option:selected").removeAttr('selected');
        $("#_class option[value=" + $(this).data('classid') + "]").attr('selected', true);
        $("#_subject").val($(this).data('subject'));
      });
      $("#subjects").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Subjects"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Subjects"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Subjects"
          }
        ],
        "stateSave":true
      });

      $("#savesubject").validate({
        rules:{
          class:{
            required:true
          },
          subject:{
            required:true
          }
        },
        messages:{
          class:{
            required:"Please choose Course"
          },
          subject:{
            required:"Please enter Subject"
          }
        }
      });      
     
     $("#updatesubject").validate({
        ignore:"",
        rules:{
          subjectid:{
            required:true
          },
          class:{
            required:true
          },
          subject:{
            required:true
          }
        },
        messages:{
          class:{
            required:"Please choose Course"
          },
          subject:{
            required:"Please enter Subject"
          }
        }
      });      

    </script>
    <?php $this->load->view('import/footer'); ?>