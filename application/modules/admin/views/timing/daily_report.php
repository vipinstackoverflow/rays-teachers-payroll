   <?php $this->load->view('import/header'); ?>
   <style>
#Table {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 70%;
}

#Table td, #Table th {
  border: 1px solid #ddd;
  padding: 8px;
}

#Table tr:nth-child(even){background-color: #f2f2f2;}

#Table tr:hover {background-color: #ddd;}

#Table th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #009688;
  color: white;
}
</style>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><img src="<?php echo base_url();?>assets/clipboard.png"> Daily Report</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">approve-timing</a></li>
          <li class="breadcrumb-item"><a href="#">daily-report</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="row">
                   <div class="col-sm-4 col-md-4 col-lg-4">
                      <div class="form-group">
                        
                     </div>
                   </div>
                   <div class="col-sm-3 col-md-3 col-lg-3">
                      <div class="form-group">
                       <div class="form-group" >
                        <label >Date</label>
                        <input type="text" name="tdate" id="tdate" class="form-control" value="<?php echo $date?>" >
                     </div>
                   </div>
                 </div>
                    <div class="col-sm-5 col-md-5 col-lg-5">
                      
                   </div>
                   </div>
                   <div class="row">
                 <div class="col-sm-12 col-md-12 col-lg-12">
                   <table class="table table-hover table-bordered" style="margin-left:141px;" id="Table">
                <thead>
                  <tr>
                    <th>Sl NO</th>
                     <th>Batches</th>
                     <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 <?php foreach($batches as $key=> $batchs) { ?>
                        <tr>
                           <td><?php echo $key+1?></td>
                           <td><?php echo $batchs->batch; ?></td>
                           <td>
                            <!--   <a class="card-link" onclick="ajaxFunction(<?php echo $batchs->batch_id;?>)">
                                <i class="btn btn-sm btn-success" >View Details</i>
                              </a> -->
                               <a class="card-link" href="<?php echo base_url('timing-requests/daily-report/'.$batchs->batch_id.'/'.$date); ?>">
                                <i class="btn btn-sm btn-success" >View Details</i>
                              </a>
                              <!-- <a class="card-link" href="">
                                <i class="btn btn-sm btn-success" >View Faculty</i>
                              </a> -->
                          </td>
                        </tr>
                    <?php } ?>
                </tbody>
              </table>
           
                 </div>
              </div>
                </div>
              
            </div>
              
         </div>
         </div>
         </div> 
         <div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -20px;">
           <div class="modal-dialog modal-lg">
             <div class="modal-content">
              <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Bc</h4>
            </div>
           <div class="modal-body">
           <!-- Place to print the fetched phone -->
            <div id="phone_result"></div>
           </div>
          <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
 </div>
<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -20px;">
           <div class="modal-dialog modal-lg">
             <div class="modal-content">
              <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Faculty</h4>
            </div>
           <div class="modal-body">
           <!-- Place to print the fetched phone -->
            <div id="phone_result"></div>
           </div>
          <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
 </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/time-picker/mdtimepicker.min.js'); ?>"></script>
   
    <script type="text/javascript">
      $(".time").mdtimepicker({
        timeFormat:'hh:mm.ss',
        format:'h:mmtt'
      });

      $("#tdate").datepicker({
        format:"dd-mm-yyyy",
        autoclose:true
      });

      $("#tdate").on('change', function(){
        date  = $(this).val(); 
        // alert(date);
        window.location = "<?php echo base_url('timming-request/daily-report/') ?>"+date;
      });

    </script>
    <script type="text/javascript">
      function ajaxFunction(id){ 
        alert(id);
         if(id !=0){

               $.ajax({
                  url: '<?php echo base_url();?>view_bc',
                  type: 'POST',
                  data: { id : id},
                  success: function (response) {
            // if(response){
            //       $('#table-data').html('');
            //       $('#table-data').html(response);
 
            //   }
            }
        });

      }
    }
    </script>
    <?php $this->load->view('import/footer'); ?>