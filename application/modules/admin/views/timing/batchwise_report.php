   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><img src="<?php echo base_url();?>assets/report.png"></i>Batch Wise Report</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
         <li class="breadcrumb-item"><a href="#">approve-timing</a></li>
          <li class="breadcrumb-item"><a href="#">Batch-wise-report</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <div class="row">
               <!--  <div class="col-md-6">
                  <div class="tile">
                    <div class="tile-title-w-btn"> -->
                     <!--  <h3 class="title"><?php echo $facultymeta->faculty_code."-".$facultymeta->name; ?></h3> -->
                     <!--  <?php if(empty($approved)) { ?>
                      <span class="badge badge-danger">Pending</span>
                      <?php } else { ?>
                      <span class="badge badge-success">Approved</span>
                    <?php } ?> -->
                <!--     </div>
                  </div>
                </div> -->
               <!--  <div class="col-md-6">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <input type="text" class="form-control" name="tdate" id="tdate" value="<?php echo $this->uri->segment(3); ?>" >
                    </div>
                  </div>
                </div> -->
                <div class="col-md-4">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">BC</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>

                          <?php if(count($bc)){
                          foreach($bc as $data) { 
                               
                            ?> 
                          <tr>
                           
                            <td><?php echo $data->name."-".date('h:ia', strtotime($data->start_time))."-".date('h:ia', strtotime($data->end_time))."-".$data->chapter_code."(".$data->chapter_status.")"
                           
                            ?>
                              <?php if(!empty($data->meta)){?>
                              <?php echo "("."<b>".$data->meta[0]->faculty_code."</b>".")"?>
                             <?php }?>

                            </td>
                           
                          </tr>

                           
                          <?php } ?>
                        <?php } else{?>
                           <tr>
                            <td><b><centre>No Data Found</centre></b></td>
                            </tr>
                           <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div> -->
                  </div>
                </div>                
                <div class="col-md-4">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">Faculty</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php if(count($faculty)){
                          foreach($faculty as $data) { ?>
                          <tr>
                            <td><?php echo "<b>".$data->faculty_code."</b>"."-".date('h:ia', strtotime($data->start_time))."-".date('h:ia', strtotime($data->end_time))."-".$data->chapter_code."(".$data->chapter_status.")"; ?></td>
                          </tr>
                          <?php } ?>
                        <?php }
                        else{?>
                           <tr>
                            <td><b><centre>No Data Found</centre></b></td>
                            </tr>
                           <?php } ?> 
                        </tbody>
                      </table>
                    </div>
                    <!-- <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div> -->
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">TT</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php if(count($timetable) > 0) { 
                             foreach($timetable as $data) { ?>
                             <tr>
                               <td><?php echo date('h:ia', strtotime($data->start_time))."-".date('h:ia', strtotime($data->end_time))."-"."<b>".$data->faculty_code."</b>"; ?></td>
                             </tr>
                            <?php } ?>
                          <?php } 
                          else {?>  
                          
                            <tr>
                            <td><b><centre>No Data Found</centre></b></td>
                            </tr>
                           <?php } ?> 
                        </tbody>
                      </table>
                    </div> 
                    <!-- <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div> -->
                 <!--  </div>
                </div> --> 
               
        
            <!-- <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div> -->
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/time-picker/mdtimepicker.min.js'); ?>"></script>

    <script type="text/javascript">

      $(".time").mdtimepicker({
        timeFormat:'hh:mm.ss',
        format:'h:mmtt'
      });

      $("#tdate").datepicker({
        format:"dd-mm-yyyy",
        autoclose:true
      });

      // $("#tdate").on('change', function(){
      //   fid   = "<?php echo $this->uri->segment(2); ?>";
      //   date  = $(this).val(); 
      //   window.location = "<?php echo base_url('timing-requests/') ?>"+fid+"/"+date;
      // });

    </script>
    <?php $this->load->view('import/footer'); ?>