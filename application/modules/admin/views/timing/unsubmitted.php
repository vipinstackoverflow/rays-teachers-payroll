<?php 

    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=Unsubmitted-".$date.".xls");
    header("Pragma: no-cache");
    header("Expires: 0");

?>
<table border="1" >
	<tr>
		<td>Short</td>
		<td>Name</td>
		<td>Phone</td>
	</tr>
	<?php foreach($faculties as $data) { ?>
	<tr>
		<td><?php echo $data->faculty_code; ?></td>
		<td><?php echo $data->name; ?></td>
		<td><?php echo $data->phone; ?></td>
	</tr>
	<?php } ?>
</table>