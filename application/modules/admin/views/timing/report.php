   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-search"></i> Report</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Timing Requests</a></li>
          <li class="breadcrumb-item"><a href="#">Report</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4>Generate Consolidated Report</h4>
            <?php echo form_open('timing-requests/search-consolidated-report'); ?>
            <div class="row" >         
              <div class="col-md-6" >
                <div class="form-group row">
                  <label class="control-label col-md-3">Date Range</label>
                  <div class="col-md-8">
                    <input type="hidden" name="daterange" id="daterange" class="form-control" >
                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6" >
              </div>
            </div>
            <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Generate</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <?php if(isset($faculties)) { ?>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <h4>Report - <?php echo @$date; ?></h4>
              <table id="approved" name="approved" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Amount</td>
                    <td>A/C</td>
                    <td>IFSC</td>
                    <td>Bank</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; $payment = 0; foreach($faculties as $data) { $i++; $payment += $data->report->amount; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->faculty_code; ?></td>
                    <td><?php echo $data->report->amount; ?></td>
                    <td><?php echo $data->ac_number; ?></td>
                    <td><?php echo $data->ifsc; ?></td>
                    <td><?php echo $data->bank_name; ?></td>
                  </tr>
                <?php } ?>
                  <tr>
                    <td><?php echo $i+1; ?></td>
                    <td>Total</td>
                    <td><?php echo $payment; ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(function() {

          var start = moment().month(0).startOf('month');
          var end = moment();

          function cb(start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              $('#daterange').val(start.format('YYYY/MM/DD') + '-' + end.format('YYYY/MM/DD'));
          }

          $('#reportrange').daterangepicker({
              startDate: start,
              endDate: end,
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                 'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                 'This Year': [
                      moment() // 2017-01-16
                          .month(0) // 2017-04-16
                          .startOf('month'), // 2016-04-01
                      
                      moment() // 2017-01-16
                          .month(12) // 2017-04-16
                          .startOf('month') // 2017-04-01
                          .subtract(1, 'days') // 2017-03-31
                  ]
              }
          }, cb);

          cb(start, end);

      });
      $("#approved").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Report <?php echo @$date; ?>"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Report <?php echo @$date; ?>"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Report <?php echo @$date; ?>"
          }
        ]
      });
    </script>
    <?php $this->load->view('import/footer'); ?>