   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-list"></i> Slabs</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Faculty</a></li>
          <li class="breadcrumb-item"><a href="#">Slabs</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?>
              <h4><?php echo $faculty->name; ?></h4>
              
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newSlabType" >Add Slab</button><br><br>
              <table id="slabs" name="slabs" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Slab Type</td>
                    <td>Rate</td>
                    <td>Extra</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($slabs as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->slab_type; ?></td>
                    <td><?php echo $data->rate; ?></td>
                    <td><?php echo $data->extra; ?></td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editSlab" data-slabid="<?php echo $data->slab_id; ?>" data-slabtypeid="<?php echo $data->slab_type_id; ?>" data-rate="<?php echo $data->rate; ?>" data-extra="<?php echo $data->extra; ?>" >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('slab/delete-slab/'.$faculty->faculty_id.'/'.$data->slab_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newSlabType">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Slab</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('slab/save-slab/'.$faculty->faculty_id, 'id="saveslab"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Slab Type</label>
              <div class="col-md-8">
                <select class="form-control" name="slabtype" >
                  <?php foreach($slabtypes as $data) { ?>
                  <option value="<?php echo $data->slab_type_id; ?>" ><?php echo $data->slab_type; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Rate</label>
              <div class="col-md-8">
                <input class="form-control" type="number" name="rate" id="rate" placeholder="Rate">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Extra</label>
              <div class="col-md-8">
                <input class="form-control" type="number" name="extra" id="extra" placeholder="Extra">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <!-- The Modal -->
    <div class="modal" id="editSlab">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Slab</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('slab/update-slab/'.$faculty->faculty_id, 'id="updateslab"'); ?>
          <div class="modal-body">
            <input type="hidden" name="slabid" id="slabid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Slab Type</label>
              <div class="col-md-8">
                <select class="form-control" name="slabtype" id="_slab_type" >
                  <?php foreach($slabtypes as $data) { ?>
                  <option value="<?php echo $data->slab_type_id; ?>" ><?php echo $data->slab_type; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Rate</label>
              <div class="col-md-8">
                <input class="form-control" type="number" name="rate" id="_rate" placeholder="Rate">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Extra</label>
              <div class="col-md-8">
                <input class="form-control" type="number" name="extra" id="_extra" placeholder="Extra">
              </div>
            </div>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(".edit").on('click', function(){
        $("#slabid").val($(this).data('slabid'));
        $("#_slab_type option:selected").removeAttr('selected');
        $("#_slab_type option[value="+$(this).data('slabtypeid')+"]").attr('selected', true);
        $("#_rate").val($(this).data('rate'));
        $("#_extra").val($(this).data('extra'));
      });
      $("#slabs").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1]
            },
            title:"Slab Types"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1]
            },
            title:"Slab Types"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1]
            },
            title:"Slab Types"
          }
        ],
        "stateSave":true
      });

      $("#saveslab").validate({
        rules:{
          slab_type:{
            required:true
          }
        },
        messages:{
          slab_type:{
            required:"Please enter Slab Type"
          }
        }
      });      

      $("#updateslab").validate({
        rules:{
          slab_type:{
            required:true
          }
        },
        messages:{
          slab_type:{
            required:"Please enter Slab Type"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>