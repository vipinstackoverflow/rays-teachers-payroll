   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Faculty</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Faculty</a></li>
          <li class="breadcrumb-item"><a href="#">Register</a></li>
        </ul>
      </div>
      <?php echo form_open_multipart('faculty/register', 'id="register"') ?>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
            <div class="row" >             
              <div class="col-md-6" >

                <div class="form-group row">
                  <label class="control-label col-md-3">Faculty Code</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="facultycode" id="facultycode" placeholder="Faculty Code eg:AS">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Batches</label>
                  <div class="col-md-8">
                    <select name="batches[]" id="batches" class="form-control" >
                      <?php foreach($batches as $data) { ?>
                        <option value="<?php echo $data->batch_id; ?>" ><?php echo $data->batch; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Subject</label>
                  <div class="col-md-8">
                    <select name="subject" id="subject" class="form-control" >
                      <option value="Physics" >Physics</option>
                      <option value="Chemistry" >Chemistry</option>
                      <option value="Maths" >Maths</option>
                      <option value="Biology" >Biology</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Name</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="name" id="name" placeholder="Enter full name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-8">
                    <input class="form-control" type="email" name="email" id="email" placeholder="Enter email address">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Phone</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Address</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" name="address" id="address" placeholder="Enter your current address"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Account Holder</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="ac_holder" id="ac_holder" placeholder="Account Holder Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Account Number</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="ac_number" id="ac_number" placeholder="Account Number">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">IFSC</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="ifsc" id="ifsc" placeholder="IFSC">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Bank Name</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="bank" id="bank" placeholder="Bank Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Pan Number</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="pan" id="pan" placeholder="Pan Number">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Image</label>
                  <div class="col-md-8">
                    <input type="file" class="form-control" name="image" id="image" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Center</label>
                  <div class="col-md-8">
                    <select class="form-control" id="branch" name="branch" >
                      <?php foreach($branches as $data) { ?>
                      <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6" >
              </div>
            </div>
            <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register Faculty</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/multiselect/kendo.all.min.js'); ?>"></script>

    <script type="text/javascript">
      var editbc = $("#batches").kendoMultiSelect({
          autoClose: false
      }).data("kendoMultiSelect");
      $("#register").validate({
        rules:{
          facultycode:{
            required: true
          },
          name:{
            required: true
          },
          phone:{
            required:true
          },
          branch:{
            required:true
          }
        },
        messages:{
          facultycode:{
            required:"Please enter Faculty Code"
          },
          name:{
            required:"Please enter Faculty Name"
          },
          phone:{
            required:"Please enter Phone Number"
          },
          branch:{
            required:"Please select Branch"
          }
        }
      });

      $("#regdate").datepicker({
        format:"yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
      });
    </script>
    <?php $this->load->view('import/footer'); ?>