   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Investors</h1>
          <p>Apex Investors</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Password</a></li>
          <li class="breadcrumb-item"><a href="#">Change Password</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="tile">
          <?php if($this->session->flashdata('success')) { ?>
          <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true" style="font-size:20px">×</span>
            </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
          </div>
          <?php } ?>
          <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger fade in alert-dismissible show">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true" style="font-size:20px">×</span>
            </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
          </div>
          <?php } ?>
            <h3 class="tile-title">Change Password</h3>
            <div class="tile-body">
              <?php echo form_open('password/update-password', 'id="change"') ?>
                <div class="form-group row">
                  <label class="control-label col-md-3">Current Password</label>
                  <div class="col-md-8">
                    <input class="form-control" type="password" name="current_pass" id="current_pass" placeholder="Current Password">
                  </div>
                </div>                
                <div class="form-group row">
                  <label class="control-label col-md-3">New Password</label>
                  <div class="col-md-8">
                    <input class="form-control" type="password" name="new_pass" id="new_pass" placeholder="New Password">
                  </div>
                </div>                
                <div class="form-group row">
                  <label class="control-label col-md-3">Confirm Password</label>
                  <div class="col-md-8">
                    <input class="form-control" type="password" name="password" id="password" placeholder="Confirm Password">
                  </div>
                </div>
            </div>
            <div class="tile-footer">
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $("#register").validate({
        rules:{
          name:{
            required:true
          },
          email:{
            required:true,
            email:true
          },
          phone:{
            required:true
          },
          gender:{
            required:true
          },
          address:{
            required:true
          },
          state:{
            required:true
          },
          regdate:{
            required:true
          },
          investment:{
            required:true,
            number:true,
            min:0
          }
        },
        messages:{
          name:{
            required:"Name can't be empty"
          },
          email:{
              required:"Email can't be empty"
          },
          phone:{
            required:"Phone can't be empty"
          },
          gender:{
            required:"Please select gender"
          },
          address:{
            required:"Please enter address"
          },
          state:{
            required:"Please select State"
          },
          regdate:{
            required:"Please choose Reg.Date"
          }
        }
      });

      $("#regdate").datepicker({
        format:"yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
      });
    </script>
    <?php $this->load->view('import/footer'); ?>