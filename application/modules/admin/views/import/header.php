<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Rays Faculty Payroll</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url('assets/admin/images/favicon.ico'); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('assets/admin/images/favicon.ico'); ?>" type="image/x-icon">
    <meta name="robots" content="noindex">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/main.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/dataTable/css/jquery.dataTables.min.css'); ?>">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/dataTable/css/buttons.dataTables.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/easy-autocomplete.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/datepicker/css/datepicker.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/daterangepicker/css/jquery.comiseo.daterangepicker.css'); ?>">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/fullcalendar.min.css'); ?>">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/jquery.timepicker.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/multiselect/kendo.common-material.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/multiselect/kendo.material.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/multiselect/kendo.material.mobile.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/MonthPicker.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/daterangepicker/css/daterangepicker.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/timepicker/jquery.timepicker.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/time-picker/mdtimepicker.min.css'); ?>">  
  </head>
  <body class="app sidebar-mini rtl">
    <header class="app-header"><a class="app-header__logo" href="<?php echo base_url(); ?>">Faculty PayRoll</a>
      <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <ul class="app-nav">
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<?php echo base_url('password/change-password'); ?>"><i class="fa fa-key fa-lg"></i> Change Password</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('user/logout'); ?>"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
        <?php $menu        = $this->uri->segment(1); ?>
        <?php $permissions = json_decode($this->session->permissions); ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo base_url('assets/admin/images/avatar.png'); ?>" width="50" alt="User Image">
        <div>
          <p class="app-sidebar__user-name"><?php echo $this->session->username; ?></p>
          <p class="app-sidebar__user-designation">Faculty PayRoll</p>
        </div>
      </div>
      <ul class="app-menu">
        <?php if($this->session->usertype == '1') { ?>
        <li>
            <a class="app-menu__item <?php if($menu == 'user') { ?> active <?php } ?>" href="<?php echo base_url('admin/userpanel'); ?>">
              <i class="app-menu__icon fa fa-dashboard"></i>
              <span class="app-menu__label">Dashboard</span>
            </a>
        </li>
        <?php if(in_array('1', $permissions)) { ?>
        <li class="treeview <?php if($menu == 'institute') { ?> is-expanded <?php } ?>">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-university"></i>
            <span class="app-menu__label">Institute</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="<?php echo base_url('institute/centers'); ?>">
                <i class="icon fa fa-circle-o"></i> Centers
              </a>
            </li>
            <li>
              <a class="treeview-item" href="<?php echo base_url('institute/courses'); ?>">
                <i class="icon fa fa-circle-o"></i> Courses
              </a>
            </li>
            <li>
              <a class="treeview-item" href="<?php echo base_url('institute/subjects'); ?>">
                <i class="icon fa fa-circle-o"></i> Subjects
              </a>
            </li>
          </ul>
        </li>
        <?php } ?> 
        <?php if(in_array('2', $permissions)) { ?>       
        <li class="treeview <?php if($menu == 'faculty') { ?> is-expanded <?php } ?>">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-users"></i>
            <span class="app-menu__label">Faculty</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="<?php echo base_url('faculty/registration'); ?>">
                <i class="icon fa fa-circle-o"></i> Register
              </a>
            </li>
            <li>
              <a class="treeview-item" href="<?php echo base_url('faculty/faculties'); ?>">
                <i class="icon fa fa-circle-o"></i> Faculty List
              </a>
            </li>
            <li>
              <a class="treeview-item" href="<?php echo base_url('faculty/bc'); ?>">
                <i class="icon fa fa-circle-o"></i> Batch In Charge
              </a>
            </li>
          </ul>
        </li>        
      <?php } ?>
      <?php if(in_array('3', $permissions) || $this->session->userid == '157') { ?>
        <li class="treeview <?php if($menu == 'slab') { ?> is-expanded <?php } ?>">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-list"></i>
            <span class="app-menu__label">Slabs</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="<?php echo base_url('slab/slab-types'); ?>">
                <i class="icon fa fa-circle-o"></i> Slab Types
              </a>
            </li>            
            <li>
              <a class="treeview-item" href="<?php echo base_url('slab/slabs'); ?>">
                <i class="icon fa fa-circle-o"></i> Slabs
              </a>
            </li>
          </ul>
        </li>
        <?php } ?>
        <?php if(in_array('4', $permissions) || $this->session->userid == '157') { ?>
        <li class="treeview <?php if($menu == 'timetable') { ?> is-expanded <?php } ?>">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-list-alt"></i>
            <span class="app-menu__label">Time Table</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="<?php echo base_url('timetable/upload'); ?>">
                <i class="icon fa fa-circle-o"></i> Upload TimeTable
              </a>
            </li>
          </ul>
        </li>
        <?php } ?>
        <?php if(in_array('5', $permissions)) { ?>
        <li class="treeview <?php if($menu == 'timing-requests') { ?> is-expanded <?php } ?>">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-clock-o"></i>
            <span class="app-menu__label">Approve Timing</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="<?php echo base_url('timing-requests/date/'.date('d-m-Y')); ?>">
                <i class="icon fa fa-circle-o"></i> All Requests
              </a>
            </li>
            <?php if($this->session->userid != '185'){?>
            <li>
              <a class="treeview-item" href="<?php echo base_url('timing-requests/approved-report'); ?>">
                <i class="icon fa fa-circle-o"></i> Report
              </a>
            </li>
            <li>
              <a class="treeview-item" href="<?php echo base_url('timing-requests/consolidated-report'); ?>">
                <i class="icon fa fa-circle-o"></i> Consolidated Report
              </a>
            </li>
             <li>
              <a class="treeview-item" href="<?php echo base_url('timing-requests/daily-report'); ?>">
                <i class="icon fa fa-circle-o"></i>Daily Report
              </a>
            </li>
          <?php } ?>
          </ul>
        </li>  
         <?php if($this->session->userid != '185'){?>
         <li><a class="app-menu__item" href="<?php echo base_url()?>report"><i class="app-menu__icon fa fa-pencil-square-o"></i><span class="app-menu__label">Report</span></a></li>    
        <?php } ?>
        <?php } ?>
        <?php if(in_array('6', $permissions)) { ?>
        <li class="treeview <?php if($menu == 'users') { ?> is-expanded <?php } ?>">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-user-secret"></i>
            <span class="app-menu__label">Administrator</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="treeview-item" href="<?php echo base_url('user/users'); ?>">
                <i class="icon fa fa-circle-o"></i> Administrators
              </a>
            </li>
          </ul>
        </li>
        <?php } ?>
      <?php } ?>
      </ul>
    </aside>