<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyRegisterController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('FacultyRegisterModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('2', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function registration()
	{
		$data['branches'] 	= $this->FacultyRegisterModel->branches();
		$data['batches'] 	= $this->FacultyRegisterModel->batches();

		$this->load->view('faculty/register', $data);
	}

	/**
	* Register faculty with their information
	* Validate and Insert Faculty details
	*/
	public function register()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('facultycode', 'Faculty Code', 'required|xss_clean|trim|is_unique[faculties.faculty_code]');
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim');
		$this->form_validation->set_rules('branch', 'Branch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('phone', 'Branch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('batches[]', 'Batches', 'required|xss_clean|trim');
		$this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$faculty['faculty_code'] 	= strtoupper($this->input->post('facultycode'));
			$faculty['name'] 			= $this->input->post('name');
			$faculty['email'] 			= $this->input->post('email');
			$faculty['phone'] 			= $this->input->post('phone');
			$faculty['address'] 		= $this->input->post('address');
			$faculty['ac_holder'] 		= $this->input->post('ac_holder');
			$faculty['ac_number'] 		= $this->input->post('ac_number');
			$faculty['ifsc'] 			= $this->input->post('ifsc');
			$faculty['bank_name'] 		= $this->input->post('bank');
			$faculty['pan_no'] 			= $this->input->post('pan');
			$faculty['branch_id'] 		= $this->input->post('branch');
			$faculty['batches'] 		= json_encode($this->input->post('batches'));
			$faculty['subject'] 		= $this->input->post('subject');
			$faculty['faculty_type_id']	= '1';

			$config['upload_path'] 		= './resources/images/';
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['max_size'] 		= '10000';

			$this->load->library('upload', $config);

			//create folder if does not exist
			if(!is_dir('./resources/images/'))
			{
				mkdir('./resources/images/', 0777 ,TRUE);
			}

			if($this->upload->do_upload('image'))
			{
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$faculty['image'] = $upload_data['file_name'];
			}

			$this->db->trans_start();

				$facultyID = $this->FacultyRegisterModel->register($faculty);
				$this->FacultyRegisterModel->register_user($faculty, $facultyID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Registered successfully'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('faculty/registration');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('faculty/registration');
		}
	}
}