<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SubjectController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('SubjectModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('1', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function subjects()
	{
		$data['classes'] 	= $this->SubjectModel->classes();
		$data['subjects'] 	= $this->SubjectModel->subjects();

		$this->load->view('institute/subjects', $data);
	}

	public function save()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');
		$this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$subject['class_id'] 	= $this->input->post('class');
			$subject['subject'] 	= $this->input->post('subject');

			$this->db->trans_start();

				$this->SubjectModel->save($subject);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Added'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/subjects/');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/subjects/');
		}
	}	

	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('subjectid', 'Subject ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');
		$this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$subjectID 				= $this->input->post('subjectid');
			$subject['class_id'] 	= $this->input->post('class');
			$subject['subject'] 	= $this->input->post('subject');

			$this->db->trans_start();

				$this->SubjectModel->update($subject, $subjectID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/subjects/');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/subjects/');
		}
	}

	public function delete($subjectID)
	{
		$this->db->trans_start();

			$this->SubjectModel->delete($subjectID);
		
		$this->db->trans_complete();


		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('institute/subjects/');
	}
}