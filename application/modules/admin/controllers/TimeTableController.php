<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimeTableController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('TimeTableModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		// else
		// {
		// 	$level = json_decode($this->session->permissions);

		// 	if( ! in_array('4', $level))
		// 	{
		// 		redirect('user/login');
		// 	}
		// }
	}

	public function upload()
	{
		$this->load->view('time-table/upload');
	}

	public function upload_excel()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('tdate', 'Date', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$date = date('Y-m-d', strtotime($this->input->post('tdate')));
			$this->load->library('excel');
		
			$config['upload_path'] 		= './resources/data/time-table/';
			$config['allowed_types'] 	= 'xlsx|xls';
			$config['max_size'] 		= '10000';

			$this->load->library('upload', $config);

			//create folder if does not exist
			if(!is_dir('./resources/data/time-table/'))
			{
				mkdir('./resources/data/time-table/', 0777 ,TRUE);
			}

			if($this->upload->do_upload('excel'))
			{
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

				$file_name 	= 	$upload_data['file_name']; //uploded file name
				$extension	=	$upload_data['file_ext'];  // uploded file extension
				
				//$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
		 		$objReader  = PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007 	  
		        //Set to read only
		        $objReader->setReadDataOnly(true); 		  
		        //Load excel file
				$objPHPExcel	=	$objReader->load('./resources/data/time-table/'.$file_name);		 
				$totalrows		=	$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
				$objWorksheet 	=	$objPHPExcel->setActiveSheetIndex(0);                
				
				//loop from first data untill last data

				$timeTableData = array();
				$faculty = '';
				for($i=2; $i <= $totalrows; $i++)
				{
					$batch_code 	= $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
					$faculty_code 	= $objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
					$faculty_code 	= ($faculty_code == 'NULL')? $faculty : $faculty_code;
					$faculty 		= $faculty_code;
					$chapter_code 	= $objWorksheet->getCellByColumnAndRow(2, $i)->getValue();
					$time 			= @explode('-', $objWorksheet->getCellByColumnAndRow(6, $i)->getValue());
					$start_time 	= @$time[0];
					$end_time 		= @$time[1];
					$time_left 		= $objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
					$slab_type_id 	= 0;
					
					/*$faculty_code 	= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
					$batch_code 	= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
					$chapter_code 	= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
					$start_time 	= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
					$end_time 		= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
					$time_left 		= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
					$slab_type_id 	= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();*/

					$timeTable = array(
										'slab_type_id'	=> $slab_type_id,
										'faculty_code' 	=> $faculty_code,
										'batch_code' 	=> $batch_code,
										'chapter_code' 	=> $chapter_code,
										'start_time' 	=> date('H:i:s', strtotime($start_time)),
										'end_time' 		=> date('H:i:s', strtotime($end_time)),
										'time_left'		=> $time_left,
										'_date'			=> $date
									);
					$timeTableData[] =	$timeTable;
				}

				$this->db->trans_start();

					//Delete Same Dated Data from DB
					$this->TimeTableModel->remove($date);
					//Save Time Table
					$this->TimeTableModel->save($timeTableData);

				$this->db->trans_complete();

				($this->db->trans_status() === FALSE)?
				$this->session->set_flashdata('error','Something went wrong ,pleas try again'):
				$this->session->set_flashdata('success','Uploaded successfully');

				@unlink('./resources/data/time-table/'.$file_name); //File Deleted After uploading in database .	

				redirect('timetable/upload');
			}
			else
			{
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('timetable/upload');				
			}
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('attendance/upload');
		}
	}
}