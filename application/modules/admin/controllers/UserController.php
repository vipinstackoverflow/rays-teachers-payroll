<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('UserModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('6', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function users()
	{
		$data['users'] 			= $this->UserModel->users();
		$data['permissions'] 	= $this->UserModel->permissions();

		$this->load->view('user/users',$data);
	}

	public function is_username_valid()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username','Username','required|xss_clean|trim|is_unique[users.username]');

		echo ($this->form_validation->run() == TRUE)? TRUE : FALSE;
	}

	public function register()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name','Name','required|xss_clean|trim');
		$this->form_validation->set_rules('username','Username','required|xss_clean|trim|min_length[5]|is_unique[users.username]');
		$this->form_validation->set_rules('newpassword','Password','required|xss_clean|trim|min_length[6]');
		$this->form_validation->set_rules('password','Password','required|xss_clean|trim|min_length[6]|matches[newpassword]');
		$this->form_validation->set_rules('permissions[]','Permission','required|xss_clean|trim');


		if($this->form_validation->run() == TRUE)
		{
			$this->load->library('bcrypt');

			$password 			= $this->input->post('password'); // plain text
			$permissions 		= json_encode($this->input->post('permissions')); // array

			$user['name'] 			 = $this->input->post('name');
			$user['username'] 		 = $this->input->post('username');
			$user['password'] 		 = $this->bcrypt->hash_password($password); //hashed
			$user['user_type_id'] 	 = '1'; //hashed

			$user['branch_id'] 	 	 = '1'; //hashed
			$user['permissions'] 	 = $permissions;
			
			$faculty['name'] 			= $this->input->post('name');
			$faculty['email'] 			= 'Nil';
			$faculty['phone'] 			= $this->input->post('username');
			$faculty['address'] 		= 'Nil';
			$faculty['branch_id'] 		= '1';
			$faculty['batches'] 		= $permissions;
			$faculty['faculty_type_id']	= '2';

			$this->db->trans_start();

				$facultyID = $this->UserModel->register_faculty($faculty);
				$user['faculty_id'] 	 = $facultyID; //hashed
				$this->UserModel->register($user);

			$this->db->trans_complete();

			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error','Something went wrong, Please try again later.'):
			$this->session->set_flashdata('success','User Created Successfully');

			redirect('user/users');
		}
		else
		{
			$this->session->set_flashdata('error',validation_errors());
			redirect('user/users');
		}
	}

	public function update_user_status($userID,$status)
	{
		($this->UserModel->update_user_status($userID, $status))?
		$this->session->set_flashdata('success','Updated Successfully'):
		$this->session->set_flashdata('error','Something went wrong , Please try again later.');

		redirect('user/users');
	}

	public function edit_user($userID)
	{
		$data['user'] 			= $this->UserModel->user_data($userID);
		$data['permissions'] 	= $this->UserModel->permissions();

		$this->load->view('user/edit-user',$data);
	}

	public function update_user($userID)
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name','Name','required|xss_clean|trim');
		$this->form_validation->set_rules('permissions[]','Permission','required|xss_clean|trim');


		if($this->form_validation->run() == TRUE)
		{
			$this->load->library('bcrypt');

			$password 			= $this->input->post('password'); // plain text
			$permissions 		= json_encode($this->input->post('permissions')); // array

			$user['name'] 		  = $this->input->post('name');
			$user['permissions'] = $permissions;

			if($password != "")
			{
				$user['password'] 		 = $this->bcrypt->hash_password($password); //hashed
			}
			
			$this->db->trans_start();

				$this->UserModel->update_user($userID,$user);

			$this->db->trans_complete();

			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error','Something went wrong, Please try again later.'):
			$this->session->set_flashdata('success','Updated Successfully');

			redirect('user/edit/'.$userID);
		}
		else
		{
			$this->session->set_flashdata('error',validation_errors());

			redirect('user/edit/'.$userID);
		}
	}
	public function delete_user($userID)
	{
		if($this->session->adminid == $userID)
		{
			$this->session->set_flashdata('error','You cant delete logged in account');
		}
		else
		{
			$this->UserModel->delete_user($userID);
			$this->session->set_flashdata('success','Deleted Successfully');
		}
		
		redirect('user/users');
	}
	
}
