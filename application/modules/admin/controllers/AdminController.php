<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		
		//$this->load->model('AdminModel');
	}

	public function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
		elseif ($this->session->usertype != '1') 
		{
			redirect('user/login');
		}
	}

	public function index()
	{
		$this->load->view('index');
	}
}
