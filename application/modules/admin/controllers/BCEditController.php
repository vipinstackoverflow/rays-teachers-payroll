<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BCEditController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('BCEditModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('1', $level))
			{
				redirect('user/login');
			}
		}
	}

	/**
	* Register faculty as Batch In Charge with their information
	* Validate and Insert Faculty details
	*/
	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim');
		$this->form_validation->set_rules('branch', 'Branch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('phone', 'Branch', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$facultyID 					= $this->input->post('facultyid');
			$faculty['name'] 			= $this->input->post('name');
			$faculty['email'] 			= $this->input->post('email');
			$faculty['phone'] 			= $this->input->post('phone');
			$faculty['address'] 		= $this->input->post('address');
			$faculty['branch_id'] 		= $this->input->post('branch');
			$faculty['batches'] 		= json_encode($this->input->post('batches'));
			$faculty['faculty_type_id']	= '2'; // Batch In Charge

			$this->db->trans_start();

				$this->BCEditModel->update($faculty, $facultyID);
				$this->BCEditModel->update_user($faculty, $facultyID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('faculty/bc');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('faculty/bc');
		}
	}

	public function edit($facultyID)
	{
		$data['bc'] 		= $this->BCEditModel->batch_in_charge($facultyID);
		$data['branches'] 	= $this->BCEditModel->branches();
		$data['batches'] 	= $this->BCEditModel->batches();
		
		$this->load->view('bc/view-bc', $data);
	}
}