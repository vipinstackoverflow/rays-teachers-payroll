<?php defined('BASEPATH') OR exit('NO direct script access allowed');

class SlabController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('SlabModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('3', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function faculties()
	{
		$data['faculties'] = $this->SlabModel->faculties();

		$this->load->view('slab/faculties', $data);
	}

	public function slabs($facultyID)
	{
		$data['faculty']	= $this->SlabModel->faculty($facultyID);
		$data['slabs'] 		= $this->SlabModel->slabs($facultyID);
		$data['slabtypes'] 	= $this->SlabModel->slabtypes();

		$this->load->view('slab/slabs', $data);
	}

	public function save($facultyID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('slabtype', 'Slab Type', 'required|xss_clean|trim');
		$this->form_validation->set_rules('rate', 'Rate', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$slab['faculty_id'] 	= $facultyID;
			$slab['slab_type_id'] 	= $this->input->post('slabtype');
			$slab['rate'] 			= $this->input->post('rate');
			$slab['extra'] 			= $this->input->post('extra');

			$this->db->trans_start();

				$this->SlabModel->save($slab);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Slab added');

			redirect('slab/slabs/'.$facultyID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('slab/slabs/'.$facultyID);
		}
	}	

	public function update($facultyID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('slabtype', 'Slab Type', 'required|xss_clean|trim');
		$this->form_validation->set_rules('rate', 'Rate', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$slabID 				= $this->input->post('slabid');
			$slab['slab_type_id'] 	= $this->input->post('slabtype');
			$slab['rate'] 			= $this->input->post('rate');
			$slab['extra'] 			= $this->input->post('extra');

			$this->db->trans_start();

				$this->SlabModel->update($slab, $slabID);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Slab added');

			redirect('slab/slabs/'.$facultyID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('slab/slabs/'.$facultyID);
		}
	}

	public function delete($facultyID, $slabID)
	{
		$this->db->trans_start();

			$this->SlabModel->delete($slabID);

		$this->db->trans_complete();
		
		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Slab deleted');

		redirect('slab/slabs/'.$facultyID);
	}
}