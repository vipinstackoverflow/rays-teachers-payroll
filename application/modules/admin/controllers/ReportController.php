<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReportController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('ReportModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('5', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function report()
	{
		
		$this->load->view('timing/report');
	}
    public function daily_report()
    {
    	$data['date'] =date('d-m-20y');
    	// print_r($data['date']);exit;
    	$data['batches']=$this->ReportModel->getBatches();
    	// print_r($data);exit;
    	$this->load->view('timing/daily_report',$data);
    }
     public function dailyreport($date)
    {
    	$data['date'] =$date;

    	$data['batches']=$this->ReportModel->getBatchesByDate($date);
    	
    	$this->load->view('timing/daily_report',$data);
    }
    public function viewBatchDetails($batchID, $date)
	{
		
		$date 				= date('Y-m-d', strtotime($date));
		$data['bc'] 		= $this->ReportModel->bc_requests($batchID, $date);
		// print_r($data['bc'] );exit;
		$data['faculty'] 	= $this->ReportModel->faculty_requests($batchID, $date);
		$data['timetable'] 	= $this->ReportModel->faculty_timetable($batchID, $date);

		$this->load->view('timing/batchwise_report.php', $data);
	}
    public function view_bc($id)
    {
    	
    	$id = $this->input->post('id');
    	print_r($id);exit;
    }
	public function search_report()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('daterange', 'Date Range', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$daterange = $this->input->post('daterange');

			$daterange = explode('-', $daterange);
			$from 	   = str_replace('/', '-', $daterange[0]);
			$to 	   = str_replace('/', '-', $daterange[1]);

			$faculties 			= $this->ReportModel->faculties();

			$reportData 		= array();
			for ($i=0; $i < count($faculties); $i++) 
			{ 
				$reportData[$i] 		= $faculties[$i];
				$reportData[$i]->report = $this->ReportModel->report($faculties[$i]->faculty_id, $from, $to);
			}

			$data['faculties'] 	= $reportData;
			$data['date'] 		= $from." - ".$to;

			$this->load->view('timing/report', $data);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('timing-requests/report');
		}
	}
}