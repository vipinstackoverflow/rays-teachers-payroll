<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyEditController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('FacultyEditModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('2', $level))
			{
				redirect('user/login');
			}
		}
	}

	/**
	* Register faculty with their information
	* Validate and Insert Faculty details
	*/
	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim');
		$this->form_validation->set_rules('branch', 'Branch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('phone', 'Branch', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$facultyID 					= $this->input->post('facultyid');
			$faculty['faculty_code']	= strtoupper($this->input->post('facultycode'));
			$faculty['name'] 			= $this->input->post('name');
			$faculty['email'] 			= $this->input->post('email');
			$faculty['phone'] 			= $this->input->post('phone');
			$faculty['address'] 		= $this->input->post('address');
			$faculty['branch_id'] 		= $this->input->post('branch');
			$faculty['ac_holder'] 		= $this->input->post('ac_holder');
			$faculty['ac_number'] 		= $this->input->post('ac_number');
			$faculty['ifsc'] 			= $this->input->post('ifsc');
			$faculty['bank_name'] 		= $this->input->post('bank');
			$faculty['pan_no'] 			= $this->input->post('pan');
			$faculty['batches'] 		= json_encode($this->input->post('batches'));
			$faculty['subject'] 		= $this->input->post('subject');

			$this->db->trans_start();

				$this->FacultyEditModel->update($faculty, $facultyID);
				$this->FacultyEditModel->update_user($faculty, $facultyID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('faculty/faculties');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('faculty/faculties');
		}
	}
}