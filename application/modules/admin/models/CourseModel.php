<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CourseModel extends CI_Model {

	public function courses()
	{
		$query = $this->db->get('classes');

		return $query->result();
	}

	public function save($class)
	{
		$this->db->insert('classes', $class);
	}

	public function update($class, $classID)
	{
		$this->db->where('class_id', $classID);
		$this->db->update('classes', $class);
	}
}