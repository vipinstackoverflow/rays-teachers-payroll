<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReportModel extends CI_Model {


    public function getBatches()
    {
      $sql="SELECT batches.batch,batches.batch_id FROM batches RIGHT JOIN timings ON batches.batch_id = timings.batch_id WHERE timings._date=CURRENT_DATE GROUP BY timings.batch_id ORDER BY batches.batch ASC";
      $batches = $this->db->query($sql);
      return $batches->result();
    }
    public function getBatchesByDate($date)
    {

		$this->db->where('timings._date', date('Y-m-d', strtotime($date)));
		$this->db->select('batches.batch,batches.batch_id');
		$this->db->join('batches', 'batches.batch_id = timings.batch_id', 'right');
		$this->db->group_by('timings.batch_id');
		$this->db->order_by("batches.batch ASC");
		$query = $this->db->get('timings');
		return $query->result();
    }
    public function bc_requests($batchID,$date)
    {
    	
      $this->db->select('timings.timing_id, timings.timing_uid, timings.time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.marker_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.chapter_status, timings.remarks, timings._date,faculties.name,faculties.faculty_code,timings.batch_id,timings._date,timings.faculty_id');
		$this->db->where('timings.batch_id', $batchID);
		$this->db->where('timings._date',$date);
		$this->db->where('faculty_types.faculty_type_id', '2');
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$query = $this->db->get('timings');
		$summaryData=array();
		if ($query->num_rows() > 0) 
         {
             $result = $query->result();

            for ($i=0; $i < count($result); $i++) 
             {
               $summaryData[$i] = $result[$i];
               $summaryData[$i]->meta = $this->facultys($result[$i]->batch_id,$date,$result[$i]->faculty_id);
             }
         }
		return $summaryData;
		
    }
    public function facultys($batchID,$date,$facultyID)
    {
    	 $this->db->select('faculties.faculty_code');
		$this->db->where('timings.batch_id', $batchID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculties.faculty_id', $facultyID);
		$this->db->where('faculty_types.faculty_type_id', '1');
		//$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->group_by('faculties.faculty_code');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$this->db->group_by('faculties.faculty_code');
		$query = $this->db->get('timings');
   // print_r($query->result());exit;
		return $query->result();
    }
    public function faculty_request($batchID,$date)
    {
     $this->db->select('timings.timing_id, timings.timing_uid, timings.time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.marker_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.chapter_status, timings.remarks, timings._date,faculties.faculty_code,timings.batch_id,timings._date');
		$this->db->where('timings.batch_id', $batchID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculty_types.faculty_type_id', '1');
		//$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->group_by('faculties.faculty_code');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$this->db->group_by('faculties.faculty_code');
		$query = $this->db->get('timings');
   // print_r($query->result());exit;
		return $query->result();
    }
    public function faculty_requests($batchID,$date)
    {
     $this->db->select('timings.timing_id, timings.timing_uid, timings.time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.marker_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.chapter_status, timings.remarks, timings._date,faculties.faculty_code,timings.batch_id,timings._date');
		$this->db->where('timings.batch_id', $batchID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculty_types.faculty_type_id', '1');
		//$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$query = $this->db->get('timings');
   // print_r($query->result());exit;
		return $query->result();
    }
    public function faculty_timetable($batchID,$date)
    {

    	$this->db->where('batch_id', $batchID);
		$query = $this->db->get('batches');
        
		$batchCode = $query->row()->batch_code;


		$this->db->where('batch_code', $batchCode);
		$this->db->where('_date', $date);
		$query = $this->db->get('time_table');
       // print_r($query->result());exit;
		return $query->result();
    }
	public function report($facultyID, $from, $to)
	{
		$this->db->select("SUM(timings.payment) AS amount");
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		//$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		//$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		//$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->row();
	}

	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function slabtypes()
	{
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}
}