<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyDeleteModel extends CI_Model {

	public function delete($facultyID)
	{
		$faculty['status'] 		 = 'deleted';
		$faculty['faculty_code'] = NULL;

		$this->db->where('faculty_id', $facultyID);
		$this->db->update('faculties', $faculty);

		$user['status'] = 'deleted';

		$this->db->where('faculty_id', $facultyID);
		$this->db->update('users', $user);
	}
}