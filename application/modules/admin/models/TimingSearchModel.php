<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingSearchModel extends CI_Model {

	public function search($from, $to, $facultyID)
	{
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function search_summary($from, $to, $facultyID)
	{
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->group_by('timings.slab_type_id');
		$query = $this->db->get('timings');
        // print_r($query->result());exit;
		if($query->num_rows() > 0)
		{
			$slabtypes = array();
			foreach ($query->result() as $data) 
			{
				$slabtypes[] = $data->slab_type_id;
			}

			$this->db->where_in('slab_type_id', $slabtypes);
			$query = $this->db->get('slab_types');

			$summaryData = array();
			if ($query->num_rows() > 0) 
			{
				$slabtypesData = $query->result();

				for ($i=0; $i < count($slabtypesData); $i++) 
				{
					$summaryData[$i] = $slabtypesData[$i];
					$summaryData[$i]->meta 	= $this->summary_meta($facultyID, $slabtypesData[$i]->slab_type_id, $from, $to);
				}
			}
          
			return $summaryData;
		}
		else
		{
			return array();
		}
	}

	private function summary_meta($facultyID, $slabtypeID, $from, $to)
	{
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.slab_type_id', $slabtypeID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		$duration = date('Y-m-d H:i:s', strtotime('2018-01-01 00:00:00'));
		$hour = 0;
		$minutes = 0;
		$seconds = 0;
		$amount 	= '0';
		foreach ($query->result() as $data) 
		{
		    $hour += date('H', strtotime($data->duration));
		    $minutes += date('i', strtotime($data->duration));
		    $seconds += date('s', strtotime($data->duration));
		    
			$amount 	+= $data->payment;
		}
		$duration 	= date('Y-m-d H:i:s',strtotime("+".$hour." hour +".$minutes." minutes +".$seconds." seconds",strtotime($duration)));
        $datetime1 = new DateTime('2018-01-01 00:00:00');
        $datetime2 = new DateTime(date('Y-m-d H:i:s', strtotime($duration)));
        $interval = $datetime1->diff($datetime2);
        //echo $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes"; exit;
        
        $h = $interval->format('%d')*24;
        $h += $interval->format('%h');
        $m = $interval->format('%i');
        $s = $interval->format('%s');
        	
		$data = (object) array('duration' => $h.' Hours '.$m.' minutes '.$s.'  seconds', 'amount' => $amount);

		return $data;
	}
    
    public function search_summary1($from, $to)
	{
		// print_r($to);exit;
		$this->db->where('status','active');
		$this->db->select('faculty_code,name,ac_number,ifsc,bank_name,pan_no,ac_holder,faculty_id');
		$this->db->where_not_in('faculty_id',array(1,3,6,7,8));
		$query = $this->db->get('faculties');
		$data	= $query->result();
		
		for ($i=0; $i < count($data); $i++) 
		{	
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id',$data[$i]->faculty_id);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->group_by('timings.slab_type_id');
		$query1= $this->db->get('timings');
        $datas	= $query1->result();
	   
		if($query1->num_rows() > 0)
		{
	        print_r($data[$i]->faculty_id);exit;
			$slabtypes = array();
			for ($i=0; $i < count($datas); $i++) 
		    {
           
				$slabtypes[] = $datas[$i]->slab_type_id;
            }
            $this->db->where('slab_type_id', $slabtypes[0]);
			$query2 = $this->db->get('slab_types');
             // print_r($query2->result());exit;
			$summaryData = array();
			if ($query2->num_rows() > 0) 
			{
				
				$slabtypesData = $query2->result();
             
				for ($i=0; $i < count($slabtypesData); $i++) 
				{

					$summaryData[$i] = $slabtypesData[$i];
					$summaryData[$i]->meta 	= $this->summary_meta1($data[$i]->faculty_id, $slabtypesData[$i]->slab_type_id, $from, $to);
					// $summaryData[$i]->details=$datas;
				}
			}
			 // print_r($summaryData);exit;
			return $summaryData;
          }
           
			
		 
		else
		{
			// print_r(array());exit();
			return array();
		}
	}
	
	}

    private function summary_meta1($facultyID, $slabtypeID, $from, $to)
	{
		// print_r($slabtypeID);exit;
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.slab_type_id', $slabtypeID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		$duration = date('Y-m-d H:i:s', strtotime('2018-01-01 00:00:00'));
		$hour = 0;
		$minutes = 0;
		$seconds = 0;
		$amount 	= '0';
		foreach ($query->result() as $data) 
		{
		    $hour += date('H', strtotime($data->duration));
		    $minutes += date('i', strtotime($data->duration));
		    $seconds += date('s', strtotime($data->duration));
		    
			$amount 	+= $data->payment;
		}
		$duration 	= date('Y-m-d H:i:s',strtotime("+".$hour." hour +".$minutes." minutes +".$seconds." seconds",strtotime($duration)));
        $datetime1 = new DateTime('2018-01-01 00:00:00');
        $datetime2 = new DateTime(date('Y-m-d H:i:s', strtotime($duration)));
        $interval = $datetime1->diff($datetime2);
        //echo $interval->format('%d')." Days ".$interval->format('%h')." Hours ".$interval->format('%i')." Minutes"; exit;
        
        $h = $interval->format('%d')*24;
        $h += $interval->format('%h');
        $m = $interval->format('%i');
        $s = $interval->format('%s');
        	
		$data = (object) array('duration' => $h.' Hours '.$m.' minutes '.$s.'  seconds', 'amount' => $amount);
       
		return $data;
	}  
  

	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function slabtypes()
	{
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}
}