<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BCEditModel extends CI_Model {

	/**
	* GET batch in charge data
	* @param string $facultyID
	* @return array
	*/
	public function batch_in_charge($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}

	public function batches()
	{
		$this->db->where('batch_status', 'active');
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function branches()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	/**
	* Update Faculty as BC Details
	* @param $faculty array
	* @return boolean
	*/
	public function update($faculty, $facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$this->db->update('faculties', $faculty);
	}	

	/**
	* Update BC login details
	* Details are in faculty array we need to create another array for user details 
	* @param $faculty array
	* @return boolean
	*/
	public function update_user($faculty, $facultyID)
	{
		$this->load->library('bcrypt');

		$user['name'] 			= $faculty['name'];
		$user['branch_id'] 		= $faculty['branch_id'];
		$user['permissions'] 	= $faculty['batches'];
		$user['updated_at'] 	= date('Y-m-d H:i:s');

		$this->db->where('faculty_id', $facultyID);
		$this->db->update('users', $user);
	}
}