<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyRegisterModel extends CI_Model {

	public function branches()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	public function batches()
	{
		$this->db->where('batch_status', 'active');
		$query = $this->db->get('batches');

		return $query->result();
	}

	/**
	* Insert Faculty Details
	* @param $faculty array
	* @return boolean
	*/
	public function register($faculty)
	{
		$this->db->insert('faculties', $faculty);

		return $this->db->insert_id();
	}	

	/**
	* Insert Faculty login details
	* Details are in faculty array we need to create another array for user details 
	* @param $faculty array
	* @return boolean
	*/
	public function register_user($faculty, $facultyID)
	{
		$this->load->library('bcrypt');

		$user['name'] 			= $faculty['name'];
		$user['username'] 		= $faculty['phone'];
		$user['password'] 		= $this->bcrypt->hash_password($faculty['phone']);
		$user['user_type_id'] 	= '4';
		$user['faculty_id'] 	= $facultyID;
		$user['branch_id'] 		= $faculty['branch_id'];
		$user['permissions'] 	= $faculty['batches'];
		$user['login_attempt'] 	= 0;
		$user['last_login'] 	= '';
		$user['updated_at'] 	= date('Y-m-d H:i:s');

		$this->db->insert('users', $user);
	}
}