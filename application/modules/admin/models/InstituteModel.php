<?php defined('BASEPATH') OR exit('No directr script access allowed');

class InstituteModel extends CI_Model {

	/**
	* Centers are stored as Branches
	* Both are same
	*/
	public function centers()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	public function save_center($branch)
	{
		$this->db->insert('branches', $branch);
	}

	public function update_center($branch, $branchID)
	{
		$this->db->where('branch_id', $branchID);
		$this->db->update('branches', $branch);
	}

	public function delete_center($branchID)
	{
		$branch['status'] = 'deleted';

		$this->db->where('branch_id', $branchID);
		$this->db->update('branches', $branch);
	}
}