<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimeTableModel extends CI_Model {

	public function remove($date)
	{
		$this->db->where('_date', $date);
		$this->db->delete('time_table');
	}

	public function save($timeTableData)
	{
		$this->db->insert_batch('time_table', $timeTableData);
	}
}