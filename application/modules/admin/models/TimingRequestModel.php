<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingRequestModel extends CI_Model {

	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$this->db->order_by('faculty_code','asc');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id', 'LEFT');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}

	public function timetable($date, $batchCode, $facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		$facultyCode = $query->row()->faculty_code;

		$this->db->where('faculty_code', $facultyCode);
		$this->db->where('batch_code', $batchCode);
		$this->db->where('_date', $date);
		$query = $this->db->get('time_table');

		return $query->row();
	}

	public function faculty_requests($facultyID, $date)
	{
		$this->db->select('timings.timing_id, timings.timing_uid, timings.time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.marker_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.chapter_status, timings.remarks, timings._date,slab_types.slab_type');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculty_types.faculty_type_id', '1');
		//$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$query = $this->db->get('timings');

		return $query->result();
	}	

	public function approved($facultyID, $date)
	{
		$this->db->select('timings.timing_id, timings.timing_uid, timings.time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.marker_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.chapter_status, timings.remarks, timings._date');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculty_types.faculty_type_id', '3');
		$this->db->where('timings.status', 'approved');
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function bc_requests($facultyID, $date)
	{
		$this->db->select('timings.timing_id, timings.timing_uid, timings.time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.marker_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.chapter_status, timings.remarks, timings._date, slab_types.slab_type');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculty_types.faculty_type_id', '2');
		//$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->order_by('timings._date', 'ASC');
		$this->db->order_by('timings.start_time', 'ASC');
		$query = $this->db->get('timings');

		return $query->result();
	}
    public function getSlabs()
    {
    	$this->db->where('status','active');
    	$query = $this->db->get('slab_types');
    	return $query->result();

    }
     public function getbatches()
    {
    	$this->db->where('batch_status','active');
    	$this->db->order_by('batch_code','ASC');
    	$query = $this->db->get('batches');
    	return $query->result();

    }
	public function faculty_timetable($facultyID, $date)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		$facultyCode = $query->row()->faculty_code;

		$this->db->where('faculty_code', $facultyCode);
		$this->db->where('_date', $date);
		$this->db->order_by('start_time', 'ASC');
		$query = $this->db->get('time_table');

		return $query->result();
	}

	public function requests($timingUID)
	{
		$this->db->where('timings.timing_uid', $timingUID);
		$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function timing_requests($facultyID, $date)
	{
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings._date', date('Y-m-d', strtotime($date)));
		$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->join('batches', 'batches.batch_id = timings.batch_id', 'right');
		$this->db->group_by('timings.timing_uid');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function batch_in_charge($batchID)
	{
		$this->db->like('batches', '"'.$batchID.'"');
		$this->db->where('faculty_type_id', '2');
		$this->db->where('status', 'active');
		$query = $this->db->get('faculties');

		return ($query->num_rows() > 0)? $query->row()->faculty_id : NULL;
	}

	public function submit($timing)
	{
		$this->db->insert('timings', $timing);

		return $this->db->insert_id();
	}
    public function getStatus($facultyID, $date)
    {
       $this->db->select('timings.copy');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings._date', $date);
		$this->db->where('faculty_types.faculty_type_id', '2');
		//$this->db->where("(timings.status = 'pending' OR timings.status = 'submitted')");
		$this->db->join('faculties', 'faculties.faculty_id = timings.marker_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id', 'right');
		$this->db->order_by('timings.copy');
		
		$query = $this->db->get('timings');
		return $query->result();
    }
	public function delete_timing($timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->delete('timings');
	}

	public function update_timetable($timetableID)
	{
		$timetable['bc_status'] = 'rejected';
		$timetable['status'] 	= 'rejected';
		
		$this->db->where('time_table_id', $timetableID);
		$this->db->update('time_table', $timetable);
	}

	public function slab($slabtypeID, $facultyID)
	{
		$this->db->where('slab_type_id', $slabtypeID);
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('slabs');

		return $query->row();
	}

	public function update_timetable_status($timetableID, $userType)
	{
		$timetable[$userType] = 'approved';
		$type 				  = ($userType == 'bc_status')? 'status' : 'bc_status';
		$timetable[$type] 	  = 'rejected';

		$this->db->where('time_table_id', $timetableID);
		$this->db->update('time_table', $timetable);
	}

	public function update_timing($timingID)
	{
		$timing['status'] = 'approved';

		$this->db->where('timing_id', $timingID);
		$this->db->update('timings', $timing);
	}

	public function update_timing_others($timingID, $timingUID)
	{
		$timing['status'] = 'rejected';

		$this->db->where('timing_id !=', $timingID);
		$this->db->where('timing_uid', $timingUID);
		$this->db->update('timings', $timing);
	}

	public function final_approval($timingData, $timings)
	{
		//Reject all other requests entered for the same
		for ($i=0; $i < count($timingData); $i++) 
		{
			$timing['status'] = 'rejected';

			//$this->db->where('timing_id !=', $timings['timing_id'][$i]);
			$this->db->where('faculty_id', $timingData[$i]['faculty_id']);
			$this->db->where('batch_id', $timingData[$i]['batch_id']);
			$this->db->where('_date', $timingData[$i]['_date']);
			$this->db->where('start_time <=', date('H:i:s', strtotime("+15 minutes", strtotime($timingData[$i]['start_time']))));
			$this->db->where('start_time >=', date('H:i:s', strtotime("-15 minutes", strtotime($timingData[$i]['start_time']))));
			$this->db->update('timings', $timing);

			//Update Balance Hour 
			$this->update_balance_hour($timingData[$i]);
		}

		$this->db->insert_batch('timings', $timingData);
	}	

	public function update_final_approval($timingData, $timings)
	{
		//Reject all other requests entered for the same
		for ($i=0; $i < count($timingData); $i++) 
		{
			$this->db->where('timing_id', $timingData[$i]['timing_id']);
			$this->db->update('timings', $timingData[$i]);
		}
	}

	private function update_balance_hour($data)
	{
		$data = (object) $data;

		$this->db->where('branch_id', $data->center_id);
		$this->db->where('class_id', $data->class_id);
		$this->db->where('batch_id', $data->batch_id);
		$this->db->where('subject_id', $data->subject_id);
		$this->db->where('chapter_id', $data->chapter_id);
		$query = $this->db->get('chapter_timing');

		if($query->num_rows() > 0)
		{
			$minutes 	= round(abs(strtotime($data->end_time) - strtotime($data->start_time)) / 60,2);
			$duration 	= gmdate("H:i:s", ($minutes * 60));

			$taken = date('H:i:s',strtotime("+".date('H', strtotime($duration))." hour +".date('i', strtotime($duration))." minutes +".date('s', strtotime($duration))." seconds",strtotime($query->row()->hours_taken)));

			//Find balance hour 
			$minutes 	= round(abs(strtotime($query->row()->hours_balance) - strtotime($duration)) / 60,2);
			$balance 	= gmdate("H:i:s", ($minutes * 60));
			
			$chaptime['hours_taken'] 	= $taken;
			$chaptime['hours_balance'] 	= $balance;
			
			$this->db->where('chapter_timing_id', $query->row()->chapter_timing_id);
			$this->db->update('chapter_timing', $chaptime);
		}
		else
		{
			$this->db->where('subject_id', $data->subject_id);
			$this->db->where('chapter_id', $data->chapter_id);
			$query = $this->db->get('chapters');

			if($query->num_rows() > 0)
			{
				$minutes 	= round(abs(strtotime($data->end_time) - strtotime($data->start_time)) / 60,2);
				$duration 	= gmdate("H:i:s", ($minutes * 60));
				//Find balance hour 
				$minutes 	= round(abs(strtotime($query->row()->hours) - strtotime($duration)) / 60,2);
				$balance 	= gmdate("H:i:s", ($minutes * 60));
				
				$chaptertiming['branch_id'] 	= $data->center_id;
				$chaptertiming['class_id'] 		= $data->class_id;
				$chaptertiming['batch_id'] 		= $data->batch_id;
				$chaptertiming['subject_id'] 	= $data->subject_id;
				$chaptertiming['chapter_id'] 	= $data->chapter_id;
				$chaptertiming['hours_allowed'] = $query->row()->hours;
				$chaptertiming['hours_taken'] 	= $duration;
				$chaptertiming['hours_balance'] = $balance;

				$this->db->insert('chapter_timing', $chaptertiming);
			}
		}
	}
}