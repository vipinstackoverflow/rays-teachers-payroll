<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyEditModel extends CI_Model {

	public function update($faculty, $facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$this->db->update('faculties', $faculty);
	}

	public function update_user($faculty, $facultyID)
	{
		$this->load->library('bcrypt');

		$user['name'] 			= $faculty['name'];
		$user['user_type_id'] 	= '4';
		$user['faculty_id'] 	= $facultyID;
		$user['branch_id'] 		= $faculty['branch_id'];
		$user['permissions'] 	= json_encode(array());
		$user['updated_at'] 	= date('Y-m-d H:i:s');

		$this->db->where('faculty_id', $facultyID);
		$this->db->update('users', $user);
	}
}