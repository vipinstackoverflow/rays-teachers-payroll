<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model {

	public function authenticate($user)
	{
		
		$query	=	$this->db->where('username',$user["username"])
							 ->where('status','active')
							 ->get('users');
		
		if($query->num_rows()>0)
		{
			$usermeta = $query->row();

			$this->load->library('bcrypt');

			if($this->bcrypt->check_password($user['password'], $usermeta->password))
			{
				$attempt['login_attempt'] 	= '0';
				$attempt['last_login']		= $this->input->ip_address();

				$this->db->where('username',$user['username']);
				$this->db->update('users',$attempt);

				return $query->row();
			}
			else
			{
				$attempt['login_attempt'] 	= $usermeta->login_attempt+1;
				$attempt['last_login']		= $this->input->ip_address();
				$attempt['status']			= ($usermeta->login_attempt >= 5)? 'blocked' : 'active';

				$this->db->where('username',$user['username']);
				$this->db->update('users',$attempt);

				$error = ($usermeta->login_attempt >= 5)? 'You have been blocked due to several failed attempts' : 'Your password is incorrect';

				$this->session->set_flashdata('error',$error);

				return NULL;
			}
		}
		else
		{
			$this->session->set_flashdata('error','username or password is incorrect');

			return NULL;
		}
	}
}