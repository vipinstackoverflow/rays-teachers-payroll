<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
	}

	public function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
	}

	public function index()
	{
		$userType = $this->session->usertype;

		$url	  = ($userType == '1' || $userType == '2')? 'admin/userpanel' : (($userType == '3')? 'bc/userpanel' : 'faculty/userpanel');

		redirect($url);
	}
}