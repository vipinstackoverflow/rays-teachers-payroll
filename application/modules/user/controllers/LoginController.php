<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends MX_Controller {

	public function index()
	{
		$this->load->view('signin');
		/*if($this->session->user)
		{
			$userType = $this->session->usertype;

			$url	  = ($userType == '1')? 'admin/userpanel' : (($userType == '2')? 'bc/userpanel' : 'faculty/userpanel');

			redirect($url);
		}
		else
		{
			$this->load->view('signin');
		}*/
	}

	public function logout()
	{
		$session = array('username' => '' , 'userid' => '' , 'facultyid' => '', 'usertype' => '' , 'access' => '' , 'user' => FALSE );
		
		# setting session
		$this->session->set_userdata($session);

		#redirect to Admin Home
		redirect('user/login');
	}
}
