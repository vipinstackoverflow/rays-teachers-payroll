<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('AuthModel');
	}

	#Checking Authentication
	public function authenticate()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username','Username','trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[5]|xss_clean');

		if($this->form_validation->run() == TRUE)
		{
			//$this->session->sess_destroy(); exit();
			$user['username'] = $this->input->post('username');
			$user['password'] = $this->input->post('password');

			// validating login attempts

			if($this->session->loginattempts)
			{
				// print_r($this->session->loginattempts);exit;

				$loginAttempts = $this->session->loginattempts+1;
				$loginAttemptTime = $this->session->loginattempttime;

				$login = array('loginattempts' => $loginAttempts);

				$this->session->set_userdata($login);

				if($loginAttempts > 3 && ((time() - $loginAttemptTime) <= 400))
				{
					$this->session->set_flashdata('error','You have reached maximum login attempts ,please try again in a while');

					redirect('user/login');
				}
				elseif((time() - $loginAttemptTime) > 400)
				{
					$loginAttempts = 1;
					$loginAttemptTime = time();		
					
					$login = array('loginattempts' => $loginAttempts, 'loginattempttime' => $loginAttemptTime);

					$this->session->set_userdata($login);

					$this->validate_user($user);		
				}
				else
				{
					$this->validate_user($user);
				}
			}
			else
			{
				$loginAttempts = 1;
				$loginAttemptTime = time();

				$login = array('loginattempts' => $loginAttempts, 'loginattempttime' => $loginAttemptTime);

				$this->session->set_userdata($login);

				$this->validate_user($user);
			}
		}
		else
		{
			$this->session->set_flashdata('error',validation_errors());
			redirect('user/login');
		}

	}

	private function validate_user($user)
	{
		// validate username and password
		$userdata = $this->AuthModel->authenticate($user);

		if(!empty($userdata))
		{
			# creating session data
			$session= array('username' => $userdata->name , 'userid' => $userdata->id , 'facultyid' => $userdata->faculty_id, 'usertype' => $userdata->user_type_id, 'permissions' => $userdata->permissions , 'user' => TRUE );
			# setting session
			$this->session->set_userdata($session);
			//var_dump($session); exit();
			#redirect to User Home
			$userType = $this->session->usertype;

			$url	  = ($userType == '1' || $userType == '2')? 'admin/userpanel' : (($userType == '3')? 'bc/userpanel' : (($userType == '4')? 'faculty/userpanel' : 'status/userpanel'));

			redirect($url);
		}
		else
		{
			redirect('user/login');
		}
	}
}
