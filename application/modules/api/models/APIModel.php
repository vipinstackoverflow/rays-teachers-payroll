<?php defined('BASEPATH') OR exit('No direct script access allowed');

class APIModel extends CI_Model {

	public function batches($centerID, $classID)
	{
		$this->db->select('batch_id, batch, batch_code');
		$this->db->order_by("batch", "asc");
		$this->db->where('batch_status', 'active');
		$this->db->where('branch_id', $centerID);
		$this->db->where('class_id', $classID);
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function subjects($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('subjects');

		return $query->result();
	}

	public function slabtypes($centerID, $batchID)
	{
		
		$batches = array(15, 16, 31, 32, 43, 127, 101, 102,41,256,42,257);
		// goutham
		$batch=array(101,102,256,257);

		$this->db->select('slab_type_id, slab_type');
		if(!in_array($batchID, $batches))
		{
		$this->db->where('slab_type_id !=', '2');
		$this->db->where('slab_type_id !=', '8');
		// goutham
		$this->db->where('slab_type_id !=', '18');

		}
		// goutham
		elseif (in_array($batchID, $batch)) {
		$this->db->where_not_in('slab_type_id', array(1,2,15));	
		$this->db->order_by('slab_type','asc');
		}
		$this->db->where_not_in('slab_type_id', array(11, 12));
		$this->db->where('status', 'active');
		$this->db->where('branch_id', $centerID);
		$query = $this->db->get('slab_types');
        
		return $query->result();
	}

	public function chapters($branchID, $classID, $batchID, $subject)
	{
		$this->db->where('subject', $subject);
		$this->db->where('class_id', $classID);
		$query = $this->db->get('subjects');

		$subjectID = $query->row()->subject_id;

		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('chapters');

		return $query->result();
	}

	public function teachers($batchID)
	{
		$this->db->select('faculty_id, name, subject, batches');
		$this->db->where('faculty_type_id', '1');
		$this->db->where('status', 'active');
		$query = $this->db->get('faculties');

		$faculties = array();
		if($query->num_rows() > 0)
		{
			$facultyData = $query->result();
			for($i=0; $i < count($facultyData); $i++)
			{
				$batches = json_decode($facultyData[$i]->batches);

				if(in_array($batchID, $batches))
				{
					$faculties[$i] = $facultyData[$i];
				}
			}

			return $faculties;
		}
		else
		{
			return $faculties;
		}
	}

	public function chapter_hour($data)
	{
		$this->db->where('branch_id', $data->centerid);
		$this->db->where('class_id', $data->classid);
		$this->db->where('batch_id', $data->batchid);
		$this->db->where('subject_id', $data->subjectid);
		$this->db->where('chapter_id', $data->chapterid);
		$query = $this->db->get('chapter_timing');

		if($query->num_rows() > 0)
		{
			$minutes 	= round(abs(strtotime($data->end) - strtotime($data->start)) / 60,2);
			$duration 	= gmdate("H:i:s", ($minutes * 60));

			//Find balance hour 
			$minutes	= round(abs(strtotime($query->row()->hours_balance) - strtotime($duration)) / 60,2);
			$duration 	= gmdate("H:i:s", ($minutes * 60));

			$duration   = ($query->row()->hours_balance == '00:00:00')? "Given time exceeded, extra of ".$duration : $duration;

			return json_encode(array('status' => 'success', 'error'=> '', 'time_left' => $duration));
		}
		else
		{
			$this->db->where('subject_id', $data->subjectid);
			$this->db->where('chapter_id', $data->chapterid);
			$query = $this->db->get('chapters');

			$chaptertiming['branch_id'] 	= $data->centerid;
			$chaptertiming['class_id'] 		= $data->classid;
			$chaptertiming['batch_id'] 		= $data->batchid;
			$chaptertiming['subject_id'] 	= $data->subjectid;
			$chaptertiming['chapter_id'] 	= $data->chapterid;
			$chaptertiming['hours_allowed'] = $query->row()->hours;
			$chaptertiming['hours_taken'] 	= '00:00:00';
			$chaptertiming['hours_balance'] = $query->row()->hours;

			$this->db->insert('chapter_timing', $chaptertiming);

			$minutes 		= round(abs(strtotime($data->end) - strtotime($data->start)) / 60,2);
			$duration 		= gmdate("H:i:s", ($minutes * 60));

			//Find balance hour 
			$minutes 	= round(abs(strtotime($query->row()->hours) - strtotime($duration)) / 60,2);
			$duration 	= gmdate("H:i:s", ($minutes * 60));

			return json_encode(array('status' => 'success', 'error' => '', 'time_left' => $duration));
		}
	}
}