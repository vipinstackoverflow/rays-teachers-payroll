<?php defined('BASEPATH') OR exit('No direct script access allowed');

class APIController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('APIModel');
	}

	public function batches()
	{
		$centerID 	= $this->input->post('centerid');
		$classID 	= $this->input->post('classid');

		$batches	= $this->APIModel->batches($centerID, $classID);
        
		echo json_encode($batches);
	}	

	public function subjects()
	{
		$centreID 	= $this->input->post('centreid');
		$classID 	= $this->input->post('classid');
		$batchID 	= $this->input->post('batchid');

		$subjects	= $this->APIModel->subjects($classID);

		echo json_encode($subjects);
	}

	public function slabtypes()
	{
		
		$centerID 	= $this->input->post('centerid');
		$batchID 	= $this->input->post('batchid');
   
		$slabtypes	= $this->APIModel->slabtypes($centerID, $batchID);

		echo json_encode($slabtypes);
	}

	public function chapters()
	{
		$branchID 	= $this->input->post('centreid');
		$classID 	= $this->input->post('classid');
		$batchID 	= $this->input->post('batchid');
		$subject 	= $this->input->post('subject');

		$chapters = $this->APIModel->chapters($branchID, $classID, $batchID, $subject);

		echo json_encode($chapters);
	}

	public function teachers()
	{
		$batchID = $this->input->post('batchid');

		echo json_encode($this->APIModel->teachers($batchID));
	}

	public function chapter_hour()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('centerid', 'Center ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('classid', 'Class ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('batchid', 'batch ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('subjectid', 'subject ID', 'required|xss_clean|trim|greater_than[0]');
		$this->form_validation->set_rules('chapterid', 'chapter ID', 'required|xss_clean|trim|greater_than[0]');
		$this->form_validation->set_rules('start', 'Start', 'required|xss_clean|trim');
		$this->form_validation->set_rules('end', 'End', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$data = (object) $this->input->post(NULL, TRUE);

			$response = $this->APIModel->chapter_hour($data);

			echo $response;
		}
		else
		{
			echo json_encode(array('status' => 'error', 'error' => validation_errors()));
		}
	}
}