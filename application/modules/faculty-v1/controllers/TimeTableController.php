<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimeTableController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('TimeTableModel');
	}

	private function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
		elseif ($this->session->usertype != '4') 
		{
			redirect('user/login');
		}
	}

	public function view($date)
	{
		$facultyID 		 = $this->session->facultyid;
		$date			 = date('Y-m-d', strtotime($date));
		$data['timings'] = $this->TimeTableModel->timings($date, $facultyID);

		$this->load->view('time-table/view', $data);
	}
}