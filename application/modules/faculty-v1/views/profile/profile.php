   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-user"></i> Profile</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Profile</a></li>
        </ul>
      </div>
      <?php echo form_open_multipart('profile/update', 'id="update"') ?>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
            <div class="row" >             
              <div class="col-md-6" >
                <a href="<?php echo base_url('faculty/userpanel'); ?>" class="btn btn-sm btn-success" ><- Back</a>

                <div class="form-group row">
                  <label class="control-label col-md-3">Photo</label>
                  <div class="col-md-8">
                    <img src="<?php echo ($profile->image != '')? base_url('resources/images/'.$profile->image) : base_url('resources/images/profile.png'); ?>" width="50%" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Name</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="name" id="name" placeholder="Enter full name" value="<?php echo $profile->name; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="email" id="email" placeholder="Enter email address" value="<?php echo $profile->email; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Phone</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone" value="<?php echo $profile->phone; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Address</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" name="address" id="address" placeholder="Enter your current address"><?php echo $profile->address; ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Account Holder</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="ac_holder" id="ac_holder" placeholder="Account Holder Name" value="<?php echo $profile->ac_holder; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Account Number</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="ac_number" id="ac_number" placeholder="Account Number" value="<?php echo $profile->ac_number; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">IFSC</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="ifsc" id="ifsc" placeholder="IFSC" value="<?php echo $profile->ifsc; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Bank Name</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="bank" id="bank" placeholder="Bank Name" value="<?php echo $profile->bank_name; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Pan Number</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" rows="4" name="pan" id="pan" placeholder="Pan Number" value="<?php echo $profile->pan_no; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Image</label>
                  <div class="col-md-8">
                    <input type="file" class="form-control" name="image" id="image" >
                  </div>
                </div>
              </div>
              <div class="col-md-6" >
              </div>
            </div>
            <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/multiselect/kendo.all.min.js'); ?>"></script>

    <script type="text/javascript">
      $("#update").validate({
        rules:{
          name:{
            required: true
          },
          phone:{
            required:true
          },
          email:{
            required:true,
            email:true
          },
          address:{
            required:true
          }
        },
        messages:{
          name:{
            required:"Please enter Faculty Name"
          },
          phone:{
            required:"Please enter Phone Number"
          },
          email:{
            required:"Please enter Email"
          },
          address:{
            required:"Please enter address"
          }
        }
      });
    </script>
    <?php $this->load->view('import/footer'); ?>