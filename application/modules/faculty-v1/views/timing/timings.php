   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-clock-o"></i> Mark Timing</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Mark Timing</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <a href="<?php echo base_url('faculty/userpanel'); ?>" class="btn btn-success" ><- Back</a>
              <div class="row">
                <div class="col-md-6">
                <?php echo form_open('timing/submit', 'id="mark"'); ?>
                <input type="hidden" name="subject" id="subject" value="<?php echo $subject; ?>" >
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">Mark Timing</h3>
                    </div>
                    <div class="tile-body">
                      <div class="form-group row">
                        <label class="control-label col-md-3">Date</label>
                        <div class="col-md-8">
                          <input type="text" name="ttdate" id="ttdate" value="<?php echo date('d-m-Y'); ?>" readonly class="form-control" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Centres</label>
                        <div class="col-md-8">
                          <select class="form-control" name="centre" id="centre" >
                              <option value="" >Please Select</option>
                              <?php foreach($centres as $centre) { ?>
                                <option value="<?php echo $centre->branch_id; ?>" ><?php echo $centre->branch; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Class</label>
                        <div class="col-md-8">
                          <select class="form-control" name="class" id="class" >
                              <option value="" >Please Select</option>
                              <?php foreach($classes as $class) { ?>
                                <option value="<?php echo $class->class_id; ?>" ><?php echo $class->class; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Batch</label>
                        <div class="col-md-8">
                          <select class="form-control" name="batch" id="batch" >
                              <option value="" >Please Select</option>
                          </select>
                        </div>
                      </div>                      
                      <div class="form-group row">
                        <label class="control-label col-md-3">Chapter</label>
                        <div class="col-md-8">
                          <select class="form-control" name="chapter" id="chapter" >
                              <option value="" >Please Select</option>
                          </select>
                        </div>
                      </div>                     
                      <div class="form-group row">
                        <label class="control-label col-md-3">Chapter Status</label>
                        <div class="col-md-8">
                          <select class="form-control" name="chapterstatus" id="chapterstatus" >
                              <option value="" >Please Select</option>
                              <option value="Ongoing" >Ongoing</option>
                              <option value="Completed" >Completed</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Slab</label>
                        <div class="col-md-8">
                          <select class="form-control" name="slab" id="slab" >
                              <option value="" >Please Select</option>
                          </select>
                        </div>
                      </div>
                      <input type="hidden" name="period" id="period" value="1" >
                      <!-- <div class="form-group row">
                        <label class="control-label col-md-3">Period</label>
                        <div class="col-md-8">

                          <select class="form-control" name="period" id="period" >
                              <option value="" >Please Select</option>
                              <?php foreach($periods as $data) { ?>
                                <option value="<?php echo $data->period_id ?>" ><?php echo $data->period; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="control-label col-md-3">Started Time</label>
                        <div class="col-md-8">
                          <input class="form-control time" type="text" name="start" id="start" placeholder="Started Time" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Ended Time</label>
                        <div class="col-md-8">
                          <input class="form-control time" type="text" name="end" id="end" placeholder="Ended Time" >
                        </div>
                      </div>
                      <div class="form-group row" id="time_left_div" style="display: none;" >
                        <label class="control-label col-md-3">Time Left</label>
                        <div class="col-md-8">
                          <span id="time_left" ></span>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Remarks</label>
                        <div class="col-md-8">
                          <textarea class="form-control" name="remarks" id="remarks" placeholder="Topic Name, . . etc" ></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div>
                  </div>
                <?php echo form_close(); ?>
                </div>
                <div class="col-md-6" >
                  <div class="tile">
                    <div class="overlay" id="overlay" style="z-index: 1000; display: none;">
                      <div class="m-loader mr-4">
                        <svg class="m-circular" viewBox="25 25 50 50">
                          <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
                        </svg>
                      </div>
                      <h3 class="l-text">Loading</h3>
                    </div>
                    <div class="tile-title-w-btn">
                      <h3 class="title">Final Submission</h3>
                    </div>
                    <div class="tile-body">
                        <div id="accordion">
                          <?php $i=0; foreach($timings as $data) { $i++; ?>
                          <?php echo form_open('timing/update/'.$data->timing_id); ?>
                          <input type="hidden" name="timingid[]" class="tid" value="<?php echo $data->timing_id; ?>" >
                          <div class="card">
                            <div class="card-header">
                              <a class="card-link" data-toggle="collapse" href="#<?php echo $i; ?>">
                                <?php echo $data->batch." - ".date('h:ia', strtotime($data->start_time))." ".date('h:ia', strtotime($data->end_time)); ?>
                              </a>
                              <span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span>
                              <a class="card-link" onclick="return confirm('Are You Sure ?')" href="<?php echo base_url('timing/delete/'.$data->timing_id); ?>">
                                <i class="fa fa-trash" style="color: red" ></i>
                              </a>
                            </div>
                            <div id="<?php echo $i; ?>" class="collapse" data-parent="#accordion">
                              <div class="card-body">
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Date</label>
                                  <div class="col-md-8">
                                    <input type="text" name="ttdate" id="ttdate" value="<?php echo date('d-m-Y'); ?>" readonly class="form-control" >
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Centres</label>
                                  <div class="col-md-8">
                                    <select class="form-control" name="centre" id="centre" >
                                        <option value="" >Please Select</option>
                                        <?php foreach($centres as $centre) { ?>
                                          <option value="<?php echo $centre->branch_id; ?>" <?php if($centre->branch_id == $data->branch_id) { ?> selected <?php } ?> ><?php echo $centre->branch; ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Class</label>
                                  <div class="col-md-8">
                                    <select class="form-control" name="class" id="class" >
                                        <option value="" >Please Select</option>
                                        <?php foreach($classes as $class) { ?>
                                          <option value="<?php echo $class->class_id; ?>" <?php if($class->class_id == $data->class_id) { ?> selected <?php } ?> ><?php echo $class->class; ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                                </div> 
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Batch</label>
                                  <div class="col-md-8">
                                    <select class="form-control" name="batch" id="batch" >
                                        <option value="" >Please Select</option>
                                        <?php foreach($data->batches as $batch) { ?>
                                          <option value="<?php echo $batch->batch_id."|".$batch->batch_code; ?>" <?php if($batch->batch_id == $data->batch_id) { ?> selected <?php } ?> ><?php echo $batch->batch; ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                                </div>                     
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Chapter</label>
                                  <div class="col-md-8">
                                    <select class="form-control" name="chapter" id="chapter" >
                                        <option value="0|0|Nil" >Please Select</option>
                                        <?php foreach($data->chapters as $chapter) { ?>
                                          <option value="<?php echo $chapter->subject_id."|".$chapter->chapter_id."|".$chapter->chapter; ?>" <?php if($data->chapter_id == $chapter->chapter_id) { ?> selected <?php } ?> ><?php echo $chapter->chapter; ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                                </div>                     
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Chapter Status</label>
                                  <div class="col-md-8">
                                    <select class="form-control" name="chapterstatus" id="chapterstatus" >
                                        <option value="" >Please Select</option>
                                        <option value="Ongoing" <?php if($data->chapter_status == 'Ongoing') { ?> selected <?php } ?> >Ongoing</option>
                                        <option value="Completed" <?php if($data->chapter_status == 'Completed') { ?> selected <?php } ?> >Completed</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Slab</label>
                                  <div class="col-md-8">
                                    <select class="form-control" name="slab" id="slab" >
                                        <option value="" >Please Select</option>
                                        <?php foreach($data->slabtypes as $slabtype) { ?>
                                          <option value="<?php echo $slabtype->slab_type_id; ?>" <?php if($slabtype->slab_type_id == $data->slab_type_id) { ?> selected <?php } ?> ><?php echo $slabtype->slab_type; ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <input type="hidden" name="period" id="period" value="1" >
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Started Time</label>
                                  <div class="col-md-8">
                                    <input class="form-control time" type="text" name="start" id="start" placeholder="Started Time" value="<?php echo  $data->start_time; ?>" >
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Ended Time</label>
                                  <div class="col-md-8">
                                    <input class="form-control time" type="text" name="end" id="end" placeholder="Ended Time" value="<?php echo $data->end_time; ?>" >
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label class="control-label col-md-3">Remarks</label>
                                  <div class="col-md-8">
                                    <textarea class="form-control" name="remarks" id="remarks" placeholder="Topic Name, . . etc" ><?php echo $data->remarks; ?></textarea>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-8">
                                    <input type="submit" value="Update" class="btn btn-sm btn-success" >
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php echo form_close(); ?>
                          <?php } ?>
                        </div>
                    </div>
                    <div class="tile-footer">
                      <?php if(count($timings) > 0) { ?>
                      <input type="submit" class="btn btn-primary" id="finalsubmit" value="Final Submit" >
                      <?php } ?>
                    </div>
                  </div>

                </div>
              </div>
            <!-- <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div> -->
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/time-picker/mdtimepicker.min.js'); ?>"></script>

    <script type="text/javascript">

      $(".time").mdtimepicker({
        timeFormat:'hh:mm.ss',
        format:'h:mmtt'
      });      
      /*$(".time").timepicker({ 
          'timeFormat': 'h:ia',
          'step' : 1 
        });*/
      $("#class").on('change', function(){
        centerid = $("#centre").val();
        classid = $(this).val();

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/batches'); ?>",
          dataType:"json",
          data:{ centerid, classid },
          success:function(res){
                if(classid == '5'){
                    batches = "<option value='134|Doubt-Clearance'>Doubt Clearance</option>";
                    if(centerid == '1'){
                        chapter = "<option value='19|266|Doubt Clearance' selected >Doubt Clearance</option>";
                        $("#chapter").html(chapter);
                        
                        slab = "<option value='10' selected >CLT DOUBT CLEARANCE</option>";
                        $("#slab").html(slab);
                    }
                    if(centerid == '2'){
                        chapter = "<option value='19|266|Doubt Clearance' selected >Doubt Clearance</option>";
                        $("#chapter").html(chapter);
                        
                        slab = "<option value='11' selected >KNR DOUBT CLEARANCE</option>";
                        $("#slab").html(slab);
                    }
                    
                    if(centerid == '3'){
                        chapter = "<option value='19|266|Doubt Clearance' selected >Doubt Clearance</option>";
                        $("#chapter").html(chapter);
                        
                        slab = "<option value='12' selected >KGD DOUBT CLEARANCE</option>";
                        $("#slab").html(slab);
                    }
                }
                else
                {
                    batches = "<option value=''>Please Select</option>";
                    $.each(res, function(i, data){
                        batches += "<option value='"+data.batch_id+"|"+data.batch_code+"' >"+data.batch+"</option>";
                    });
                }
            $("#batch").html(batches);
          },
          error:function(res){
            console.log('Connection error');
          }
        });
      });
      $("#batch").on('change', function(){
        centreid  = $("#centre").val();
        classid   = $("#class").val();
        batch     = $("#batch").val().split('|');
        batchid   = batch[0];
        subject   = $("#subject").val(); //.split('|')
        //centerid  = 

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/chapters'); ?>",
          dataType:"json",
          data:{ centreid, classid, batchid, subject },
          success:function(res){
            console.log(res);
            options = '<option value="0|0|Nil" >Please Select</option>';
            $.each(res, function(i, data){
              options += "<option value='"+data.subject_id+"|"+data.chapter_id+"|"+data.chapter+"' >"+data.chapter+"</option>";
            });
            $("#chapter").html(options);
          },
          error:function(res){

          }
        });

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/slabtypes'); ?>",
          dataType:"json",
          data:{ centerid, batchid },
          success:function(res){
            options = '';
            $.each(res, function(i, data){
              options += "<option value='"+data.slab_type_id+"' >"+data.slab_type+"</option>";
            });
            $("#slab").html(options);
          },
          error:function(res){
            console.log('Connection error');
          }
        })
      });

      $("#end").on('change', function(){

        data      = $("#batch").val().split('|');
        centreid  = $("#centre").val();
        classid   = $("#class").val();
        batchid   = data[0];

        chapmeta= $("#chapter").val().split('|');
        subjectid= chapmeta[0]; 
        chapterid= chapmeta[1];

        start = $("#start").val();
        end   = $("#end").val();

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/chapter-hour'); ?>",
          dataType:"json",
          data:{ centerid, classid, batchid, subjectid, chapterid, start, end },
          success:function(res){
            if(res.status == 'success')
            {
              $("#time_left_div").css('display', '');
              $("#time_left").html(res.time_left);
            }
            else
            {
              $("#time_left_div").css('display', 'none');
              $("#time_left").html('');
            }
          },
          error:function(res){
            console.log('Connection error');
          }
        });
      });

      $("#mark").validate({
        rules:{
          ttdate:{
            required:true
          },
          center:{
            required:true
          },
          class:{
            required:true
          },
          batch:{
            required:true
          },
          slab:{
            required:true
          }
        },
        messages:{
          ttdate:{
            required:"Please choose Date"
          },
          center:{
            required:"Please choose center"
          },
          class:{
            required:"Please choose class"
          },
          batch:{
            required:"Please choose batch"
          },
          slab:{
              required:"Please choose Slab"
          }
        }
      });

      $("#finalsubmit").on('click', function(){
        $("#overlay").css('display', '');
        timings = $("input[name='timingid[]']").map(function(){return $(this).val()}).get();
        csrfHash="<?php echo $this->security->get_csrf_hash(); ?>";
        $.ajax({
          type:"POST",
          url:"<?php echo base_url('timing/final-submission'); ?>",
          dataType:"json",
          data:{ token:csrfHash, timings },
          success:function(res){
            alert('Submitted for Approval');
            window.location.reload();
          },
          error:function(res){
            console.log('Connection error');
            alert('Something went wrong, Please try again');
          }
        });
      });

    </script>
    <?php $this->load->view('import/footer'); ?>