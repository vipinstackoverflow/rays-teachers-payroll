<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileModel extends CI_Model {

	public function profile($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}

	public function update($faculty, $facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$this->db->update('faculties', $faculty);
	}

	public function update_user($faculty, $facultyID)
	{
		$this->load->library('bcrypt');

		$user['name'] 			= $faculty['name'];
		$user['updated_at'] 	= date('Y-m-d H:i:s');

		$this->db->where('faculty_id', $facultyID);
		$this->db->update('users', $user);
	}
}