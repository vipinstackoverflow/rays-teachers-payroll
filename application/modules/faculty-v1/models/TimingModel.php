<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingModel extends CI_Model {

	public function centers()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}
	
	public function classes()
	{
		$query = $this->db->get('classes');

		return $query->result();
	}

	public function subject()
	{
		$facultyID = $this->session->facultyid;

		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row()->subject;
	}

	public function batches_list($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function batches()
	{
		$this->db->where_in('batch_id', json_decode($this->session->permissions));
		$this->db->join('classes', 'classes.class_id = batches.class_id');
		$this->db->join('branches', 'branches.branch_id = batches.branch_id');
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function periods()
	{
		$query = $this->db->get('periods');

		return $query->result();
	}

	public function slabtypes($centerID)
	{
		$this->db->select('slab_type_id, slab_type');
		$this->db->where('status', 'active');
		$this->db->where('branch_id', $centerID);
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function chapters($subjectID)
	{
		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('chapters');

		return $query->result();
	}

	public function submitted_timings()
	{
		$this->db->where('_date', date('Y-m-d'));
		$this->db->where('faculty_id', $this->session->facultyid);
		$this->db->where('status', 'user_pending');
		$this->db->join('batches', 'batches.batch_id = timings.batch_id');
		$query = $this->db->get('timings');

		return $query->result();
	}

	/**
	* Get time table for the requested date for the faculty
	* Timing is stored for each faculty in timetable with faculty code
	* Get faculty code from faculty table using faculty id in the session
	* @param string $date, string $facultyID
	* @return array
	*/
	public function timings($date, $facultyID)
	{
		//get faculty code using faculty ID
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		$facultyCode = $query->row()->faculty_code;

		// Get timings
		$this->db->where('faculty_code', $facultyCode);
		$this->db->where('_date', $date);
		$this->db->where('time_table.status', 'pending');
		$this->db->join('slab_types', 'slab_types.slab_type_id = time_table.slab_type_id', 'left');
		$this->db->order_by('time_table.start_time', 'ASC');
		$query = $this->db->get('time_table');

		return $query->result();
	}

	public function submit($timing)
	{
		//Generate UID
		$this->db->select_max('timing_uid');
		$query = $this->db->get('timings');

		$timingUID = 1;
		$timingUID = ($query->num_rows() > 0)? $query->row()->timing_uid+1 : 1;

		$timing['timing_uid'] = $timingUID;

		$this->db->insert('timings', $timing);
	}

	public function update($timing, $timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->update('timings', $timing);
	}

	public function delete($timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->delete('timings');
	}

	public function update_timetable($timetableID)
	{
		$timetable['status'] = 'submitted';
		
		$this->db->where('time_table_id', $timetableID);
		$this->db->update('time_table', $timetable);
	}

	public function slab($slabtypeID, $facultyID)
	{
		$this->db->where('slab_type_id', $slabtypeID);
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('slabs');

		return $query->row();
	}

	public function final_submission($timingID)
	{
		for ($i=0; $i < count($timingID); $i++) 
		{ 
			$timing['status'] = 'pending';

			$this->db->where('timing_id', $timingID[$i]);
			$this->db->update('timings', $timing);
		}
	}

	public function approved($facultyID)
	{
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->result();
	}
}