<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingModel extends CI_Model {


	public function subject()
	{
		$facultyID = $this->session->facultyid;

		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row()->subject;
	}

	public function batches()
	{
		$this->db->where_in('batch_id', json_decode($this->session->permissions));
		$this->db->join('classes', 'classes.class_id = batches.class_id');
		$this->db->join('branches', 'branches.branch_id = batches.branch_id');
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function periods()
	{
		$query = $this->db->get('periods');

		return $query->result();
	}

	public function submitted_timings($date)
	{
	    $date = (date('H:i') > '10:00')? date('Y-m-d') : date('Y-m-d', strtotime($date)); 

		$this->db->where('_date', $date);
		$this->db->where('marker_id', $this->session->facultyid);
		$this->db->where('status', 'bc_pending');
		$this->db->join('batches', 'batches.batch_id = timings.batch_id');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function slabtypes($centerID)
	{
		$this->db->select('slab_type_id, slab_type');
		$this->db->where('status', 'active');
		$this->db->where('branch_id', $centerID);
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function chapters($subjectID)
	{
		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('chapters');

		return $query->result();
	}
	
	public function faculty_list()
	{
	    $this->db->where('status', 'active');
	    $this->db->order_by('faculty_code','asc');
	    $this->db->where('faculty_type_id', '1');
	    $query = $this->db->get('faculties');
	    
	    return $query->result();
	}

	public function faculties($batchID)
	{
		$this->db->select('faculty_id, name, subject, batches');
		$this->db->where('faculty_type_id', '1');
		$this->db->where('status', 'active');
		$query = $this->db->get('faculties');

		$faculties = array();
		if($query->num_rows() > 0)
		{
			$facultyData = $query->result();
			for($i=0; $i < count($facultyData); $i++)
			{
				$batches = json_decode($facultyData[$i]->batches);

				if($batches != NULL)
				{
					if(in_array($batchID, $batches))
					{
						$faculties[$i] = $facultyData[$i];
					}
				}
			}

			return $faculties;
		}
		else
		{
			return $faculties;
		}
	}

	public function requests()
	{
		$this->db->select('timings.timing_id, timings.timing_uid, timings.timing_uid, time_table_id, timings.center_id, timings.class_id, timings.batch_id, timings.subject_id, timings.chapter_id, timings.slab_type_id, timings.period_id, timings.faculty_id, timings.start_time, timings.end_time, timings.batch_code, timings.chapter_code, timings.remarks, timings._date, batches.batch, slab_types.slab_type, faculties.name, faculties.faculty_code, subjects.subject, chapters.chapter');
		$this->db->where_in('timings.batch_id', json_decode($this->session->permissions));
		$this->db->where('_date', date('Y-m-d'));
		$this->db->where('timings.status', 'pending');
		$this->db->group_by('timing_uid');
		$this->db->order_by('start_time', 'ASC');
		$this->db->join('batches', 'batches.batch_id = timings.batch_id');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$this->db->join('subjects', 'subjects.subject_id = timings.subject_id', 'left');
		$this->db->join('chapters', 'chapters.chapter_id = timings.chapter_id', 'left');
		$query = $this->db->get('timings');

		return  $query->result();
	}

	public function centers()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}
	
	public function classes()
	{
		$query = $this->db->get('classes');

		return $query->result();
	}

	public function batches_list()
	{
		$batches = json_decode($this->session->permissions);

		$this->db->where_in('batches.batch_id', $batches);
		$this->db->join('classes', 'classes.class_id = batches.class_id');
		$this->db->join('branches', 'branches.branch_id = batches.branch_id');
		$query = $this->db->get('batches');

		return $query->result();
	}

	/**
	* Faculties DB
	*/
	/*public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$query = $this->db->get('faculties');

		return $query->result();
	}*/

	/**
	* Get time table for the requested date for the batch
	* Timing is stored for each faculty in timetable with bacth code
	* @param string $date, string $batchCode
	* @return array
	*/
	public function timings($date, $batchCode)
	{
		// Get timings
		$this->db->where('batch_code', $batchCode);
		$this->db->where('_date', $date);
		$this->db->where('time_table.bc_status', 'pending');
		$this->db->join('slab_types', 'slab_types.slab_type_id = time_table.slab_type_id', 'left');
		$this->db->join('faculties', 'faculties.faculty_code = time_table.faculty_code', 'right');
		$this->db->order_by('time_table.start_time', 'ASC');
		$query = $this->db->get('time_table');

		return $query->result();
	}

	public function submit($timing)
	{
				//Generate UID
		$this->db->select_max('timing_uid');
		$query = $this->db->get('timings');

		$timingUID = 1;
		$timingUID = ($query->num_rows() > 0)? $query->row()->timing_uid+1 : 1;

		$timing['timing_uid'] = $timingUID;
		
		$this->db->insert('timings', $timing);
	}

	public function update($timing, $timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->update('timings', $timing);
	}

	public function delete($timingID)
	{
		$this->db->where('timing_id', $timingID);
		$this->db->delete('timings');
	}

	public function update_status($timingID)
	{
		$timing['status'] = 'submitted';

		$this->db->where('timing_id', $timingID);
		$this->db->update('timings', $timing);
	}

	public function update_timetable($timetableID)
	{
		$timetable['bc_status'] = 'submitted';
		
		$this->db->where('time_table_id', $timetableID);
		$this->db->update('time_table', $timetable);
	}

	public function final_submission($timingID)
	{
		for ($i=0; $i < count($timingID); $i++) 
		{ 
			$timing['status'] = 'pending';

			$this->db->where('timing_id', $timingID[$i]);
			$this->db->update('timings', $timing);
		}
	}

	public function slab($slabtypeID, $facultyID)
	{
		$this->db->where('slab_type_id', $slabtypeID);
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('slabs');

		return $query->row();
	}

	public function approved($facultyID)
	{
		$this->db->where('status', 'approved');
		$this->db->where('faculty_id', $facultyID);
		$this->db->order_by('_date', 'DESC');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function approved_list()
	{
		$batches = json_decode($this->session->permissions);

		$this->db->where_in('timings.batch_id', $batches);
		$this->db->where('timings.status', 'approved');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->result();
	}
}