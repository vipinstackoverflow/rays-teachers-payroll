<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimeTableModel extends CI_Model {

	/**
	* Get batches that are allowed to managed by this teacher
	*/
	public function batches()
	{
		$this->db->where('batch_status', 'active');
		$this->db->where_in('batch_id', json_decode($this->session->permissions));
		$query = $this->db->get('batches');

		return $query->result();
	}

	/**
	* Get time table for the requested date for the batch
	* Timing is stored for each batch in timetable with batch code
	* Get batch id from session, get the batch codes for the same
	* @param string $date, string $facultyID
	* @return array
	*/
	public function timings($batchCode, $date)
	{
		// Get timings
		$this->db->where('batch_code', $batchCode);
		$this->db->where('_date', $date);
		$this->db->join('slab_types', 'slab_types.slab_type_id = time_table.slab_type_id', 'left');
		$this->db->order_by('time_table.start_time', 'ASC');
		$query = $this->db->get('time_table');

		return $query->result();
	}
}