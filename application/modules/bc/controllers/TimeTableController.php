<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimeTableController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('TimeTableModel');
	}

	private function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
		elseif ($this->session->usertype != '3') 
		{
			redirect('user/login');
		}
	}

	public function batches()
	{
		$data['batches'] = $this->TimeTableModel->batches();

		$this->load->view('time-table/batches', $data);
	}

	public function view($batchCode, $date)
	{
		$date			 = date('Y-m-d', strtotime($date));

		$data['timings'] = $this->TimeTableModel->timings($batchCode, $date);

		$this->load->view('time-table/view', $data);
	}
}