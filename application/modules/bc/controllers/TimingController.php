<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('TimingModel');
	}

	private function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
		elseif ($this->session->usertype != '3') 
		{
			redirect('user/login');
		}
	}

	public function batches()
	{
		$data['batches'] = $this->TimingModel->batches();

		$this->load->view('timing/batches', $data);
	}

	public function timings($date)
	{
		//$data['requests']	= $this->TimingModel->requests();

		$data['subject']    = $this->TimingModel->subject();
		$data['batches']    = $this->TimingModel->batches();
		$data['periods']    = $this->TimingModel->periods();
		$data['faculties']  = $this->TimingModel->faculty_list();
		$timings 		    = $this->TimingModel->submitted_timings($date);

		$timingData = array();
		for ($i=0; $i < count($timings); $i++) 
		{ 
			$timingData[$i] = $timings[$i];
			$timingData[$i]->chapters 	= $this->TimingModel->chapters($timings[$i]->subject_id);
			$timingData[$i]->slabtypes 	= $this->TimingModel->slabtypes($timings[$i]->center_id);
			$timingData[$i]->faculties 	= $this->TimingModel->faculties($timings[$i]->batch_id);
		}

		$data['timings'] = $timingData;

		$this->load->view('timing/timings', $data);
	}

	public function approved()
	{
		$data['approved'] = $this->TimingModel->approved_list();

		$this->load->view('timing/approved', $data);
	}

	public function submit()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('ttdate', 'Date', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('center', 'Center', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');
		$this->form_validation->set_rules('batch', 'Batch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('slab', 'Slab', 'required|xss_clean|trim');
		$this->form_validation->set_rules('start', 'Started Time', 'required|xss_clean|trim');
		$this->form_validation->set_rules('end', 'Ended Time', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('remarks', 'Remarks', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$slabtypeID 				= $this->input->post('slab');
			$facultyID 					= $this->session->facultyid;
			$slab 						= $this->TimingModel->slab($slabtypeID, $facultyID);
			$rate 						= (isset($slab))? $slab->rate : 0;
			$extra 						= (isset($slab))? $slab->extra : 0;
			$amount						= ($rate+$extra)/60;
			$batch 						= explode("|", $this->input->post('batch'));

			$timing['time_table_id'] 	= '0';
			$timing['center_id'] 		= $batch[0];
			$timing['class_id'] 		= $batch[1];
			$timing['batch_id'] 		= $batch[2];
			$subject 					= explode('|', $this->input->post('chapter'));
			$timing['subject_id'] 		= $subject[0];
			$timing['chapter_id'] 		= $subject[1];
			$timing['slab_type_id']	 	= $slabtypeID;
			$timing['period_id']	 	= $this->input->post('period');
			$timing['faculty_id'] 		= $this->input->post('faculty');
			$timing['marker_id'] 		= $this->session->facultyid;
			$timing['start_time'] 		= date('H:i:s', strtotime($this->input->post('start')));
			$timing['end_time'] 		= date('H:i:s', strtotime($this->input->post('end')));
			$minutes 					= round(abs(strtotime($timing['end_time']) - strtotime($timing['start_time'])) / 60,2);
			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));
			$timing['payment'] 			= $minutes*$amount;
			$timing['batch_code'] 		= $batch[3];
			$timing['chapter_code'] 	= $subject[2];
			$timing['remarks'] 			= $this->input->post('remarks');
			$timing['_date'] 			= date('Y-m-d', strtotime($this->input->post('ttdate')));
			$timing['user'] 			= $this->session->username;
			$timing['status'] 			= 'bc_pending';

			$this->db->trans_start();

				//Submit timing for review
				$this->TimingModel->submit($timing);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Timing added');

			redirect('timings/new/'.$this->input->post('ttdate'));
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('timings/new/'.date('d-m-Y'));
		}
	}	

	public function update($timingID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('ttdate', 'Date', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('center', 'Center', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');
		$this->form_validation->set_rules('batch', 'Batch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('slab', 'Slab', 'required|xss_clean|trim');
		$this->form_validation->set_rules('start', 'Started Time', 'required|xss_clean|trim');
		$this->form_validation->set_rules('end', 'Ended Time', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('remarks', 'Remarks', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$slabtypeID 				= $this->input->post('slab');
			$facultyID 					= $this->session->facultyid;
			$slab 						= $this->TimingModel->slab($slabtypeID, $facultyID);
			$rate 						= (isset($slab))? $slab->rate : 0;
			$extra 						= (isset($slab))? $slab->extra : 0;
			$amount						= ($rate+$extra)/60;
			$batch 						= explode("|", $this->input->post('batch'));

			$timing['time_table_id'] 	= '0';
			$timing['center_id'] 		= $batch[0];
			$timing['class_id'] 		= $batch[1];
			$timing['batch_id'] 		= $batch[2];
			$subject 					= explode('|', $this->input->post('chapter'));
			$timing['subject_id'] 		= $subject[0];
			$timing['chapter_id'] 		= $subject[1];
			$timing['slab_type_id']	 	= $slabtypeID;
			$timing['period_id']	 	= $this->input->post('period');
			$timing['faculty_id'] 		= $this->input->post('faculty');
			$timing['marker_id'] 		= $this->session->facultyid;
			$timing['start_time'] 		= date('H:i:s', strtotime($this->input->post('start')));
			$timing['end_time'] 		= date('H:i:s', strtotime($this->input->post('end')));
			$minutes 					= round(abs(strtotime($timing['end_time']) - strtotime($timing['start_time'])) / 60,2);
			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));
			$timing['payment'] 			= $minutes*$amount;
			$timing['batch_code'] 		= $batch[3];
			$timing['chapter_code'] 	= $subject[2];
			$timing['remarks'] 			= $this->input->post('remarks');
			$timing['_date'] 			= date('Y-m-d', strtotime($this->input->post('ttdate')));
			$timing['user'] 			= $this->session->username;
			$timing['status'] 			= 'bc_pending';

			$this->db->trans_start();

				//Submit timing for review
				$this->TimingModel->update($timing, $timingID);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Updated');

			redirect('timings');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('timings');
		}
	}

	public function delete($timingID)
	{
		$this->db->trans_start();

			$this->TimingModel->delete($timingID);	

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('timings');
	}

	public function final_submission()
	{
		$timingID = $this->input->post('timings');

		$this->db->trans_start();

			$this->TimingModel->final_submission($timingID);

		$this->db->trans_complete();
		
		echo ($this->db->trans_status() === FALSE)? FALSE : TRUE;
	}
}