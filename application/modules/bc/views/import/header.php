<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Rays Faculty Payroll</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url('assets/admin/images/favicon.ico'); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('assets/admin/images/favicon.ico'); ?>" type="image/x-icon">
    <meta name="robots" content="noindex">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/main.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/jquery-ui.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/dataTable/css/jquery.dataTables.min.css'); ?>">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/dataTable/css/buttons.dataTables.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/easy-autocomplete.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/datepicker/css/datepicker.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/daterangepicker/css/jquery.comiseo.daterangepicker.css'); ?>">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/fullcalendar.min.css'); ?>">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/jquery.timepicker.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/multiselect/kendo.common-material.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/multiselect/kendo.material.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/multiselect/kendo.material.mobile.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/css/MonthPicker.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/daterangepicker/css/daterangepicker.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/timepicker/jquery.timepicker.min.css'); ?>"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/time-picker/mdtimepicker.min.css'); ?>"> 
  </head>
  <body class="app sidebar-mini rtl">
    <header class="app-header"><a class="app-header__logo" href="<?php echo base_url(); ?>">Faculty PayRoll</a>
      <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <ul class="app-nav">
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<?php echo base_url('password/change-password'); ?>"><i class="fa fa-key fa-lg"></i> Change Password</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('user/logout'); ?>"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
        <?php $menu = $this->uri->segment(1); ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo base_url('assets/admin/images/avatar.png'); ?>" width="50" alt="User Image">
        <div>
          <p class="app-sidebar__user-name"><?php echo $this->session->username; ?></p>
          <p class="app-sidebar__user-designation">Faculty PayRoll</p>
        </div>
      </div>
      <ul class="app-menu">
        <li>
            <a class="app-menu__item <?php if($menu == 'bc') { ?> active <?php } ?>" href="<?php echo base_url('bc/userpanel'); ?>">
              <i class="app-menu__icon fa fa-dashboard"></i>
              <span class="app-menu__label">Dashboard</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item <?php if($menu == 'time-table') { ?> active <?php } ?>" href="<?php echo base_url('time-table/batches'); ?>">
              <i class="app-menu__icon fa fa-calendar"></i>
              <span class="app-menu__label">Time Table</span>
            </a>
        </li>
        <li>
            <a class="app-menu__item <?php if($menu == 'timings') { ?> active <?php } ?>" href="<?php echo base_url('timings/new/'.date('d-m-Y')); ?>">
              <i class="app-menu__icon fa fa-clock-o"></i>
              <span class="app-menu__label">Mark Timing</span>
            </a>
        </li>
      </ul>
    </aside>