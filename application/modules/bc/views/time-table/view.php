   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-calendar"></i> Time Table</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Time Table</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
            <div class="row" >
              <div class="col-md-12 table-responsive" >
                <a href="<?php echo base_url('time-table/batches'); ?>" class="btn btn-sm btn-success" ><- Back</a><br><br>
                <input type="text" id="tdate" name="tdate" class="form-control" width="50%" value="<?php echo $this->uri->segment(4); ?>" ><br>
                <input type="hidden" id="batchcode" name="batchcode" class="form-control" width="50%" value="<?php echo $this->uri->segment(3); ?>" >
                <table id="timetable" name="timetable" class="table" >
                  <thead>
                    <tr>
                      <td>Timing</td>
                      <td>Batch</td>
                      <td>Class</td>
                      <td>Chapter</td> 
                      <td>Time Left</td>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach($timings as $data) { ?>
                    <tr>
                      <td><?php echo date('h:i A', strtotime($data->start_time))." - ".date('h:i A', strtotime($data->end_time)); ?></td>
                      <td><?php echo $data->batch_code; ?></td>
                      <td><?php echo $data->slab_type; ?></td>
                      <td><?php echo $data->chapter_code; ?></td>
                      <td><?php echo $data->time_left; ?></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div> -->
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/multiselect/kendo.all.min.js'); ?>"></script>

    <script type="text/javascript">
      $("#tdate").datepicker({
        format:"dd-mm-yyyy",
        autoclose:true
      });

      $("#tdate").on('change', function(){
        date = $("#tdate").val();
        batch = $("#batchcode").val();

        window.location = "<?php echo base_url('time-table/view/') ?>"+batch+"/"+date;

      });

    </script>
    <?php $this->load->view('import/footer'); ?>