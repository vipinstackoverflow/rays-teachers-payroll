   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
      </div>
      <div class="row">
      	<div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('timings/new/'.date('d-m-Y')); ?>">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-clock-o fa-3x"></i>
            <div class="info">
              <h4>Timings</h4>
              <p><b>Mark Ur Timings</b></p>
            </div>
          </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('time-table/batches'); ?>">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-calendar fa-3x"></i>
            <div class="info">
              <h4>Time Table</h4>
              <p><b>View Timings</b></p>
            </div>
          </div>
          </a>
        </div>
        
        <!-- <div class="col-md-6 col-lg-3">
          <a href="<?php echo base_url('timings/approved') ?>">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-check fa-3x"></i>
            <div class="info">
              <h4>Approved Timings</h4>
              <p><b>View</b></p>
            </div>
          </div>
          </a>
        </div> -->
        <div class="col-md-6 col-lg-3">
         <!--  <a href="<?php echo base_url('timings/approved') ?>"> -->
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-graduation-cap"></i>
            <div class="info">
              <h4>Attendence</h4>
              <p><b>Students Attendance</b></p>
            </div>
          </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-3">
         <!--  <a href="<?php echo base_url('timings/approved') ?>"> -->
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-puzzle-piece"></i>
            <div class="info">
              <h4>Upload Viva</h4>
              <p><b>Upload Viva</b></p>
            </div>
          </div>
          </a>
        </div>

      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/footer'); ?>