   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-clock-o"></i> Mark Timing</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Mark Timing</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h3 class="title" >Edit & Approve</h3>
              <div class="row">
                <?php foreach($requests as $data) { ?>
                <div class="col-md-6">
                <?php echo form_open('timings/submit', 'id="mark"'); ?>
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title"><?php echo $data->batch; ?></h3>
                    </div>
                    <div class="tile-body">
                      <b><?php echo date('d-m-Y', strtotime($data->_date)); ?></b><br>
                      <b><?php echo date('h:ia', strtotime($data->start_time))." - ".date('h:ia', strtotime($data->end_time)); ?></b><br>
                      <b>Faculty - <?php echo $data->faculty_code." ".$data->name; ?></b><br>
                      <b><?php echo $data->slab_type; ?></b><br>
                      <b><?php echo $data->subject." - ".$data->chapter; ?></b><br><br>
                      <input type="hidden" name="timingid" value="<?php echo $data->timing_id; ?>" >
                      <input type="hidden" name="timetableid" value="<?php echo $data->time_table_id; ?>" >
                      <input type="hidden" name="timing_uid" value="<?php echo $data->timing_uid; ?>" >

                      <input type="hidden" name="centerid" value="<?php echo $data->center_id; ?>" >
                      <input type="hidden" name="classid" value="<?php echo $data->class_id; ?>" >
                      <input type="hidden" name="batchid" value="<?php echo $data->batch_id; ?>" >
                      <input type="hidden" name="subjectid" value="<?php echo $data->subject_id; ?>" >
                      <input type="hidden" name="chapterid" value="<?php echo $data->chapter_id; ?>" >
                      <input type="hidden" name="slabtypeid" value="<?php echo $data->slab_type_id; ?>" >
                      <input type="hidden" name="periodid" value="<?php echo $data->period_id; ?>" >
                      <input type="hidden" name="facultyid" value="<?php echo $data->faculty_id ?>" >
                      <input type="hidden" name="markerid" value="<?php echo $this->session->facultyid; ?>" >
                      <input type="hidden" name="batchcode" value="<?php echo $data->batch_code; ?>" >
                      <input type="hidden" name="chaptercode" value="<?php echo $data->chapter_code; ?>" >
                      <input type="hidden" name="tdate" value="<?php echo $data->_date; ?>" >

                      <div class="form-group row">
                        <label class="control-label col-md-3">Started Time</label>
                        <div class="col-md-8">
                          <input class="form-control time" type="text" name="start" id="start" placeholder="Started Time" value="<?php echo date('h:ia', strtotime($data->start_time)); ?>" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Ended Time</label>
                        <div class="col-md-8">
                          <input class="form-control time" type="text" name="end" id="end" placeholder="Ended Time" value="<?php echo date('h:ia', strtotime($data->end_time)); ?>" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-3">Remarks</label>
                        <div class="col-md-8">
                          <textarea class="form-control" name="remarks" id="remarks" placeholder="Topic Name, . . etc" ><?php echo $data->remarks; ?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div>
                  </div>
                <?php echo form_close(); ?>
                </div>
              <?php } ?>
              </div>
            <!-- <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div> -->
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      /*$("#ttdate").datepicker({
        format:"dd-mm-yyyy",
        todayHighlight:true,
        autoclose:true
      });*/

      $(".time").timepicker({ 
          'timeFormat': 'h:ia',
          'step' : 5 
        });

      $(".getbatch").on('change', function(){

        centerid  = $("#center").val();
        classid   = $("#class").val();

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/batches'); ?>",
          dataType:"json",
          data:{ centerid, classid },
          success:function(res){
            options = '';
            $.each(res, function(i, data){
              options += "<option value='"+data.batch_id+"|"+data.batch_code+"' >"+data.batch+"</option>";
            });
            $("#batch").html(options);
          },
          error:function(res){

          }
        })        

        $.ajax({
          type:"POST",
          url:"<?php echo base_url('api/v1/slabtypes'); ?>",
          dataType:"json",
          data:{ centerid },
          success:function(res){
            option = '';
            $.each(res, function(i, data){
              option += "<option value='"+data.slab_type_id+"' >"+data.slab_type+"</option>";
            });
            $("#slab").html(option);
          },
          error:function(res){

          }
        })

      });

      $("#mark").validate({
        rules:{
          facultyid:{
            required:true
          },
          ttdate:{
            required:true
          },
          center:{
            required:true
          },
          class:{
            required:true
          },
          batch:{
            required:true
          },
          slab:{
            required:true
          }
        },
        messages:{
          facultyid:{
            required:"Please choose Faculty"
          },
          ttdate:{
            required:"Please choose Date"
          },
          center:{
            required:"Please choose center"
          },
          class:{
            required:"Please choose class"
          },
          batch:{
            required:"Please choose batch"
          },
          slab:{
              required:"Please choose Slab"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>