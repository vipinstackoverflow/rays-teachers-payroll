<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SlabTypeController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('SlabTypeModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('3', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function slab_types()
	{
		$data['slabtypes'] = $this->SlabTypeModel->slab_types();

		$this->load->view('slab/slab-types', $data);
	}

	public function save()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('slab_type','Slab Type', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$slab['slab_type'] = $this->input->post('slab_type');

			$this->db->trans_start();

				$this->SlabTypeModel->save($slab);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Slab Type added');

			redirect('slab/slab-types');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('slab/slab-types');
		}
	}	

	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('slab_type','Slab Type', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$slabtypeID 		= $this->input->post('slabtypeid');
			$slab['slab_type'] 	= $this->input->post('slab_type');

			$this->db->trans_start();

				$this->SlabTypeModel->update($slab, $slabtypeID);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Slab Type updated');

			redirect('slab/slab-types');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('slab/slab-types');
		}
	}

	public function delete($slabtypeID)
	{
		$this->db->trans_start();

			$this->SlabTypeModel->delete($slabtypeID);

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('slab/slab-types');
	}
}