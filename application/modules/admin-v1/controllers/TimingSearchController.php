<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingSearchController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('TimingSearchModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('5', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function search()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('facultyid', 'Faculty ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('daterange', 'Date Range', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$facultyID = $this->input->post('facultyid');
			$daterange = $this->input->post('daterange');
			$PaySlip = $this->input->post('PaySlip');

			$daterange = explode('-', $daterange);
			$from 	   = str_replace('/', '-', $daterange[0]);
			$to 	   = str_replace('/', '-', $daterange[1]);

			$data['approved'] 	= $this->TimingSearchModel->search($from, $to, $facultyID);
			$data['summary'] 	= $this->TimingSearchModel->search_summary($from, $to, $facultyID);
			$data['faculties'] 	= $this->TimingSearchModel->faculties();
			$data['date'] 		= $from." - ".$to;
			$data['faculty'] 	= $this->TimingSearchModel->faculty($facultyID);

			if($PaySlip)
			{
				$this->load->view('payslip/payslip', $data);
			}
			else
			{
				$this->load->view('timing/search', $data);
			}
		}
		else
		{

		}
	}
}