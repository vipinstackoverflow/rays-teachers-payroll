<?php defined('BASEPATH') OR exit('No direct script access allowed');

class InstituteController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('InstituteModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('1', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function centers()
	{
		$data['centers'] = $this->InstituteModel->centers();

		$this->load->view('institute/centers', $data);
	}

	public function save_center()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('branch', 'Center', 'required|xss_clean|trim');
		$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|trim');
		$this->form_validation->set_rules('address', 'Address', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$branch['branch'] 			= $this->input->post('branch');
			$branch['branch_phone'] 	= $this->input->post('phone');
			$branch['branch_address'] 	= $this->input->post('address');

			$this->db->trans_start();

				$facultyID = $this->InstituteModel->save_center($branch);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Added successfully'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/centers');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/centers');
		}
	}	

	public function update_center()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('branch', 'Center', 'required|xss_clean|trim');
		$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|trim');
		$this->form_validation->set_rules('address', 'Address', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$branchID 					= $this->input->post('branchid');
			$branch['branch'] 			= $this->input->post('branch');
			$branch['branch_phone'] 	= $this->input->post('phone');
			$branch['branch_address'] 	= $this->input->post('address');

			$this->db->trans_start();

				$facultyID = $this->InstituteModel->update_center($branch, $branchID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
			$this->session->set_flashdata('success', 'Updated');

			redirect('institute/centers');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/centers');
		}
	}

	public function delete_center($branchID)
	{
		$this->db->trans_start();

			$this->InstituteModel->delete_center($branchID);
		
		$this->db->trans_complete();


		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('institute/centers');
	}
}