<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('FacultyModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('2', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function faculties()
	{
		$data['faculties'] 	= $this->FacultyModel->faculties();
		$data['branches'] 	= $this->FacultyModel->branches();
		$data['batches'] 	= $this->FacultyModel->batches();

		$this->load->view('faculty/faculties', $data);
	}

	public function employee($employeeID)
	{
		$data['employee'] 	= $this->FacultyModel->employee($employeeID);

		$this->load->view('faculty/view', $data);
	}
}