<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReportController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('ReportModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('5', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function report()
	{
		$this->load->view('timing/report');
	}

	public function search_report()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('daterange', 'Date Range', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$daterange = $this->input->post('daterange');

			$daterange = explode('-', $daterange);
			$from 	   = str_replace('/', '-', $daterange[0]);
			$to 	   = str_replace('/', '-', $daterange[1]);

			$faculties 			= $this->ReportModel->faculties();

			$reportData 		= array();
			for ($i=0; $i < count($faculties); $i++) 
			{ 
				$reportData[$i] 		= $faculties[$i];
				$reportData[$i]->report = $this->ReportModel->report($faculties[$i]->faculty_id, $from, $to);
			}

			$data['faculties'] 	= $reportData;
			$data['date'] 		= $from." - ".$to;

			$this->load->view('timing/report', $data);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('timing-requests/report');
		}
	}
}