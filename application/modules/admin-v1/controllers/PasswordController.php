<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PasswordController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();

		$this->load->model('PasswordModel');
	}

	private function isuservalid()
	{
		if(!$this->session->user)
		{
			redirect('user/login');
		}
	}

	public function change_password()
	{
		$this->load->view('password/change-password');
	}

	public function update_password()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('current_pass','Current Password','required|xss_clean|trim|max_length[12]');
		$this->form_validation->set_rules('new_pass','New Password','required|xss_clean|trim|min_length[6]|max_length[12]');
		$this->form_validation->set_rules('password','Confirm Password','required|xss_clean|trim|min_length[6]|max_length[12]|matches[new_pass]');

		if($this->form_validation->run() == TRUE)
		{
			$password['current'] 	= $this->input->post('current_pass');
			$password['password']	= $this->input->post('password');

			// validate current password

			if($this->PasswordModel->validate_current_password($password))
			{
				if($this->PasswordModel->update_password($password))
				{
					$this->session->set_flashdata('success','Password updated successfully');
					
					redirect('password/change-password');

				}
				else
				{
					$this->session->set_flashdata('error','Something went wrong ,please try again later.');

					redirect('password/change-password');
				}

			}
			else
			{
				redirect('password/change-password');
			}

		}
		else
		{
			$this->session->set_flashdata('error',validation_errors());

			redirect('password/change-password');
		}
	}
}
