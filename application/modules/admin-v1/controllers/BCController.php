<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BCController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('BCModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('2', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function batch_in_charges()
	{
		$bc 				= $this->BCModel->batch_in_charges();
		$bcData				= array();
		for ($i=0; $i < count($bc); $i++) 
		{ 
			$bcData[$i] 			= $bc[$i];
			$bcData[$i]->batch 	= $this->BCModel->batch($bc[$i]->batches);
		}
		$data['bc']			= $bcData;
		$data['branches'] 	= $this->BCModel->branches();
		$data['batches'] 	= $this->BCModel->batches();

		$this->load->view('bc/bc', $data);
	}
}