<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BatchController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('BatchModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('4', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function batches($classID)
	{
		$data['class'] 		= $this->BatchModel->class_meta($classID);
		$data['batches'] 	= $this->BatchModel->batches($classID);
		$data['subjects'] 	= $this->BatchModel->subjects($classID);
		$data['branches'] 	= $this->BatchModel->branches();

		$this->load->view('institute/batches', $data);
	}

	public function save($classID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('batch', 'Batch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('branch', 'Center', 'required|xss_clean|trim');
		$this->form_validation->set_rules('subjects[]', 'Subjects', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$batch['class_id'] 	= $classID;
			$batch['branch_id'] = $this->input->post('branch');
			$batch['batch'] 	= $this->input->post('batch');
			$batch['batch_code']= str_replace(' ', '-', $this->input->post('batch')); // internal use
			$batch['subjects'] 	= json_encode($this->input->post('subjects'));

			$this->db->trans_start();

				$this->BatchModel->save($batch);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Added'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/batches/'.$classID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/batches/'.$classID);
		}
	}

	public function batch_code()
	{
		$query = $this->db->get('batches');

		foreach ($query->result() as $value) 
		{
			$data['batch_code'] = str_replace(' ', '-', $value->batch);

			$this->db->where('batch_id', $value->batch_id);
			$this->db->update('batches', $data);
		}
	}
	
	public function update($classID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('batchid', 'Batch ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('branch', 'Center', 'required|xss_clean|trim');
		$this->form_validation->set_rules('batch', 'Batch', 'required|xss_clean|trim');
		$this->form_validation->set_rules('subjects[]', 'Subjects', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$batchID			= $this->input->post('batchid');
			$batch['class_id'] 	= $classID;
			$batch['branch_id'] = $this->input->post('branch');
			$batch['batch'] 	= $this->input->post('batch');
			$batch['batch_code']= str_replace(' ', '-', $this->input->post('batch')); //internal use
			$batch['subjects'] 	= json_encode($this->input->post('subjects'));

			$this->db->trans_start();

				$this->BatchModel->update($batch, $batchID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/batches/'.$classID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/batches/'.$classID);
		}
	}	

	public function delete($classID, $batchID)
	{
		$this->db->trans_start();

			$this->BatchModel->delete($batchID);
		
		$this->db->trans_complete();


		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('institute/batches/'.$classID);
	}
}