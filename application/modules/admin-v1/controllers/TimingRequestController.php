<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingRequestController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('TimingRequestModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('5', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function date($date)
	{

		$faculties = $this->TimingRequestModel->faculties();

		$facultyData = array();
		for ($i=0; $i < count($faculties); $i++) 
		{ 
			$requests = $this->TimingRequestModel->timing_requests($faculties[$i]->faculty_id, $date);

			if(count($requests) > 0)
			{
				$facultyData[$i] = $faculties[$i];
				$facultyData[$i]->requests = count($requests);
			}
		}

		$data['faculties'] = $facultyData;

		$this->load->view('timing/date', $data);
	}
	public function unsubmitted($date)
	{
		$faculties = $this->TimingRequestModel->faculties();

		$facultyData = array();
		for ($i=0; $i < count($faculties); $i++) 
		{ 
			$requests = $this->TimingRequestModel->timing_requests($faculties[$i]->faculty_id, $date);

			if(count($requests) < 1)
			{
				$facultyData[$i] = $faculties[$i];
			}
		}

		$data['faculties'] 	= $facultyData;		
		$data['date'] 		= $date;		

		$this->load->view('timing/unsubmitted', $data);
	}

	public function faculties()
	{
		$faculties = $this->TimingRequestModel->faculties();

		$facultyData = array();
		for ($i=0; $i < count($faculties); $i++) 
		{ 
			$requests = $this->TimingRequestModel->timing_requests($faculties[$i]->faculty_id);

			$facultyData[$i] = $faculties[$i];
			$facultyData[$i]->requests = count($requests);
		}

		$data['faculties'] = $facultyData;

		$this->load->view('timing/faculties', $data);
	}

	public function view_requests($facultyID, $date)
	{
		$date 				= date('Y-m-d', strtotime($date));
		$data['approved'] 	= $this->TimingRequestModel->approved($facultyID, $date);
		$data['bc'] 		= $this->TimingRequestModel->bc_requests($facultyID, $date);
		$data['faculty'] 	= $this->TimingRequestModel->faculty_requests($facultyID, $date);
		$data['timetable'] 	= $this->TimingRequestModel->faculty_timetable($facultyID, $date);
		$data['facultymeta']= $this->TimingRequestModel->faculty($facultyID);

		$this->load->view('timing/requests', $data);
	}

	public function add_faculty_request_to_bc($fID, $date)
	{
		try {
			
			$slabtypeID 				= $this->input->post('slab_type_id');
			$facultyID 					= $this->input->post('faculty_id');
			$slab 						= $this->TimingRequestModel ->slab($slabtypeID, $facultyID);
			$rate 						= (isset($slab))? $slab->rate : 0;
			$extra 						= (isset($slab))? $slab->extra : 0;
			$amount						= ($rate+$extra)/60;
			$batchID 					= $this->input->post('batch_id');

			$facultyUID					= $this->TimingRequestModel->batch_in_charge($batchID);

			if(empty($facultyUID)){
				throw new Exception("No BC Found for the corresponding Batch");
			}

			$timing['time_table_id'] 	= '0';
			$timing['center_id'] 		= $this->input->post('center_id');
			$timing['class_id'] 		= $this->input->post('class_id');
			$timing['batch_id'] 		= $this->input->post('batch_id');
			$timing['subject_id'] 		= $this->input->post('subject_id');
			$timing['chapter_id'] 		= $this->input->post('chapter_id');
			$timing['slab_type_id']	 	= $slabtypeID;
			$timing['period_id']	 	= $this->input->post('period_id');
			$timing['faculty_id'] 		= $facultyID;
			$timing['marker_id'] 		= $facultyUID;
			$timing['start_time'] 		= date('H:i:s', strtotime($this->input->post('start_time')));
			$timing['end_time'] 		= date('H:i:s', strtotime($this->input->post('end_time')));
			$minutes 					= round(abs(strtotime($timing['end_time']) - strtotime($timing['start_time'])) / 60,2);
			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));
			$timing['payment'] 			= $minutes*$amount;
			$timing['batch_code'] 		= $this->input->post('batch_code');
			$timing['chapter_code'] 	= $this->input->post('chapter_code');
			$timing['remarks'] 			= $this->input->post('remarks');
			$timing['_date'] 			= $this->input->post('_date');
			$timing['user'] 			= $this->session->username;
			$timing['status'] 			= 'bc_pending';

			$this->db->trans_start();

				$this->TimingRequestModel->submit($timing);

			$this->db->trans_complete(); 
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
			$this->session->set_flashdata('success', 'Added');

			redirect('timing-requests/'.$fID.'/'.$date);

		} catch (Exception $e) {
			$this->session->set_flashdata('error', $e->getMessage());
			redirect('timing-requests/'.$fID.'/'.$date);
		}
	}

	public function delete_timing($timingID, $fID, $date)
	{
		$this->db->trans_start();

			$this->TimingRequestModel->delete_timing($timingID);

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('timing-requests/'.$fID.'/'.$date);
	}

	public function final_approval($facultyUID, $date)
	{
		$timings = $this->input->post(NULL, TRUE);

		$timingData = array();
		for ($i=0; $i < count($timings['time_table_id']); $i++) 
		{ 
			$slabtypeID 				= $timings['slab_type_id'][$i];
			$facultyID 					= $timings['faculty_id'][$i];

			$slab 						= $this->TimingRequestModel->slab($slabtypeID, $facultyID);
			$rate 						= (isset($slab))? $slab->rate : 0;
			$extra 						= (isset($slab))? $slab->extra : 0;
			$amount						= ($rate+$extra)/60;

			$timing['timing_uid'] 		= $timings['timing_uid'][$i];
			$timing['time_table_id'] 	= $timings['time_table_id'][$i];
			$timing['center_id'] 		= $timings['center_id'][$i];
			$timing['class_id'] 		= $timings['class_id'][$i];
			$timing['batch_id'] 		= $timings['batch_id'][$i];
			$timing['subject_id'] 		= $timings['subject_id'][$i];
			$timing['chapter_id'] 		= $timings['chapter_id'][$i];
			$timing['slab_type_id']	 	= $timings['slab_type_id'][$i];
			$timing['period_id']	 	= $timings['period_id'][$i];
			$timing['faculty_id'] 		= $timings['faculty_id'][$i];
			$timing['marker_id'] 		= $timings['marker_id'][$i];
			$timing['start_time'] 		= date('H:i:s', strtotime($timings['start_time'][$i]));
			$timing['end_time'] 		= date('H:i:s', strtotime($timings['end_time'][$i]));
			$minutes 					= round(abs(strtotime($timings['end_time'][$i]) - strtotime($timings['start_time'][$i])) / 60,2);
			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));
			$timing['payment'] 			= $minutes*$amount;
			$timing['batch_code'] 		= $timings['batch_code'][$i];
			$timing['chapter_code'] 	= $timings['chapter_code'][$i];
			$timing['remarks'] 			= $timings['remarks'][$i];
			$timing['_date'] 			= $timings['_date'][$i];
			$timing['user'] 			= $this->session->username;
			$timing['status'] 			= 'approved';

			$timingData[] = $timing;
		}

		$this->db->trans_start();

			$this->TimingRequestModel->final_approval($timingData, $timings);

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Approved');

		redirect('timing-requests/'.$facultyUID.'/'.$date);
	}

	public function update_final_approval($facultyUID, $date)
	{
		$timings = $this->input->post(NULL, TRUE);

		$timingData = array();
		for ($i=0; $i < count($timings['time_table_id']); $i++) 
		{ 
			$slabtypeID 				= $timings['slab_type_id'][$i];
			$facultyID 					= $timings['faculty_id'][$i];

			$slab 						= $this->TimingRequestModel->slab($slabtypeID, $facultyID);
			$rate 						= (isset($slab))? $slab->rate : 0;
			$extra 						= (isset($slab))? $slab->extra : 0;
			$amount						= ($rate+$extra)/60;

			$timing['timing_id'] 		= $timings['timing_id'][$i];
			$timing['timing_uid'] 		= $timings['timing_uid'][$i];
			$timing['time_table_id'] 	= $timings['time_table_id'][$i];
			$timing['center_id'] 		= $timings['center_id'][$i];
			$timing['class_id'] 		= $timings['class_id'][$i];
			$timing['batch_id'] 		= $timings['batch_id'][$i];
			$timing['subject_id'] 		= $timings['subject_id'][$i];
			$timing['chapter_id'] 		= $timings['chapter_id'][$i];
			$timing['slab_type_id']	 	= $timings['slab_type_id'][$i];
			$timing['period_id']	 	= $timings['period_id'][$i];
			$timing['faculty_id'] 		= $timings['faculty_id'][$i];
			$timing['marker_id'] 		= $timings['marker_id'][$i];
			$timing['start_time'] 		= date('H:i:s', strtotime($timings['start_time'][$i]));
			$timing['end_time'] 		= date('H:i:s', strtotime($timings['end_time'][$i]));
			$minutes 					= round(abs(strtotime($timings['end_time'][$i]) - strtotime($timings['start_time'][$i])) / 60,2);
			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));
			$timing['payment'] 			= $minutes*$amount;
			$timing['batch_code'] 		= $timings['batch_code'][$i];
			$timing['chapter_code'] 	= $timings['chapter_code'][$i];
			$timing['remarks'] 			= $timings['remarks'][$i];
			$timing['_date'] 			= $timings['_date'][$i];
			$timing['user'] 			= $this->session->username;
			$timing['status'] 			= 'approved';

			$timingData[] = $timing;
		}

		$this->db->trans_start();

			$this->TimingRequestModel->update_final_approval($timingData, $timings);

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Updated');

		redirect('timing-requests/'.$facultyUID.'/'.$date);
	}

	public function submit($facultyID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('ttid', 't ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('uid', 'u ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('slabtypeid', 'slab ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('start', 'Started Time', 'required|xss_clean|trim');
		$this->form_validation->set_rules('end', 'Ended Time', 'required|xss_clean|trim');
		//$this->form_validation->set_rules('remarks', 'Remarks', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$timingID 					= $this->input->post('tid');
			$slabtypeID 				= $this->input->post('slabtypeid');
			$facultyID 					= $this->input->post('facultyid');
			$slab 						= $this->TimingRequestModel->slab($slabtypeID, $facultyID);

			$rate 						= (isset($slab))? $slab->rate : 0;
			$extra 						= (isset($slab))? $slab->extra : 0;
			$amount						= ($rate+$extra)/60;

			$timing['time_table_id'] 	= $this->input->post('ttid');
			$timing['timing_uid'] 		= $this->input->post('uid');
			$timing['center_id'] 		= $this->input->post('cid');
			$timing['class_id'] 		= $this->input->post('classid');
			$timing['batch_id'] 		= $this->input->post('batchid');
			$timing['subject_id'] 		= $this->input->post('subjectid');
			$timing['chapter_id'] 		= $this->input->post('chapterid');
			$timing['slab_type_id']	 	= $slabtypeID;
			$timing['faculty_id'] 		= $this->input->post('facultyid');
			$timing['marker_id'] 		= $this->session->facultyid;
			$timing['start_time'] 		= date('H:i:s', strtotime($this->input->post('start')));
			$timing['end_time'] 		= date('H:i:s', strtotime($this->input->post('end')));
			$minutes 					= round(abs(strtotime($timing['end_time']) - strtotime($timing['start_time'])) / 60,2);
			$timing['duration'] 		= gmdate("H:i:s", ($minutes * 60));
			$timing['payment'] 			= $minutes*$amount;
			$timing['batch_code'] 		= $this->input->post('batchcode');
			$timing['chapter_code'] 	= $this->input->post('chaptercode');
			$timing['remarks'] 			= $this->input->post('remarks');
			$timing['_date'] 			= $this->input->post('date');
			$timing['user'] 			= $this->session->username;
			$timing['status'] 			= 'approved';

			$this->db->trans_start();

				//Submit timing for review
				$timingID = $this->TimingRequestModel->submit($timing);

				$this->TimingRequestModel->update_timing_others($timingID, $timing['timing_uid']);
				//Update time table row as submitted for the review
				//$this->TimingRequestModel->update_timetable($timing['time_table_id']);

			$this->db->trans_complete();
			
			($this->db->trans_status() === FALSE)?
			$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
			$this->session->set_flashdata('success', 'Approved');

			redirect('timing-requests/'.$facultyID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('timing-requests/'.$facultyID);
		}
	}

	public function approve($userType, $timingID, $timingUID, $facultyID)
	{
		$this->db->trans_start();

			//$this->TimingRequestModel->update_timetable_status($timetableID, $userType);
			$this->TimingRequestModel->update_timing($timingID);
			$this->TimingRequestModel->update_timing_others($timingID, $timingUID);

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again'):
		$this->session->set_flashdata('success', 'Approved');

		redirect('timing-requests/'.$facultyID);
	}

	public function approved()
	{
		$data['faculties'] = $this->TimingRequestModel->faculties();

		$this->load->view('timing/search', $data);
	}
}