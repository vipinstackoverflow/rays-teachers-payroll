<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ChapterController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('ChapterModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('1', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function chapters($classID, $subjectID)
	{
		$data['class'] 		= $this->ChapterModel->class_meta($classID);
		$data['subject'] 	= $this->ChapterModel->subject($subjectID);
		$data['chapters'] 	= $this->ChapterModel->chapters($subjectID);

		$this->load->view('institute/chapters', $data);
	}

	public function save($classID, $subjectID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('chapter', 'Chapter', 'required|xss_clean|trim');
		$this->form_validation->set_rules('hours', 'Hours', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$chapter['subject_id'] 	= $subjectID;
			$chapter['chapter'] 	= $this->input->post('chapter');
			$chapter['hours'] 		= date('H:i:s', strtotime($this->input->post('hours')));
			
			$this->db->trans_start();

				$this->ChapterModel->save($chapter);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Added'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/chapters/'.$classID.'/'.$subjectID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/chapters/'.$classID.'/'.$subjectID);
		}
	}	

	public function update($classID, $subjectID)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('chapterid', 'Chapter ID', 'required|xss_clean|trim');
		$this->form_validation->set_rules('chapter', 'Chapter', 'required|xss_clean|trim');
		$this->form_validation->set_rules('hours', 'Hours', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$chapterID 				= $this->input->post('chapterid');
			$chapter['chapter'] 	= $this->input->post('chapter');
			$chapter['hours'] 		= date('H:i:s', strtotime($this->input->post('hours')));

			$this->db->trans_start();

				$this->ChapterModel->update($chapter, $chapterID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/chapters/'.$classID.'/'.$subjectID);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/chapters/'.$classID.'/'.$subjectID);
		}
	}

	public function delete($classID, $subjectID, $chapterID)
	{
		$this->db->trans_start();

			$this->ChapterModel->delete($chapterID);
		
		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('institute/chapters/'.$classID.'/'.$subjectID);
	}
}