<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BCDeleteController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('BCDeleteModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('1', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function delete($facultyID)
	{
		$this->db->trans_start();

			$this->BCDeleteModel->delete($facultyID);

		$this->db->trans_complete();

		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again.'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('faculty/bc');
	}
}