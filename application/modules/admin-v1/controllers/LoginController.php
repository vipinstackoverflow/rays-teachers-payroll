<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends MX_Controller {

	public function index()
	{
		if($this->session->admin)
		{
			redirect('admin/userpanel');
		}
		else
		{
			$this->load->view('signin');
		}
	}

	public function logout()
	{
		$session = array('adminname' => '' , 'adminid' => '' ,'access' => '' , 'admin' => FALSE );
		
		# setting session
		$this->session->set_userdata($session);

		#redirect to Admin Home
		redirect('admin/login');
	}
}
