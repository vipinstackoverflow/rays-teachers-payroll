<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CourseController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->isuservalid();
		$this->load->model('CourseModel');
	}

	public function isuservalid()
	{
		if( ! $this->session->user )
		{
			redirect('user/login');
		}
		elseif($this->session->usertype != '1')
		{
			redirect('user/login');
		}
		else
		{
			$level = json_decode($this->session->permissions);

			if( ! in_array('1', $level))
			{
				redirect('user/login');
			}
		}
	}

	public function courses()
	{
		$data['courses'] = $this->CourseModel->courses();

		$this->load->view('institute/courses', $data);
	}

	public function save()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$class['class'] = $this->input->post('class');

			$this->db->trans_start();

				$this->CourseModel->save($class);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Added successfully'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/courses');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/courses');
		}
	}	

	public function update()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('class', 'Class', 'required|xss_clean|trim');

		if($this->form_validation->run() == TRUE)
		{
			$classID 		= $this->input->post('classid');
			$class['class'] = $this->input->post('class');

			$this->db->trans_start();

				$this->CourseModel->update($class, $classID);
			
			$this->db->trans_complete();


			($this->db->trans_status() === TRUE)?
			$this->session->set_flashdata('success', 'Updated'):
			$this->session->set_flashdata('error', 'Something went wrong, Please try again later');

			redirect('institute/courses');
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('institute/courses');
		}
	}	

	public function delete_center($branchID)
	{
		$this->db->trans_start();

			$this->InstituteModel->delete_center($branchID);
		
		$this->db->trans_complete();


		($this->db->trans_status() === FALSE)?
		$this->session->set_flashdata('error', 'Something went wrong, Please try again later'):
		$this->session->set_flashdata('success', 'Deleted');

		redirect('institute/centers');
	}
}