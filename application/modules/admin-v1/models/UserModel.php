<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function users()
	{
		$this->db->order_by('id','DESC');
		$this->db->where("(users.user_type_id = '1' OR users.user_type_id = '2')");
		$query = $this->db->get('users');

		return $query->result();
	}

	public function permissions()
	{
		$query = $this->db->get('permissions');

		return $query->result();
	}

	public function register($user)
	{
		$this->db->insert('users',$user);
	}

	public function register_faculty($faculty)
	{
		$this->db->insert('faculties', $faculty);

		return $this->db->insert_id();
	}

	public function update_user_status($userID, $status)
	{
		$user['status'] = ($status == 'active')? 'active' : 'blocked';

		$this->db->where('id',$userID);
		$this->db->update('users',$user);

		return ($this->db->affected_rows() > 0)? TRUE : FALSE;
	}

	public function user_data($userID)
	{
		$this->db->where('id',$userID);
		$query = $this->db->get('users');

		return $query->row();
	}

	public function update_user($userID,$user)
	{
		$this->db->where('id',$userID);
		$this->db->update('users',$user);
	}
	public function delete_user($userID)
	{
		$this->db->where('id',$userID);
		$this->db->delete('users');
	}
}
