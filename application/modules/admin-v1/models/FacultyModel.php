<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FacultyModel extends CI_Model {

	/**
	* Faculties DB
	*/
	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		//$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function branches()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	public function batches()
	{
		$this->db->where('batch_status', 'active');
		$query = $this->db->get('batches');

		return $query->result();
	}

	/**
	* Faculty Data
	*/
	public function faculty($facultyID)
	{
		$this->db->where('faculties.faculty_id', $facultyID);
		$this->db->where('faculties.faculty_type_id', '1');
		$query = $this->db->get('faculties');

		return $query->row();
	}
}