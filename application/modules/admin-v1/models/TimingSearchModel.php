<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TimingSearchModel extends CI_Model {

	public function search($from, $to, $facultyID)
	{
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->result();
	}

	public function search_summary($from, $to, $facultyID)
	{
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->group_by('timings.slab_type_id');
		$query = $this->db->get('timings');

		if($query->num_rows() > 0)
		{
			$slabtypes = array();
			foreach ($query->result() as $data) 
			{
				$slabtypes[] = $data->slab_type_id;
			}

			$this->db->where_in('slab_type_id', $slabtypes);
			$query = $this->db->get('slab_types');

			$summaryData = array();
			if ($query->num_rows() > 0) 
			{
				$slabtypesData = $query->result();

				for ($i=0; $i < count($slabtypesData); $i++) 
				{
					$summaryData[$i] = $slabtypesData[$i];
					$summaryData[$i]->meta 	= $this->summary_meta($facultyID, $slabtypesData[$i]->slab_type_id, $from, $to);
				}
			}

			return $summaryData;
		}
		else
		{
			return array();
		}
	}

	private function summary_meta($facultyID, $slabtypeID, $from, $to)
	{
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		$this->db->where('timings.slab_type_id', $slabtypeID);
		$this->db->where('timings.payment >', '0');
		$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		$duration = '00:00:00';
		$amount 	= '0';
		foreach ($query->result() as $data) 
		{
			$duration 	= date('H:i:s',strtotime("+".date('H', strtotime($data->duration))." hour +".date('i', strtotime($data->duration))." minutes +".date('s', strtotime($data->duration))." seconds",strtotime($data->duration)));

			$amount 	+= $data->payment;
		}

		$data = (object) array('duration' => $duration, 'amount' => $amount);

		return $data;
	}

	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function slabtypes()
	{
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}
}