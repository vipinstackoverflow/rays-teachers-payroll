<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReportModel extends CI_Model {

	public function report($facultyID, $from, $to)
	{
		$this->db->select("SUM(timings.payment) AS amount");
		$this->db->where('_date >=', $from);
		$this->db->where('_date <=', $to);
		$this->db->where('timings.status', 'approved');
		$this->db->where('timings.faculty_id', $facultyID);
		//$this->db->join('slab_types', 'slab_types.slab_type_id = timings.slab_type_id');
		//$this->db->join('faculties', 'faculties.faculty_id = timings.faculty_id', 'right');
		//$this->db->join('faculty_types', 'faculty_types.faculty_type_id = faculties.faculty_type_id', 'right');
		$query = $this->db->get('timings');

		return $query->row();
	}

	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function slabtypes()
	{
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}
}