<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ChapterModel extends CI_Model {

	public function class_meta($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('classes');

		return $query->row();
	}

	public function subject($subjectID)
	{
		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('subjects');

		return $query->row();
	}

	public function chapters($subjectID)
	{
		$this->db->where('status', 'active');
		$this->db->where('subject_id', $subjectID);
		$query = $this->db->get('chapters');

		return $query->result();
	}

	public function save($chapter)
	{
		$this->db->insert('chapters', $chapter);
	}

	public function update($chapter, $chapterID)
	{
		$this->db->where('chapter_id', $chapterID);
		$this->db->update('chapters', $chapter);
	}

	public function delete($chapterID)
	{
		$chapter['status'] = 'deleted';

		$this->db->where('chapter_id', $chapterID);
		$this->db->update('chapters', $chapter);
	}
}