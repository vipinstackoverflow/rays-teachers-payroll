<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SlabTypeModel extends CI_Model {

	public function slab_types()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function save($slab)
	{
		$this->db->insert('slab_types', $slab);
	}

	public function update($slab, $slabtypeID)
	{
		$this->db->where('slab_type_id', $slabtypeID);
		$this->db->update('slab_types', $slab);
	}

	public function delete($slabtypeID)
	{
		$slab['status'] = 'deleted';

		$this->db->where('slab_type_id', $slabtypeID);
		$this->db->update('slab_types', $slab);
	}
}