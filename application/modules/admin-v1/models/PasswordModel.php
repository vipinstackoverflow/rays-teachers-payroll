<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PasswordModel extends CI_Model {

	public function validate_current_password($password)
	{
		$adminID = $this->session->userid;

		$this->db->where('id',$adminID);
		$query = $this->db->get('users');

		if($query->num_rows()>0)
		{
			$this->load->library('bcrypt');

			if($this->bcrypt->check_password($password['current'], $query->row()->password))
			{
				return TRUE;
			}
			else
			{
				$this->session->set_flashdata('error','Current password is wrong');

				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}

	}

	public function update_password($password)
	{
		$adminID = $this->session->userid;

		$this->load->library('bcrypt');

		$user['password'] 	= $this->bcrypt->hash_password($password['password']);
		$user['updated_at'] = date('d-m-Y H:i:s');

		$this->db->where('id',$adminID);
		$this->db->update('users', $user);

		return ($this->db->affected_rows()>0)? TRUE : FALSE;
	}
}