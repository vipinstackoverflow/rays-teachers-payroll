<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SlabModel extends CI_Model {

	public function faculties()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '1');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();
	}

	public function faculty($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('faculties');

		return $query->row();
	}

	public function slabs($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$this->db->join('slab_types', 'slab_types.slab_type_id = slabs.slab_type_id');
		$query = $this->db->get('slabs');

		return $query->result();
	}

	public function slabtypes()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('slab_types');

		return $query->result();
	}

	public function save($slab)
	{
		$this->db->insert('slabs', $slab);
	}

	public function update($slab, $slabID)
	{
		$this->db->where('slab_id', $slabID);
		$this->db->update('slabs', $slab);
	}

	public function delete($slabID)
	{
		$this->db->where('slab_id', $slabID);
		$this->db->delete('slabs');
	}
}