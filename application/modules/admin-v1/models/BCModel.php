<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BCModel extends CI_Model {

	public function batch_in_charges()
	{
		$this->db->where('faculties.status', 'active');
		$this->db->where('faculties.faculty_type_id', '2');
		$this->db->join('branches', 'branches.branch_id = faculties.branch_id');
		$query = $this->db->get('faculties');

		return $query->result();

		return $query->result();
	}

	public function branches()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	public function batches()
	{
		$this->db->where('batch_status', 'active');
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function batch($batchIDs)
	{
		$this->db->select('batch');
		$this->db->where_in('batch_id', json_decode($batchIDs));
		$query = $this->db->get('batches');

		$batches = '';
		foreach ($query->result() as $data) 
		{
			$batches .= $data->batch.",";
		}

		return $batches;
	}
}