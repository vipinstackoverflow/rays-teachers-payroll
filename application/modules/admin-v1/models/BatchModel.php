<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BatchModel extends CI_Model {

	public function class_meta($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('classes');

		return $query->row();
	}

	public function batches($classID)
	{
		$this->db->where('batch_status', 'active');
		$this->db->where('class_id', $classID);
		$this->db->join('branches', 'branches.branch_id = batches.branch_id');
		$query = $this->db->get('batches');

		return $query->result();
	}

	public function subjects($classID)
	{
		$this->db->where('class_id', $classID);
		$query = $this->db->get('subjects');

		return $query->result();
	}

	public function branches()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('branches');

		return $query->result();
	}

	public function save($batch)
	{
		$this->db->insert('batches', $batch);
	}

	public function update($batch, $batchID)
	{
		$this->db->where('batch_id', $batchID);
		$this->db->update('batches', $batch);
	}

	public function delete($batchID)
	{
		$batch['batch_status'] = 'deleted';

		$this->db->where('batch_id', $batchID);
		$this->db->update('batches', $batch);
	}
}