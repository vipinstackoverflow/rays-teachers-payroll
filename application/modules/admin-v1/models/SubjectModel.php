<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SubjectModel extends CI_Model {

	public function classes()
	{
		$query = $this->db->get('classes');

		return $query->result();
	}

	public function subjects()
	{
		$this->db->where('subjects.status', 'active');
		$this->db->join('classes', 'classes.class_id = subjects.class_id');
		$query = $this->db->get('subjects');

		return $query->result();
	}

	public function save($subject)
	{
		$this->db->insert('subjects', $subject);
	}

	public function update($subject, $subjectID)
	{
		$this->db->where('subject_id', $subjectID);
		$this->db->update('subjects', $subject);
	}

	public function delete($subjectID)
	{
		$subject['status'] = 'deleted';

		$this->db->where('subject_id', $subjectID);
		$this->db->update('subjects', $subject);
	}
}