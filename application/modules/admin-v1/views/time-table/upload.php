   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Time Table</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Time Table</a></li>
          <li class="breadcrumb-item"><a href="#">Upload</a></li>
        </ul>
      </div>
      <?php echo form_open_multipart('timetable/save', 'id="savetimetable"') ?>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
            <div class="row" >             
              <div class="col-md-6" >

                <div class="form-group row">
                  <label class="control-label col-md-3">Sample Format</label>
                  <div class="col-md-8">
                    <a href="<?php echo base_url('resources/data/formats/faculty-payroll-time-table.xlsx'); ?>" class="btn btn-sm btn-success" >Download</a>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Excel File [xlsx]</label>
                  <div class="col-md-8">
                    <input class="form-control" type="file" name="excel" id="excel" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Date</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" autocomplete="off" name="tdate" id="tdate" placeholder="Date">
                  </div>
                </div>
              </div>
              <div class="col-md-6" >
              </div>
            </div>
            <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Upload</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/multiselect/kendo.all.min.js'); ?>"></script>

    <script type="text/javascript">
      $("#tdate").datepicker({
        format: "dd-mm-yyyy",
        todayHighlight: true,
        autoclose:true
      });

      $("#savetimetable").validate({
        rules:{
          tdate:{
            required:true
          },
          excel:{
            required:true,
            extension: "xlsx"
          }
        },
        messages:{
          tdate:{
            required:"Please enter Date"
          },
          excel:{
            required:"Please choose File"
          }
        }
      })

    </script>
    <?php $this->load->view('import/footer'); ?>