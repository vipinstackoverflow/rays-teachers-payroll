   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-clock-o"></i> Mark Timing</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Mark Timing</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <div class="row">
                <div class="col-md-6">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title"><?php echo $facultymeta->faculty_code."-".$facultymeta->name; ?></h3>
                      <?php if(empty($approved)) { ?>
                      <span class="badge badge-danger">Pending</span>
                      <?php } else { ?>
                      <span class="badge badge-success">Approved</span>
                    <?php } ?>
                    </div>
                  </div>
                </div><div class="col-md-6">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <input type="text" class="form-control" name="tdate" id="tdate" value="<?php echo $this->uri->segment(3); ?>" >
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">BC</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php foreach($bc as $data) { ?>
                          <tr>
                            <td><?php echo date('h:ia', strtotime($data->start_time))."-".date('h:ia', strtotime($data->end_time))." ".$data->batch_code; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div> -->
                  </div>
                </div>                
                <div class="col-md-4">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">Faculty</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php foreach($faculty as $data) { ?>
                          <tr>
                            <td><?php echo date('h:ia', strtotime($data->start_time))."-".date('h:ia', strtotime($data->end_time))." ".$data->batch_code; ?></td>
                            <td>
                              <?php echo form_open('timing-requests/add-fac-req-to-bc/'.$this->uri->segment('2').'/'.$this->uri->segment('3')); ?>

                                <input type="hidden" name="timing_uid" value="<?php echo $data->timing_uid; ?>" >
                                <input type="hidden" name="time_table_id" value="<?php echo $data->time_table_id; ?>" >
                                <input type="hidden" name="center_id" value="<?php echo $data->center_id; ?>" >
                                <input type="hidden" name="class_id" value="<?php echo $data->class_id ; ?>" >
                                <input type="hidden" name="batch_id" value="<?php echo $data->batch_id; ?>" >
                                <input type="hidden" name="subject_id" value="<?php echo $data->subject_id; ?>" >
                                <input type="hidden" name="chapter_id" value="<?php echo $data->chapter_id; ?>" >
                                <input type="hidden" name="slab_type_id" value="<?php echo $data->slab_type_id; ?>" >
                                <input type="hidden" name="period_id" value="<?php echo $data->period_id; ?>" > 
                                <input type="hidden" name="faculty_id" value="<?php echo $data->faculty_id; ?>" >
                                <input type="hidden" name="marker_id" value="<?php echo $this->session->facultyid; ?>" >
                                <input type="hidden" name="start_time" class="form-control" value="<?php echo date('h:ia', strtotime($data->start_time)); ?>" >
                                <input type="hidden" name="end_time" class="form-control" value="<?php echo date('h:ia', strtotime($data->end_time)); ?>" >
                                <input type="hidden" name="batch_code" value="<?php echo $data->batch_code; ?>" >
                                <input type="hidden" name="chapter_code" value="<?php echo $data->chapter_code; ?>" >
                                <input type="hidden" name="chapter_status" value="<?php echo $data->chapter_status; ?>" >
                                <input type="hidden" name="remarks" value="<?php echo $data->remarks; ?>" >
                                <input type="hidden" name="_date" value="<?php echo $data->_date; ?>" >

                                <button type="submit" onclick="return confirm('Add Faculty Request to Work Place ?')" title="Add Faculty Request to Work Place" ><i class="fa fa-plus" ></i></button>

                              <?php echo form_close(); ?>
                            </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div> -->
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">TT</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php foreach($timetable as $data) { ?>
                          <tr>
                            <td><?php echo date('h:ia', strtotime($data->start_time))."-".date('h:ia', strtotime($data->end_time))." ".$data->batch_code; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- <div class="tile-footer"><input type="submit" class="btn btn-primary" value="Submit"></div> -->
                  </div>
                </div>
                <?php if(empty($approved)) { ?>
                <div class="col-md-12">
                  <?php echo form_open('timing-requests/final-approval/'.$this->uri->segment(2).'/'.$this->uri->segment(3)); ?>
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">Work Place</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php foreach($bc as $data) { ?>
                          <tr>
                            <input type="hidden" name="timing_id[]" value="<?php echo $data->timing_id; ?>" >
                            <input type="hidden" name="timing_uid[]" value="<?php echo $data->timing_uid; ?>" >
                            <input type="hidden" name="time_table_id[]" value="<?php echo $data->time_table_id; ?>" >
                            <input type="hidden" name="center_id[]" value="<?php echo $data->center_id; ?>" >
                            <input type="hidden" name="class_id[]" value="<?php echo $data->class_id ; ?>" >
                            <input type="hidden" name="batch_id[]" value="<?php echo $data->batch_id; ?>" >
                            <input type="hidden" name="subject_id[]" value="<?php echo $data->subject_id; ?>" >
                            <input type="hidden" name="chapter_id[]" value="<?php echo $data->chapter_id; ?>" >
                            <input type="hidden" name="slab_type_id[]" value="<?php echo $data->slab_type_id; ?>" >
                            <input type="hidden" name="period_id[]" value="<?php echo $data->period_id; ?>" >
                            <input type="hidden" name="faculty_id[]" value="<?php echo $data->faculty_id; ?>" >
                            <input type="hidden" name="marker_id[]" value="<?php echo $this->session->facultyid; ?>" >
                            <input type="hidden" name="batch_code[]" value="<?php echo $data->batch_code; ?>" >
                            <input type="hidden" name="chapter_code[]" value="<?php echo $data->chapter_code; ?>" >
                            <input type="hidden" name="chapter_status[]" value="<?php echo $data->chapter_status; ?>" >
                            <input type="hidden" name="remarks[]" value="<?php echo $data->remarks; ?>" >
                            <input type="hidden" name="_date[]" value="<?php echo $data->_date; ?>" >
                            <td>
                              <input type="text" name="start_time[]" class="form-control time" value="<?php echo date('h:ia', strtotime($data->start_time)); ?>" >
                            </td>
                            <td>
                              <input type="text" name="end_time[]" class="form-control time" value="<?php echo date('h:ia', strtotime($data->end_time)); ?>" >
                            </td>
                            <td><?php echo $data->batch_code; ?></td>
                            <td><a class="btn btn-sm btn-danger" href="<?php echo base_url('timing-requests/delete/'.$data->timing_id.'/'.$this->uri->segment('2').'/'.$this->uri->segment('3')) ?>" onclick="return confirm('Are You Sure ?')" ><i class="fa fa-trash" ></i></a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <div class="tile-footer">
                      <?php if(! empty($bc)) { ?>
                      <input type="submit" class="btn btn-primary" value="Submit">
                      <?php } ?>
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                </div>
              <?php }else { ?>
                <div class="col-md-12">
                  <?php echo form_open('timing-requests/update-final-approval/'.$this->uri->segment(2).'/'.$this->uri->segment(3)); ?>
                  <div class="tile">
                    <div class="tile-title-w-btn">
                      <h3 class="title">Work Place</h3>
                    </div>
                    <div class="tile-body">
                      <table class="table" >
                        <tbody>
                          <?php foreach($approved as $data) { ?>
                          <tr>
                            <input type="hidden" name="timing_id[]" value="<?php echo $data->timing_id; ?>" >
                            <input type="hidden" name="timing_uid[]" value="<?php echo $data->timing_uid; ?>" >
                            <input type="hidden" name="time_table_id[]" value="<?php echo $data->time_table_id; ?>" >
                            <input type="hidden" name="center_id[]" value="<?php echo $data->center_id; ?>" >
                            <input type="hidden" name="class_id[]" value="<?php echo $data->class_id ; ?>" >
                            <input type="hidden" name="batch_id[]" value="<?php echo $data->batch_id; ?>" >
                            <input type="hidden" name="subject_id[]" value="<?php echo $data->subject_id; ?>" >
                            <input type="hidden" name="chapter_id[]" value="<?php echo $data->chapter_id; ?>" >
                            <input type="hidden" name="slab_type_id[]" value="<?php echo $data->slab_type_id; ?>" >
                            <input type="hidden" name="period_id[]" value="<?php echo $data->period_id; ?>" >
                            <input type="hidden" name="faculty_id[]" value="<?php echo $data->faculty_id; ?>" >
                            <input type="hidden" name="marker_id[]" value="<?php echo $this->session->facultyid; ?>" >
                            <input type="hidden" name="batch_code[]" value="<?php echo $data->batch_code; ?>" >
                            <input type="hidden" name="chapter_code[]" value="<?php echo $data->chapter_code; ?>" >
                            <input type="hidden" name="chapter_status[]" value="<?php echo $data->chapter_status; ?>" >
                            <input type="hidden" name="remarks[]" value="<?php echo $data->remarks; ?>" >
                            <input type="hidden" name="_date[]" value="<?php echo $data->_date; ?>" >
                            <td>
                              <input type="text" name="start_time[]" class="form-control time" value="<?php echo date('h:ia', strtotime($data->start_time)); ?>" >
                            </td>
                            <td>
                              <input type="text" name="end_time[]" class="form-control time" value="<?php echo date('h:ia', strtotime($data->end_time)); ?>" >
                            </td>
                            <td><?php echo $data->batch_code; ?></td>

                            <td><a href="<?php echo base_url('timing-requests/delete/'.$this->uri->segment('2').'/'.$this->uri->segment('3')) ?>" ><i class="fa fa-delete" ></i></a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <div class="tile-footer">
                      <?php if(! empty($approved)) { ?>
                      <input type="submit" class="btn btn-primary" value="Update">
                      <?php } ?>
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                </div>
              <?php } ?>
              </div>
            <!-- <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div> -->
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/time-picker/mdtimepicker.min.js'); ?>"></script>

    <script type="text/javascript">

      $(".time").mdtimepicker({
        timeFormat:'hh:mm.ss',
        format:'h:mmtt'
      });

      $("#tdate").datepicker({
        format:"dd-mm-yyyy",
        autoclose:true
      });

      $("#tdate").on('change', function(){
        fid   = "<?php echo $this->uri->segment(2); ?>";
        date  = $(this).val(); 
        window.location = "<?php echo base_url('timing-requests/') ?>"+fid+"/"+date;
      });

    </script>
    <?php $this->load->view('import/footer'); ?>