   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Faculty List</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Timing Requests</a></li>
          <li class="breadcrumb-item"><a href="#">Faculty List</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile">
            <div class="col-md-3" >
              <div class="form-group">
                <label>Date</label>
                <input type="text" name="req_date" id="req_date" class="form-control" value="<?php echo $this->uri->segment('3'); ?>" >
              </div>
            </div>
            <div class="col-md-9" ></div>
          </div>
        </div>
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <a class="btn btn-sm btn-success" href="<?php echo base_url('timing-requests/unsubmitted/'.$this->uri->segment('3')); ?>">Unsubmitted Report</a>
              <h4>Pending Requests - <?php echo $this->uri->segment('3');    ?></h4>
              <table id="faculties" name="faculties" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Short Name</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Address</td>
                    <td>Center</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($faculties as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->name; ?></td>
                    <td><?php echo $data->faculty_code; ?></td>
                    <td><?php echo $data->email; ?></td>
                    <td><?php echo $data->phone; ?></td>
                    <td><?php echo $data->address; ?></td>
                    <td><?php echo $data->branch ?></td>
                    <td>
                      <a href="<?php echo base_url('timing-requests/'.$data->faculty_id.'/'.$this->uri->segment('3')); ?>" class="btn btn-sm btn-success" >View Requests</a>
                      <span class="badge badge-danger" ><?php echo $data->requests; ?></span>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="editFaculty">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Faculty</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('faculty/update', 'id="updatefaculty"'); ?>
          <div class="modal-body">
            <input type="hidden" name="facultyid" id="facultyid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Faculty Code</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="facultycode" id="facultycode" placeholder="Faculty Code eg:AS">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="name" id="name" placeholder="Enter full name">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-8">
                <input class="form-control" type="email" name="email" id="email" placeholder="Enter email address">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Phone</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Address</label>
              <div class="col-md-8">
                <textarea class="form-control" rows="4" name="address" id="address" placeholder="Enter your current address"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Center</label>
              <div class="col-md-8">
                <select class="form-control" id="branch" name="branch" >
                  <?php foreach($branches as $data) { ?>
                  <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">

      $("#req_date").datepicker({
        format:"dd-mm-yyyy",
        todayHighlight:true,
        autoclose:true
      });

      $("#req_date").on('change', function(){
        date = $(this).val();
        window.location = "<?php echo base_url('timing-requests/date/') ?>"+date;
      });

      $(".edit").on('click', function(){
        $("#facultyid").val($(this).data('facultyid'));
        $("#name").val($(this).data('name'));
        $("#email").val($(this).data('email'));
        $("#phone").val($(this).data('phone'));
        $("#address").val($(this).data('address'));
        $("#branch option:selected").removeAttr('selected');
        $("#branch option[value="+$(this).data('branchid')+"]").attr('selected', true);
      });
      $("#faculties").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Faculty List"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Faculty List"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Faculty List"
          }
        ],
        "stateSave":true
      });

      $("#updatefaculty").validate({
        rules:{
          facultycode:{
            required:true
          },
          name:{
            required: true
          },
          phone:{
            required:true
          },
          branch:{
            required:true
          }
        },
        messages:{
          facultycode:{
            required:"Please enter Faculty Code"
          },
          name:{
            required:"Please enter Faculty Name"
          },
          phone:{
            required:"Please enter Phone Number"
          },
          branch:{
            required:"Please select Branch"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>