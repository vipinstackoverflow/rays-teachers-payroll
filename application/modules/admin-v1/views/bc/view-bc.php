   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Batch In Charge</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Batch In Charge</a></li>
          <li class="breadcrumb-item"><a href="#">Edit</a></li>
        </ul>
      </div>
      <?php echo form_open_multipart('bc/update', 'id="updatebc"') ?>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
            <div class="row" >             
              <div class="col-md-6" >
                <input type="hidden" name="facultyid" id="facultyid" value="<?php echo $bc->faculty_id; ?>" >
                <div class="form-group row">
                  <label class="control-label col-md-3">Name</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="name" id="name" placeholder="Enter full name" value="<?php echo $bc->name; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-8">
                    <input class="form-control" type="email" name="email" id="email" placeholder="Enter email address" value="<?php echo $bc->email; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Phone</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone" value="<?php echo $bc->phone; ?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Address</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" name="address" id="address" placeholder="Enter your current address"><?php echo $bc->address; ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Center</label>
                  <div class="col-md-8">
                    <select class="form-control" id="branch" name="branch" >
                      <?php foreach($branches as $data) { ?>
                      <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3">Batches</label>
                  <div class="col-md-8">
                    <select class="form-control batches" id="batches" name="batches[]" >
                      <?php foreach($batches as $data) { ?>
                      <option value="<?php echo $data->batch_id; ?>" ><?php echo $data->batch; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6" >
              </div>
            </div>
            <div class="tile-footer" >
              <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Batch In Charge</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>
    </main>
    <?php $this->load->view('import/scripts'); ?>
    <script src="<?php echo base_url('assets/admin/multiselect/kendo.all.min.js'); ?>"></script>

    <script type="text/javascript">

      $("#updatebc").validate({
        rules:{
          _name:{
            required: true
          },
          _phone:{
            required:true
          },
          _branch:{
            required:true
          }
        },
        messages:{
          _name:{
            required:"Please enter Faculty Name"
          },
          _phone:{
            required:"Please enter Phone Number"
          },
          _branch:{
            required:"Please select Branch"
          }
        }
      });
      var editbc = $("#batches").kendoMultiSelect({
          autoClose: false
      }).data("kendoMultiSelect"); 

      values = <?php echo $bc->batches; ?>;

      editbc.value(values);
    </script>
    <?php $this->load->view('import/footer'); ?>