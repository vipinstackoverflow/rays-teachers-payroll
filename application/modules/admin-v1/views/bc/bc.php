   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Batch In Charges</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Batch In Charge</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?>
              <h4>Batch In Charges</h4>
              <button class="btn btn-success clear" data-toggle="modal" data-target="#newBC" >Add BC</button>
              <br><br>
              <table id="bc" name="bc" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Batches</td>
                    <td>Center</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($bc as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->name; ?></td>
                    <td><?php echo $data->email; ?></td>
                    <td><?php echo $data->phone; ?></td>
                    <td><?php echo $data->batch; ?></td>
                    <td><?php echo $data->branch ?></td>
                    <td>
                      <a class="btn btn-sm btn-success" href="<?php echo base_url('bc/edit/'.$data->faculty_id); ?>">Edit</a>
                      <!-- <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editBC" data-facultyid="<?php echo $data->faculty_id; ?>" data-name="<?php echo $data->name; ?>" data-email="<?php echo $data->email; ?>" data-phone="<?php echo $data->phone; ?>" data-address="<?php echo $data->address; ?>" data-branchid="<?php echo $data->branch_id; ?>" data-batches="<?php echo str_replace('"', "'", $data->batches); ?>" >Edit</button> -->
                    </td>
                    <td>
                      <a href="<?php echo base_url('bc/delete/'.$data->faculty_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure To Delete ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newBC">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add BC</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('bc/register', 'id="newbc"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="name" id="name" placeholder="Enter full name">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-8">
                <input class="form-control" type="email" name="email" id="email" placeholder="Enter email address">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Phone</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Address</label>
              <div class="col-md-8">
                <textarea class="form-control" rows="4" name="address" id="address" placeholder="Enter your current address"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Branch</label>
              <div class="col-md-8">
                <select class="form-control" id="branch" name="branch" >
                  <?php foreach($branches as $data) { ?>
                  <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Batches</label>
              <div class="col-md-8">
                <select class="form-control batches" id="batches" name="batches[]" >
                  <?php foreach($batches as $data) { ?>
                  <option value="<?php echo $data->batch_id; ?>" ><?php echo $data->batch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <!-- The Modal -->
    <div class="modal" id="editBC">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit BC</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('bc/update', 'id="updatebc"'); ?>
          <div class="modal-body">
            <input type="hidden" name="facultyid" id="facultyid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="name" id="_name" placeholder="Enter full name">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-8">
                <input class="form-control" type="email" name="email" id="_email" placeholder="Enter email address">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Phone</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="phone" id="_phone" placeholder="Enter Phone">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Address</label>
              <div class="col-md-8">
                <textarea class="form-control" rows="4" name="address" id="_address" placeholder="Enter your current address"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Branch</label>
              <div class="col-md-8">
                <select class="form-control" id="_branch" name="branch" >
                  <?php foreach($branches as $data) { ?>
                  <option value="<?php echo $data->branch_id; ?>" ><?php echo $data->branch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Batches</label>
              <div class="col-md-8">
                <select class="form-control batches" id="_batches" name="batches[]" >
                  <?php foreach($batches as $data) { ?>
                  <option value="<?php echo $data->batch_id; ?>" ><?php echo $data->batch; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/multiselect/kendo.all.min.js'); ?>"></script>

    <script type="text/javascript">

      var addbc = $("#batches").kendoMultiSelect({
                autoClose: false
            }).data("kendoMultiSelect");      
      var editbc = $("#_batches").kendoMultiSelect({
                autoClose: false
            }).data("kendoMultiSelect");

      $(".edit").on('click', function(){
        $("#facultyid").val($(this).data('facultyid'));
        $("#_name").val($(this).data('name'));
        $("#_email").val($(this).data('email'));
        $("#_phone").val($(this).data('phone'));
        $("#_address").val($(this).data('address'));
        $("#_branch option:selected").removeAttr('selected');
        $("#_branch option[value="+$(this).data('branchid')+"]").attr('selected', true);
        values = $(this).data('batches');
        values = values.replace(/\'/g, "\"");
        editbc.value(values);
      });

      $("#bc").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Batch In Charges"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Batch In Charges"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5]
            },
            title:"Batch In Charges"
          }
        ]
      });

      $("#newbc").validate({
        rules:{
          name:{
            required: true
          },
          phone:{
            required:true
          },
          branch:{
            required:true
          }
        },
        messages:{
          name:{
            required:"Please enter Faculty Name"
          },
          phone:{
            required:"Please enter Phone Number"
          },
          branch:{
            required:"Please select Branch"
          }
        }
      });      
      $("#updatebc").validate({
        rules:{
          _name:{
            required: true
          },
          _phone:{
            required:true
          },
          _branch:{
            required:true
          }
        },
        messages:{
          _name:{
            required:"Please enter Faculty Name"
          },
          _phone:{
            required:"Please enter Phone Number"
          },
          _branch:{
            required:"Please select Branch"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>