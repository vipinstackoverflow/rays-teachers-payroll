   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-bar-chart"></i> Administrator</h1>
          <p>Apex Depositor</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Admin</a></li>
          <li class="breadcrumb-item"><a href="#">Administrators</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <?php if($this->session->flashdata('success')) { ?>
          <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true" style="font-size:20px">×</span>
            </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
          </div>
          <?php } ?>
          <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger fade in alert-dismissible show">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true" style="font-size:20px">×</span>
            </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
          </div>
          <?php } ?>
            <h3 class="tile-title">Administrators</h3>
            <div class="tile-body"  >
              <div class="row">
                <div class="col-lg-6" >
                <?php echo form_open('user/update/'.$user->id, 'id="register"'); ?>
                <a href="<?php echo base_url('user/users'); ?>" class="btn btn-success" ><- Back</a><br><br>
                  <h5>Edit</h5>
                  <div class="form-group row">
                    <label class="control-label col-md-3">Name</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="name" id="name" placeholder="Name" value="<?php echo $user->name; ?>" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-md-3">Permissions</label>
                    <div class="col-md-8">
                    <?php $permitted = json_decode($user->permissions); ?>
                    <?php foreach($permissions as $data) { ?>
                      <h6><input type="checkbox" name="permissions[]" value="<?php echo $data->permission_id; ?>"  <?php if( in_array($data->permission_id, $permitted) ) { ?> checked <?php } ?> >&emsp;<?php echo $data->permission; ?></h6>
                    <?php } ?>
                    </div>
                  </div>                
                  <div class="form-group row">
                    <label class="control-label col-md-3">Username</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="username" id="username" placeholder="Username" value="<?php echo $user->username ?>" disabled >
                    </div>
                  </div>                
                  <div class="form-group row">
                    <label class="control-label col-md-3">Password</label>
                    <div class="col-md-8">
                      <input class="form-control" type="password" name="newpassword" id="newpassword" placeholder="Password">
                    </div>
                  </div>                
                  <div class="form-group row">
                    <label class="control-label col-md-3">Confrm Password</label>
                    <div class="col-md-8">
                      <input class="form-control" type="password" name="password" id="password" placeholder="Confirm Password">
                    </div>
                  </div>                  
                  <div class="form-group row">
                    <div class="col-md-8">
                      <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>update</button>&nbsp;&nbsp;&nbsp;
                    </div>
                  </div>
                <?php echo form_close(); ?>
                </div>
                <div class="col-lg-6" >
                </div>
              </div>
            </div>
            <div class="tile-footer">
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </main>
    <?php $this->load->view('import/scripts'); ?>

    <script type="text/javascript">
      $.validator.addMethod("checkUserName", 
        function(value, element) {
            var result = false;
            $.ajax({
                type:"POST",
                async: false,
                url: "<?php echo base_url('user/is-username-valid'); ?>", // script to validate in server side
                data: {username: value},
                success: function(data) {
                    result = (data == true) ? true : false;
                }
            });
            // return true if username is exist in database
            return result; 
        }, 
        "This username is already taken! Try another."
      );
      $("#register").validate({
        rules:{
          name:{
            required:true
          },
          permissions:{
            required:true
          },
          username:{
            required:true
          },
          newpassword:{
            minlength:6
          },
          password:{
            minlength:6,
            equalTo:"#newpassword"
          }
        },
        messages:{
          name:{
            required:"Name can't be empty"
          },
          permissions:{
            required:"Permissions can't be empty"
          },
          username:{
            required:"Username can't be empty"
          },
          newpassword:{
            minlength:"Password must contain atleast 6 characters"
          },
          password:{
            minlength:"Password must contain atleast 6 characters",
            equalTo:"Passwords does not match"
          }
        }
      });
    </script>
    <?php $this->load->view('import/footer'); ?>