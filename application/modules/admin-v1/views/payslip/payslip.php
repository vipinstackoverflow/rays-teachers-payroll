<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Pay Slip</title>
<script type="text/javascript">
  window.print();
</script>
</head>

<body style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px">
<table width="1000" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse">
  <tbody>
    <tr>
      <td colspan="2" style="text-align:center; border-bottom:1px solid black">
        <img src="<?php echo base_url('assets/admin/images/logo.jpg'); ?>" alt="">
        <p style="margin:0 0 0 0">Kanakasree Arcade, Behind Kairali Theatre,<br>S.K. Temple Road, Calicut</p>
        <p style="font-weight:bold">Pay Slip<br><small>for <?php echo $date; ?></small><br><?php echo $faculty->name; ?></p>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" cellpadding="5" cellspacing="0">
          <tr>
            <td>Employee :</td>
            <td><?php echo $faculty->name; ?></td>
          </tr>
          <tr>
            <td>Email:</td>
            <td><?php echo $faculty->email; ?></td>
          </tr>
          <tr>
            <td>Phone:</td>
            <td><?php echo $faculty->phone; ?></td>
          </tr>
        </table>
      </td>
      <td>
      <table width="100%" cellpadding="5" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>          
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>          
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>         
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="border:1px solid black">
      <?php $i=0; $payment = 0; foreach($approved as $data) { $i++; $payment += $data->payment; } ?>
        <table width="100%" cellpadding="5" cellspacing="0">
          <tr>
            <td style="border-bottom:1px solid black; border-right:1px solid black"><strong>Earnings</strong></td>
            <td style="border-bottom:1px solid black; border-right:1px solid black"><strong>Amount</strong></td>
          </tr>
          <tr>
            <td style="border-right:1px solid black">Class Taken</td>
            <td style="border-right:1px solid black"><?php echo $payment; ?></td>
          </tr>
          <tr>
            <td style="border-right:1px solid black">&nbsp;</td>
            <td style="border-right:1px solid black">&nbsp;</td>
          </tr>
          <tr>
            <td style="border-right:1px solid black">&nbsp;</td>
            <td style="border-right:1px solid black">&nbsp;</td>
          </tr>
          <tr>
            <td style="border-right:1px solid black">&nbsp;</td>
            <td style="border-right:1px solid black">&nbsp;</td>
          </tr>
          <tr>
            <td style="border-right:1px solid black; border-bottom:1px solid black; border-top:1px solid black"><strong>Total Earnings</strong></td>
            <td style="border-right:1px solid black; border-bottom:1px solid black; border-top:1px solid black"><strong><?php echo number_format($payment, 2); ?></strong></td>
          </tr>
          <tr>
            <td style="border-right:1px solid black"><strong>Net Amount</strong></td>
            <td><strong><?php echo number_format($payment, 2); ?></strong></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <p>Amount (in words):</p>
        <p id="amount" ></p>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <p>This is a system generated Pay Slip, hence signature is not required.</p>
      </td>
    </tr>
  </tbody>
</table>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/numbertowords.js'); ?>" ></script>
<script type="text/javascript">
  document.getElementById("amount").innerHTML= toWords(<?php echo $payment; ?>)+" rupees only";
</script>
</body>
</html>
