   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-university"></i> Centers</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Centers</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4>Centers</h4>

              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newCenter" >Add Center</button><br><br>

              <table id="centers" name="centers" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Center</td>
                    <td>Phone</td>
                    <td>Address</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($centers as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->branch; ?></td>
                    <td><?php echo $data->branch_phone; ?></td>
                    <td><?php echo $data->branch_address; ?></td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editCenter" data-branchid="<?php echo $data->branch_id; ?>" data-branch="<?php echo $data->branch; ?>" data-phone="<?php echo $data->branch_phone; ?>" data-address="<?php echo $data->branch_address; ?>" >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('institute/delete-center/'.$data->branch_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure To Delete ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newCenter">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Center</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/save-center', 'id="savecenter"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Center</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="branch" id="branch" placeholder="Center">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Phone</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="phone" id="phone" placeholder="Phone">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Address</label>
              <div class="col-md-8">
                <textarea class="form-control" name="address" id="address" placeholder="Address" ></textarea>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>     
    <!-- The Modal -->
    <div class="modal" id="editCenter">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Center</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/update-center', 'id="updatecenter"'); ?>
          <div class="modal-body">
            <input type="hidden" name="branchid" id="branchid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Center</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="branch" id="_branch" placeholder="Center">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Phone</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="phone" id="_phone" placeholder="Phone">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Address</label>
              <div class="col-md-8">
                <textarea class="form-control" name="address" id="_address" placeholder="Address" ></textarea>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(".edit").on('click', function(){
        $("#branchid").val($(this).data('branchid'));
        $("#_branch").val($(this).data('branch'));
        $("#_phone").val($(this).data('phone'));
        $("#_address").val($(this).data('address'));
      });
      $("#centers").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2, 3]
            },
            title:"Centers"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 2, 3]
            },
            title:"Centers"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2, 3]
            },
            title:"Centers"
          }
        ],
        "stateSave":true
      });

      $("#savecenter").validate({
        rules:{
          branch:{
            required:true
          },
          phone:{
            required: true
          },
          address:{
            required:true
          }
        },
        messages:{
          branch:{
            required:"Please enter Center Name"
          },
          phone:{
            required:"Please enter Phone"
          },
          address:{
            required:"Please enter Address"
          }
        }
      });      

      $("#updatecenter").validate({
        ignore:"",
        rules:{
          branchid:{
            required:true
          },
          branch:{
            required:true
          },
          phone:{
            required: true
          },
          address:{
            required:true
          }
        },
        messages:{
          branch:{
            required:"Please enter Center Name"
          },
          phone:{
            required:"Please enter Phone"
          },
          address:{
            required:"Please enter Address"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>