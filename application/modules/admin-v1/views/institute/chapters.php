   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-book"></i> Chapters</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Subject</a></li>
          <li class="breadcrumb-item"><a href="#">Chapters</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4><?php echo $class->class."-".$subject->subject; ?> Chapters</h4>

              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newChapter" >Add Chapter</button><br><br>

              <table id="chapters" name="chapters" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Chapter</td>
                    <td>Time Allowed</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($chapters as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->chapter; ?></td>
                    <td><?php echo $data->hours; ?></td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editChapter" data-chapterid="<?php echo $data->chapter_id; ?>" data-chapter="<?php echo $data->chapter; ?>"  data-hours="<?php echo date('H:i', strtotime($data->hours)); ?>" >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('institute/chapter/delete/'.$this->uri->segment(3).'/'.$data->subject_id.'/'.$data->chapter_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure To Delete ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newChapter">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Chapter</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/chapter/save/'.$this->uri->segment(3).'/'.$this->uri->segment(4), 'id="savechapter"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Chapter</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="chapter" id="chapter" placeholder="Chapter">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Hours Allowed</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="hours" id="hours" placeholder="Hours">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>     
    <!-- The Modal -->
    <div class="modal" id="editChapter">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Chapter</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/chapter/update/'.$this->uri->segment(3).'/'.$this->uri->segment(4), 'id="updatechapter"'); ?>
          <div class="modal-body">
            <input type="hidden" name="chapterid" id="chapterid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Chapter</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="chapter" id="_chapter" placeholder="Chapter">
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-3">Hours</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="hours" id="_hours" placeholder="Hours">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $("#chapters").on('click', '.edit', function(){
        $("#chapterid").val($(this).data('chapterid'));
        $("#_chapter").val($(this).data('chapter'));
        $("#_hours").val($(this).data('hours'));
      });
      $("#chapters").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1]
            },
            title:"<?php echo $class->class."-".$subject->subject; ?> Chapters"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1]
            },
            title:"<?php echo $class->class."-".$subject->subject; ?> Chapters"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"<?php echo $class->class."-".$subject->subject; ?> Chapters"
          }
        ],
        "stateSave":true
      });

      $("#savechapter").validate({
        rules:{
          chapter:{
            required:true
          }
        },
        messages:{
          chapter:{
            required:"Please enter Chapter"
          }
        }
      });      

      $("#updatechapter").validate({
        ignore:"",
        rules:{
          chapterid:{
            required:true
          },
          chapter:{
            required:true
          }
        },
        messages:{
          chapter:{
            required:"Please enter Chapter"
          }
        }
      });  

    </script>
    <?php $this->load->view('import/footer'); ?>