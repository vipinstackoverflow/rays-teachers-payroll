   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-graduation-cap"></i> Courses</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Courses</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?> 
              <h4>Centers</h4>

              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newCenter" >Add Course</button><br><br>

              <table id="courses" name="courses" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Course</td>
                    <td>Batches</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($courses as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->class; ?></td>
                    <td>
                      <a href="<?php echo base_url('institute/batches/'.$data->class_id); ?>" class="btn btn-sm btn-success" >Batches</a>
                    </td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editCourse" data-classid="<?php echo $data->class_id; ?>" data-class="<?php echo $data->class; ?>" >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('institute/course/delete/'.$data->class_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure To Delete ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newCenter">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Course</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/course/save', 'id="savecourse"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Course</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="class" id="class" placeholder="Class">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>     
    <!-- The Modal -->
    <div class="modal" id="editCourse">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Course</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('institute/course/update', 'id="updatecourse"'); ?>
          <div class="modal-body">
            <input type="hidden" name="classid" id="classid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Course</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="class" id="_class" placeholder="Course">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(".edit").on('click', function(){
        $("#classid").val($(this).data('classid'));
        $("#_class").val($(this).data('class'));
      });
      $("#courses").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1]
            },
            title:"Courses"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1]
            },
            title:"Courses"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1]
            },
            title:"Courses"
          }
        ],
        "stateSave":true
      });

      $("#savecourse").validate({
        rules:{
          class:{
            required:true
          }
        },
        messages:{
          class:{
            required:"Please enter Course"
          }
        }
      });      

      $("#updatecourse").validate({
        ignore:"",
        rules:{
          classid:{
            required:true
          },
          class:{
            required:true
          }
        },
        messages:{
          class:{
            required:"Please enter Course"
          }
        }
      });

    </script>
    <?php $this->load->view('import/footer'); ?>