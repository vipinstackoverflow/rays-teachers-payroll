    <!-- Essential javascripts for application to work-->
    <script src="<?php echo base_url('assets/admin/js/jquery-3.2.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/main.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery-ui.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/moment.js'); ?>"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="<?php echo base_url('assets/admin/js/plugins/pace.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery.validate.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/additional-methods.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/dataTable/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/dataTable/js/dataTables.bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery.easy-autocomplete.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/daterangepicker/js/daterangepicker.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/timepicker/jquery.timepicker.min.js'); ?>"></script>