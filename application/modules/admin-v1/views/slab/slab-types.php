   <?php $this->load->view('import/header'); ?>
    <main class="app-content">
      <div class="app-title">
        <div> 
          <h1><i class="fa fa-users"></i> Faculty List</h1>
          <p>Faculty Payroll</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="#">Slab Types</a></li>
        </ul>
      </div>
      <div class="row" >
        <div class="col-md-12" >
          <div class="tile table-responsive" >
              <?php if($this->session->flashdata('success')) { ?>
              <div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button>    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>
              <?php if($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger fade in alert-dismissible show">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true" style="font-size:20px">×</span>
                </button><strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?>
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newSlabType" >Add Slab Type</button> 
              <h4>Slab Types</h4>
              <table id="slabtypes" name="slabtypes" class="table table-striped" >
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Slab Type Code</td>
                    <td>Slab Type</td>
                    <td>Action</td>
                    <td>Action</td>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($slabtypes as $data) { $i++; ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data->slab_type_id; ?></td>
                    <td><?php echo $data->slab_type; ?></td>
                    <td>
                      <button type="button"  class="btn btn-sm btn-success edit" data-toggle="modal" data-target="#editSlabType" data-slabtypeid="<?php echo $data->slab_type_id; ?>" data-slabtype="<?php echo $data->slab_type; ?>" >Edit</button>
                    </td>
                    <td>
                      <a href="<?php echo base_url('slab/delete/'.$data->slab_type_id); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are You Sure ?')" >Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            <div class="tile-footer" >
              <!-- <div class="row">
                <div class="col-md-8 col-md-offset-3">
                  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;
                </div>
              </div> -->              
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- The Modal -->
    <div class="modal" id="newSlabType">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Slab Type</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('slab/save', 'id="saveslab"'); ?>
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-md-3">Slab Type</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="slab_type" id="slab_type" placeholder="Slab Type">
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Save</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <!-- The Modal -->
    <div class="modal" id="editSlabType">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Slab Type</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <?php echo form_open('slab/update', 'id="updateslab"'); ?>
          <div class="modal-body">
            <input type="hidden" name="slabtypeid" id="slabtypeid" >
            <div class="form-group row">
              <label class="control-label col-md-3">Slab Type</label>
              <div class="col-md-8">
                <input class="form-control" type="text" name="slab_type" id="_slab_type" placeholder="Slab Type">
              </div>
            </div>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" >Update</button>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>   
    <?php $this->load->view('import/scripts'); ?>
    <?php $this->load->view('import/data-table'); ?>
    <script src="<?php echo base_url('assets/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script>

    <script type="text/javascript">
      $(".edit").on('click', function(){
        $("#slabtypeid").val($(this).data('slabtypeid'));
        $("#_slab_type").val($(this).data('slabtype'));
      });
      $("#slabtypes").DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'print',
            exportOptions: {
              columns: [0, 1, 2]
            },
            title:"Slab Types"
          }, 
          {
            extend: 'excel',
            exportOptions: {
              columns: [0, 1, 3]
            },
            title:"Slab Types"
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: [0, 1, 3]
            },
            title:"Slab Types"
          }
        ],
        "stateSave":true
      });

      $("#saveslab").validate({
        rules:{
          slab_type:{
            required:true
          }
        },
        messages:{
          slab_type:{
            required:"Please enter Slab Type"
          }
        }
      });      

      $("#updateslab").validate({
        rules:{
          slab_type:{
            required:true
          }
        },
        messages:{
          slab_type:{
            required:"Please enter Slab Type"
          }
        }
      });

    </script>
    